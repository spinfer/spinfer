BUNDLES_DIR = $(shell pwd)/bundles
BUNDLES = $(wildcard $(BUNDLES_DIR)/*)
FLAGS = $(foreach d, $(BUNDLES),-cflags '-I $d' \
	-lflags '-I $d $(shell basename $d).cmxa')
BYTE_FLAGS = $(foreach d, $(BUNDLES),-cflags '-I $d' \
	-lflags '-I $d $(shell basename $d).cma')
OCAMLBUILD = ocamlbuild -use-ocamlfind
OCAMLC = ocamlc
EXECUTABLE = spinfer
OCAMLDOC = 	ocamldoc
INSTALL_DIR_PREFIX = /usr/local
INSTALL_DIR = $(INSTALL_DIR_PREFIX)/lib/spinfer
BIN_FILE = $(INSTALL_DIR_PREFIX)/bin/spinfer
MAN_DIR = /usr/local/share/man/man1
OCAML_INCLUDES = -I _build/coccinelle/parsing_c \
	-I _build/coccinelle/commons \
	-I _build/coccinelle/tools \
	-I _build/coccinelle/globals \
	-I _build/coccinelle \
	-I _build/coccinelle/engine \
	-I _build/coccinelle/parsing_cocci \
	-I _build/coccinelle/ctl \
	-I _build/src \
	-I _build/src/control-graph \
	-I _build/src/pattern-mining \
	-I _build/src/input-output \
	-I _build/src/common \
	-I _build/src/MCES \
	-I _build/src/skim \
	-I _build/src/clone-detection \
	-I _build/src/growing-sequences \
    -I _build/src/test_coccinelle \
	-I _build/src/semantic-patches
OCAMLDOC_FILES = src/**/*.ml src/**/*.mli src/*.ml
OCAMLDOC_COCCI_MODULES = coccinelle/parsing_c/control_flow_c.* \
	coccinelle/parsing_c/ast_c.* \
	coccinelle/parsing_c/visitor_c.* \
	coccinelle/parsing_c/unparse_c.* \
	coccinelle/parsing_c/control_flow_c_build.* \
	coccinelle/parsing_c/type_annoter_c.* \
	coccinelle/commons/ograph_extended.* \
	coccinelle/commons/common.* \
	coccinelle/globals/flag.* \
	coccinelle/ctl/flag_ctl.* \
	coccinelle/engine/flag_matcher.* \
	coccinelle/flag_cocci.* \
	coccinelle/parsing_cocci/flag_parsing_cocci.* \
	coccinelle/parsing_c/flag_parsing_c*
DOC_FOLDER = doc
GRAPH_OUTPUT_FOLDER = graphs
PATCHES_OUTPUT_FILE = patterns.txt
GITINSPECTOR = gitinspector


build:
	$(OCAMLBUILD) $(FLAGS) $(EXECUTABLE).native

build_debug:
	$(OCAMLBUILD) $(FLAGS) -cflag -g $(EXECUTABLE).native

build_byte:
	$(OCAMLBUILD) $(BYTE_FLAGS) -cflag -g $(EXECUTABLE).d.byte

safe:
	$(OCAMLBUILD) -cflag -g safe.native

eval_patch:
	$(OCAMLBUILD) $(FLAGS) eval_patch.native

doc:
	mkdir -p $(DOC_FOLDER)/graphs
	$(OCAMLDOC) \
		$(OCAML_INCLUDES) \
		-dot-reduce -dot -o $(DOC_FOLDER)/graphs/dependency_graph.gv \
		$(OCAMLDOC_FILES)
	$(OCAMLDOC) \
		$(OCAML_INCLUDES) \
		-dot -o $(DOC_FOLDER)/graphs/dependency_graph_complete.gv \
		$(OCAMLDOC_FILES)
	$(OCAMLDOC) \
		$(OCAML_INCLUDES) \
		-dot -o $(DOC_FOLDER)/graphs/dependency_graph_complete_dependencies.gv \
		$(OCAMLDOC_FILES) $(OCAMLDOC_COCCI_MODULES)
	mkdir -p $(DOC_FOLDER)/HTML
	$(OCAMLDOC) \
		$(OCAML_INCLUDES) \
		-html -keep-code -m p -sort -colorize-code -d $(DOC_FOLDER)/HTML \
		-t "The spinfer tool" -intro $(DOC_FOLDER)/intro.txt \
		$(OCAMLDOC_FILES)
	ln -f -s HTML/index.html $(DOC_FOLDER)/doc-main-page.html

inspect:
	$(GITINSPECTOR) -F htmlembedded -f ml,mli -HlLmrTw -x tests/ -x calendar/ \
	> $(DOC_FOLDER)/git_info.html ./

test_debug: build_debug
	./$(EXECUTABLE).native --specfile tests/files.spec \
		--diff-graphs-dir $(GRAPH_OUTPUT_FOLDER) -v

interface:
	$(OCAMLC) $(OCAML_INCLUDES) -i ${file}.ml > ${file}.mli

test:
	./$(EXECUTABLE).native --specfile tests/files.spec \
	 --graphs-dir $(GRAPH_OUTPUT_FOLDER) \
	 -o $(PATCHES_OUTPUT_FILE) -v

test_rule:
	./$(EXECUTABLE).native --specfile tests/sample_rule${number}/index \
	--graphs-dir $(GRAPH_OUTPUT_FOLDER) \
	-o $(PATCHES_OUTPUT_FILE) -v

profile:
	$(OCAMLBUILD) $(EXECUTABLE).p.native

mascot:
	mascot -config .mascot -html mascot-report.html $(OCAMLDOC_FILES)

all: build doc

install:
	install -d $(INSTALL_DIR)/_build/src
	install _build/src/spinfer.native $(BIN_FILE)
	install -d $(MAN_DIR)
	install $(DOC_FOLDER)/spinfer.1 $(MAN_DIR)/spinfer.1
	install -d $(INSTALL_DIR_PREFIX)/share/spinfer/statics
	install -t $(INSTALL_DIR_PREFIX)/share/spinfer/statics statics/*


uninstall:
	rm -rf $(INSTALL_DIR)
	rm -f $(BIN_FILE)
	rm -f $(MAN_DIR)/spinfer.1
	mandb > /dev/null

clean:
	rm -rf _build/

veryclean: clean
	rm -rf bundles/

.PHONY: all doc clean veryclean
