module A = Ast_cocci
module B = Ast_c
module F = Control_flow_c
module S = Subexpression
module CH = Cocci_hack

let opt f e =
  match e with
  | Some e -> Some (f e)
  | None -> None

let mc x = A.make_mcode x
let mt x = A.make_term x


let rec expression_to_cocci ((expr, info), _) =
  let base_expr =
    let is_local = match fst !info with
    | Some(_, B.LocalVar _) | Some(_, B.StaticLocalVar _) -> true
    | _ -> false
    in
    match expr with
    | B.Ident(name) ->
      if is_local
      then A.Ident(CH.to_wrap [CH.Local] (name_to_id name))
      else A.Ident (name_to_id name)
    | B.Constant(const) -> A.Constant (mc (constant_to_cocci const))
    | B.StringConstant(str_frag,_, isWchar) ->
      let string_fragment_to_cocci  ((str_frag,_): B.string_fragment)
        : A.base_string_fragment =
        match str_frag with
        | B.ConstantFragment(str) -> A.ConstantFragment (mc str)
        | B.FormatFragment(str_format,_) ->
          let str_format_cocci = match str_format with
            | B.ConstantFormat(str) -> A.ConstantFormat (A.make_mcode str)
          in
          A.FormatFragment (A.make_mcode "%",mt (str_format_cocci))
      in
      let str_frag_cocci =
        List.map (fun str -> mt (string_fragment_to_cocci str)) str_frag
      in
      A.StringConstant(mc "\"",mt str_frag_cocci,mc "\"",isWchar_to_cocci isWchar)
    | B.FunCall(expr,args) ->
      let args_cocci = args_to_cocci args in
      A.FunCall(expression_to_cocci expr, mc "(", mt args_cocci, mc ")")
    | B.CondExpr(expr1,expr2,expr3) ->
      let expr1 = expression_to_cocci expr1 in
      let expr2 = opt expression_to_cocci expr2 in
      let expr3 = expression_to_cocci expr3 in
      A.CondExpr(expr1,mc "?",expr2,mc ":",expr3)
    | B.Sequence(expr1,expr2) ->
      A.Sequence(expression_to_cocci expr1, mc ",", expression_to_cocci expr2)
    | B.Assignment(expr1,(op_assign,_),expr2) ->
      let op_assign = match op_assign with
        | B.SimpleAssign -> A.SimpleAssign (mc "=")
        | B.OpAssign(op)-> A.OpAssign (mc (arithOp_to_cocci op))
      in
      let expr1 = expression_to_cocci expr1 in
      let expr2 = expression_to_cocci expr2 in
      A.Assignment(expr1, mt op_assign,expr2,false)
    | B.Postfix(expr,op) ->
      A.Postfix(expression_to_cocci expr, mc (fixOp_to_cocci op))
    | B.Infix(expr,op) ->
      A.Infix(expression_to_cocci expr, mc (fixOp_to_cocci op))
    | B.Unary(expr,op) ->
      A.Unary(expression_to_cocci expr, mc (unaryOp_to_cocci op))
    | B.Binary(expr1,op,expr2) ->
      A.Binary(expression_to_cocci expr1,
               binaryOp_to_cocci op,
               expression_to_cocci expr2)
    | B.ArrayAccess(expr1,expr2) ->
      let expr1 = expression_to_cocci expr1 in
      let expr2 = expression_to_cocci expr2 in
      A.ArrayAccess(expr1,mc "[",expr2,mc "]")
    | B.RecordAccess(expr,name) ->
      A.RecordAccess(expression_to_cocci expr, mc ".", name_to_id name)
    | B.RecordPtAccess(expr,name) ->
      A.RecordPtAccess(expression_to_cocci expr, mc "->", name_to_id name)
    | B.SizeOfExpr(expr) ->
      A.SizeOfExpr(mc "sizeof", expression_to_cocci expr)
    | B.SizeOfType(typ) ->
      A.SizeOfType(mc "sizeof", mc "(", type_to_cocci typ, mc ")")
    | B.Cast(typ,expr) ->
      A.Cast(mc "(", type_to_cocci typ, mc ")", expression_to_cocci expr)
    | B.StatementExpr(_) ->
      raise (Debug.NotInC "Multiple instructions inside an expression.")
    | B.Constructor(typ,init) ->
      A.Constructor(mc "(", type_to_cocci typ, mc ")",initialiser_to_cocci init)
    | B.ParenExpr(expr) ->
      A.Paren(mc "(", expression_to_cocci expr, mc ")")
    | B.New(_,_) | B.Delete(_) | B.Defined(_)
      -> raise (Debug.NotInC "C++ construction.")
  in
  mt base_expr


and args_to_cocci args =
  let argument_to_cocci arg =
    match arg with
    | Common.Left(expr) -> expression_to_cocci expr
    | Common.Right(B.ArgType(param)) ->
	mt (A.TypeExp(type_to_cocci param.B.p_type))
    | _ -> raise (Debug.NotInC "Handle: weird arguments.")
  in
  let args_cocci = ref [] in
  List.iter (fun (arg,_) ->
      args_cocci := (argument_to_cocci arg) :: !args_cocci;
      args_cocci := mt (A.EComma(mc ",")) :: !args_cocci;
    ) args;
  if !args_cocci = [] then []
  else List.rev (List.tl !args_cocci)

and initialiser_to_cocci (init,_) =
  match init with
  | B.InitExpr(expr) -> mt (A.InitExpr (expression_to_cocci expr))
  | B.InitList(_)
  | B.InitDesignators(_,_)
  | B.InitFieldOld(_,_)
  | B.InitIndexOld(_,_) ->
    raise (Debug.NotInC "Unsupported initialiser construction.")

and type_to_cocci ((typeQualifierbis,_),(typ,_)) =
  let baseType_to_cocci (btype: B.baseType) : A.base_typeC =
    let wrap_basetype (basetype: A.baseType) : A.base_typeC =
      let str = A.string_of_baseType basetype in
      A.BaseType(basetype, [mc str])
    in
    match btype with
    | B.Void -> wrap_basetype A.VoidType
    | B.IntType (intType) ->
      begin
        match intType with
        | B.CChar -> wrap_basetype A.CharType
        | B.Si(sign, base) ->
          let subtype = match base with
          | B.CChar2 -> A.CharType
          | B.CShort -> A.ShortType
          | B.CInt -> A.IntType
          | B.CLong -> A.LongType
          | B.CLongLong -> A.LongLongType
          in
          begin match sign with
          | B.UnSigned ->
            A.SignedT(mc A.Unsigned, Some(mt (wrap_basetype subtype)))
          | B.Signed -> wrap_basetype subtype
          end
      end
    | B.FloatType (floatType) ->
      wrap_basetype begin
        match floatType with
        | B.CFloat -> A.FloatType
        | B.CDouble -> A.DoubleType
        | B.CLongDouble -> A.LongDoubleType
        | B.CFloatComplex -> A.FloatComplexType
        | B.CDoubleComplex -> A.DoubleComplexType
        | B.CLongDoubleComplex -> A.LongDoubleComplexType

      end
    | B.SizeType -> wrap_basetype A.SizeType
    | B.SSizeType -> wrap_basetype A.SSizeType
    | B.PtrDiffType -> wrap_basetype A.PtrDiffType
  in
  let base_typeC = begin
    match typ with
    | B.BaseType(baseType) -> baseType_to_cocci baseType
    | B.Pointer(fullType) ->
      A.Pointer(type_to_cocci fullType, mc "*")
    | B.Array(expr,fullType) ->
      A.Array(type_to_cocci fullType,mc "[",opt expression_to_cocci expr,mc "]")
    | B.Decimal(expr1,expr2) ->
      let expr1 = expression_to_cocci expr1 in
      let expr2 = opt expression_to_cocci expr2 in
      A.Decimal(mc "decimal",mc "(",expr1,Some (mc ","),expr2,mc ")")
    | B.FunctionType(ftype,(parameters,_)) ->
      let params_cocci =
        List.map (fun (param,_) ->
            let base_parameterTypeDef = match param.B.p_namei with
              | Some(name) -> A.Param(type_to_cocci ftype,Some(name_to_id name))
              | None -> A.VoidParam(type_to_cocci ftype)
            in mt  base_parameterTypeDef
          )parameters
      in
      A.FunctionPointer(type_to_cocci ftype,
                        mc "(", mc "*", mc ")", mc "(",
                        mt params_cocci, mc ")")
    | B.EnumName(str)-> A.EnumName(mc "enum", Some (mt (A.Id (mc str))))
    | B.StructUnionName(struct_union,str) ->
      let struct_union_cocci = match struct_union with
      | B.Struct -> A.Struct
      | B.Union -> A.Union
      in
      A.StructUnionName(mc struct_union_cocci, Some (mt (A.Id (mc str))))
    | B.TypeName(name,_) ->
      let tn =
        match name with
        | B.RegularName(str,_) -> A.TypeName(mc str)
        | _ -> raise (Debug.NotInC "TypeName: unsupported in ast_cocci")
      in tn
    | B.TypeOfExpr(expr) ->
      A.TypeOfExpr(mc "sizeof", mc "(", expression_to_cocci expr, mc ")")
    | B.TypeOfType(fullType) ->
      A.TypeOfType(mc "sizeof", mc "(", type_to_cocci fullType, mc ")")
    | B.NoType -> A.BaseType(A.Unknown,[mc ""])
    | B.Enum(_,_)
    | B.FieldType(_,_,_)
    | B.StructUnion(_,_,_)
    | B.ParenType(_) ->
      raise (Debug.NotInC "type: unsupported in ast_cocci")
  end in
  let (const_vol,empty) =
    match (typeQualifierbis.B.const,typeQualifierbis.B.volatile) with
    | (true,false) -> (A.Const,false)
    | (false,true) -> (A.Volatile,false)
    | (false,false) -> (A.Const,true)
    | (true,true) ->
      raise (Debug.NotInC "const_vol: unsupported in ast_cocci")
  in
  match empty with
  | false -> mt (A.Type (false, Some (mc const_vol), mt base_typeC))
  | true -> mt (A.Type (false, None, mt base_typeC))

and declaration_to_cocci decl =
  let onedecl_to_cocci (onedecl : B.onedecl) : A.base_declaration =
    let (id,init) = begin match onedecl.B.v_namei with
      | Some(name,init) ->
        (Some (CH.to_wrap [CH.NewIdent] (name_to_id name)),
         begin match init with
           | B.NoInit -> None
           | B.ValInit(_,init') ->
             Some (initialiser_to_cocci init')
           | B.ConstrInit(_) ->
             raise (Debug.NotInC "C++ constructor initialiser.")
         end
        )
      | None -> (None,None)
    end
    in
    let attrs = List.map(fun (B.Attribute(str),_) -> mc str) onedecl.B.v_attr in
    let (storagebis,inline) = onedecl.B.v_storage in
    let storage =
      match storagebis_to_cocci storagebis with
      | None -> None
      | Some(s) -> Some (mc s)
    in
    let typ = type_to_cocci onedecl.B.v_type in
    begin match (id,init) with
      | (Some id, None) -> A.UnInit(storage,typ,id,attrs,mc ";")
      | (Some id, Some init) -> A.Init(storage,typ,id,attrs,mc "=",init,mc ";")
      | (_,_) -> raise (Debug.NotInC "Type: Unsupported in ast_cocci.")
    end
  in
  let base_decl =
    begin match decl with
    | B.DeclList([onedecl,_],_) -> onedecl_to_cocci onedecl
    | B.DeclList(declList,_) ->
      raise (Debug.NotInC "DeclList: Unsupported in ast_cocci.")
    | B.MacroDecl((stobis,str,args,_),_) ->
      let args_cocci = args_to_cocci args in
      let storage =
        match storagebis_to_cocci stobis with
        | None -> None
        | Some(s) -> Some (mc s)
      in
      A.MacroDecl(storage,mt (A.Id (mc str)),mc "(",mt args_cocci,mc ")",mc ",")
    | B.MacroDeclInit((stobis,str,args,init),_) ->
      let args_cocci = args_to_cocci args in
      let storage =
        match storagebis_to_cocci stobis with
        | None -> None
        | Some(s) -> Some (mc s)
      in
      A.MacroDeclInit(storage,mt (A.Id (mc str)), mc "(",mt args_cocci, mc ")",
                      mc "=",initialiser_to_cocci init,mc ",")
    end
  in mt base_decl

and logicalOp_to_cocci = function
  | B.Inf -> A.Inf
  | B.Sup -> A.Sup
  | B.InfEq -> A.InfEq
  | B.SupEq -> A.SupEq
  | B.Eq -> A.Eq
  | B.NotEq -> A.NotEq
  | B.AndLog -> A.AndLog
  | B.OrLog -> A.OrLog

and unaryOp_to_cocci = function
  | B.GetRef -> A.GetRef
  | B.DeRef -> A.DeRef
  | B.UnPlus -> A.UnPlus
  | B.UnMinus -> A.UnMinus
  | B.Tilde -> A.Tilde
  | B.Not -> A.Not
  | B.GetRefLabel -> A.GetRefLabel

and fixOp_to_cocci = function
  | B.Dec -> A.Dec
  | B.Inc -> A.Inc

and arithOp_to_cocci = function
  | B.Plus -> A.Plus
  | B.Minus -> A.Minus
  | B.Mul -> A.Mul
  | B.Div -> A.Div
  | B.Mod -> A.Mod
  | B.DecLeft -> A.DecLeft
  | B.DecRight -> A.DecRight
  | B.And -> A.And
  | B.Or -> A.Or
  | B.Xor -> A.Xor
  | B.Max -> A.Max
  | B.Min -> A.Min

and binaryOp_to_cocci (op,_) =
  match op with
  | B.Arith(aOp) -> mt (A.Arith ((mc (arithOp_to_cocci aOp))))
  | B.Logical(lOp) -> mt (A.Logical (mc (logicalOp_to_cocci lOp)))

and constant_to_cocci = function
  | B.String(str, isWchar) -> A.String(str, isWchar_to_cocci isWchar)
  | B.Char(str, isWchar) -> A.Char(str, isWchar_to_cocci isWchar)
  | B.Int(str,_) -> A.Int(str)
  | B.Float(str,_) -> A.Float(str)
  | B.DecimalConst(str1,str2,str3) -> A.DecimalConst(str1,str2,str3)
  | B.MultiString(_) ->
    raise (Debug.NotInC "Constant: Unsupported in ast_cocci")

and storagebis_to_cocci s : A.storage option =
  match s with
  | B.Sto(stoClass) -> Some
      begin match stoClass with
        | B.Auto -> A.Auto
        | B.Static -> A.Static
        | B.Register -> A.Register
        | B.Extern  -> A.Extern
      end
  | B.NoSto -> None
  | B.StoTypedef -> raise (Debug.NotInC "Storage: Unsupported in ast_cocci")

and name_to_id  = function
  | B.RegularName(str,_) -> mt (A.Id (mc str))
  | B.CppConcatenatedName(_)
  | B.CppVariadicName(_)
  | B.CppIdentBuilder(_) -> raise (Debug.NotInC "Weird variable name.")

and isWchar_to_cocci = function
  | B.IsWchar -> A.IsWchar
  | B.IsUchar -> A.IsUchar
  | B.Isuchar -> A.Isuchar
  | B.Isu8char -> A.Isu8char
  | B.IsChar -> A.IsChar

(**CFG node to ast_cocci**)
let cfg_node_to_ast_cocci (((node,_),_) as fullNode : CFG.node)
  : A.base_rule_elem option =
  try
    match node with
    (** Some nodes in the CFG are not real C instructions. *)
    | F.TopNode | F.EndNode
    | F.EndStatement(_)
    | F.Enter
    | F.Exit
    | F.Fake
    | F.CaseNode(_)
    | F.TrueNode(_)
    | F.FalseNode
    | F.InLoopNode
    | F.AfterNode(_)
    | F.FallThroughNode
    | F.LoopFallThroughNode
    | F.ErrorExit
      -> None

    (** The following nodes are not supported yet. *)
    | F.IfdefHeader(_)
    | F.IfdefElse(_)
    | F.IfdefEndif(_)
    | F.IfdefIteHeader(_)
    | F.DefineHeader(_)
    | F.DefineType(_)
    | F.DefineExpr(_)
    | F.DefineTodo
    | F.DefineInit _
    | F.Include(_)
    | F.DefineDoWhileZeroHeader(_)
    | F.PragmaHeader(_,_)
    | F.MacroTop(_,_,_)
    | F.Asm(_,_)
    | F.MacroStmt(_,_)
    | F.Exec(_,_)
    | F.ExprStatement(_,(None,_))
      ->
      raise (Debug.NotInC "CFG node to tree conversion not implemented for \
                           some constructions like #define and #ifdef.")

    (**The following nodes are not supported in ast_cocci**)
    | F.CaseRange(_,_)
    | F.DoWhileTail(_,_)
      -> None

    | F.FunHeader(def,_) ->
      let mcodekind = A.CONTEXT(A.NoPos, A.NOTHING) in
      let (ftype,(parameters,_)) = def.B.f_type in
      let params_wo_dots =
        List.map(fun (param,_) ->
            let ptype = type_to_cocci param.B.p_type in
            match param.B.p_namei with
            | Some(name) ->
              A.Param(ptype, Some(CH.to_wrap [CH.Local] (name_to_id name)))
            | None -> A.VoidParam(ptype)
          ) parameters
      in
      let accu = ref [] in
      List.iter(fun p ->
          accu := (mt p) ::!accu;
          accu := mt (A.PComma(mc ",")) :: !accu
        )params_wo_dots;
      let params_cocci = if !accu = [] then [] else
          List.rev (List.tl !accu)
      in
      let (storagebis,inline) = def.B.f_storage in
      let storage =
        match storagebis_to_cocci storagebis with
        | None -> None
        | Some(s) -> Some (A.FStorage(mc s))
      in
      let fninfo = [A.FType(type_to_cocci ftype)] in
      let fninfo = if inline then
          List.append fninfo [A.FInline(mc "inline")]
        else fninfo
      in
      let fninfo =
        match storage with
        | None -> fninfo
        | Some s  -> List.append fninfo [s]
      in
      Some (A.FunHeader(mcodekind,false, fninfo,
                        name_to_id def.B.f_name,
                        mc "(", mt params_cocci,
                        None, mc ")"))
    | F.Decl(decl) ->
      let mcodekind = A.CONTEXT(A.NoPos, A.NOTHING) in
      let base = A.DElem(mcodekind, false, (declaration_to_cocci decl)) in
      Some (A.Decl(mt base))
    | F.SeqStart(_,_,_) ->
      Some (A.SeqStart(mc "{"))
    | F.SeqEnd(_,_) ->
      Some (A.SeqStart(mc "}"))
    | F.ExprStatement(_,(Some(expr),_)) ->
      Some (A.ExprStatement(Some (expression_to_cocci expr), mc ";"))
    | F.IfHeader(_,(expr,_)) ->
      Some (A.IfHeader(mc "if", mc "(", expression_to_cocci expr, mc ")"))
    | F.Else(_) ->
      Some (A.Else(mc "else"))
    | F.WhileHeader(_,(expr,_)) ->
      Some (A.WhileHeader(mc "while", mc "(", expression_to_cocci expr, mc ")"))
    | F.DoHeader(_) ->
      Some (A.DoHeader(mc "do"))
    | F.ForHeader(_,((declOrExpr,(expr1,_),(expr2,_)),_)) ->
      let forinfo =
        match declOrExpr with
        | B.ForDecl(decl) ->
          let mcodekind = A.CONTEXT(A.NoPos, A.NOTHING) in
          let base = A.DElem(mcodekind,false,(declaration_to_cocci decl)) in
          A.ForDecl (mt base)
        | B.ForExp(Some(expr),_) ->
          A.ForExp (Some (expression_to_cocci expr),mc ";")
        | B.ForExp(None,_) ->
          A.ForExp (None,mc ";")
      in
      let expr1 = opt expression_to_cocci expr1 in
      let expr2 = opt expression_to_cocci expr2 in
      Some (A.ForHeader(mc "for",mc "(",forinfo,expr1,mc ";",expr2,mc ")"))
    | F.MacroIterHeader (_, ((name, args), _)) ->
      let args_cocci = args_to_cocci args in
      let ident = mt (A.Id (mc name)) in
      Some (A.IteratorHeader(ident, mc "(", mt args_cocci, mc ")"))
    | F.SwitchHeader(_,(expr,_)) ->
      Some (A.SwitchHeader(mc "switch",mc "(",expression_to_cocci expr,mc ")"))
    | F.Return(_,_) ->
      Some (A.Return(mc "return",mc ";"))
    | F.ReturnExpr(_,(expr,_)) ->
      Some (A.ReturnExpr(mc "return",expression_to_cocci expr,mc ";"))
    | F.Case(_,(expr,_)) ->
      Some (A.Case(mc "case",expression_to_cocci expr,mc ":"))
    | F.Default(_) ->
      Some (A.Default(mc "default", mc ":"))
    | F.Continue(_,_) ->
      Some (A.Continue(mc "continue",mc ";"))
    | F.Break(_,_,_) ->
      Some (A.Break(mc "break",mc ";"))
    | F.Label(_,name,_) ->
      Some (A.Label(name_to_id name, mc ":"))
    | F.Goto(_,name,_) ->
      Some (A.Goto(mc "goto",name_to_id name, mc ";"))
  with
  | Debug.NotInC(errorMessage) ->
    Printf.eprintf ("[WARNING] Unable to transform \"%s\" (%s).\n%!")
      (Cocci_addon.stringify_CFG_node fullNode) errorMessage;
    None

let forest_to_ast_cocci (nodes: S.nodes_type) :
        (A.rule_elem * Code.cfg_node_id) list list =
    match nodes with
    | S.TopLevel(node_list) ->
        let rule_lists = List.map (fun nodes ->
            List.fold_left (fun accu (node, node_info) ->
                match cfg_node_to_ast_cocci node with
                | None -> accu
                | Some(ast) ->
                    let cfg_id = node_info.Code.cfg_id in
                    (mt ast, cfg_id)::accu
            ) [] nodes
        ) node_list
        in
        (* Cluster can be empty if it composed of unsupported constructions *)
        List.filter (fun l -> l <> []) rule_lists
    | S.SubExpr(nodes) -> [
        List.fold_left (fun accu (expr, node_info) ->
            try
                let cfg_id = node_info.Code.cfg_id in
                (mt (A.Exp(expression_to_cocci expr)), cfg_id)::accu
            with Debug.NotInC(errorMessage) ->
                Printf.eprintf "Error in expr\n%!";
                accu
        ) [] nodes
    ]
