module Ast = Ast_cocci

(* Build a new ast_cocci (pattern) by comparing two ast_cocci.
   A metavariable is replaced when the two subtrees are not equal*)

(* --------------------------------------------------------------------- *)
(* compute the common prefix *)

let unify_mcode (x,_,_,_) (y,_,_,_) = x = y

let mt x = Ast.make_term x

let unify_lists f la lb =
  let m = ref [] in
  let l = List.map2(fun a b -> let (x,y) = f a b in
                     m := y :: !m; x
                   ) (Ast.unwrap la) (Ast.unwrap lb) in
  (mt l, List.flatten (List.rev !m))

let unify_option f t1 t2 =
  let t =
    match (t1,t2) with
    | (Some t1, Some t2) -> Some (f t1 t2)
    | (_, _) -> None
  in match t with
  | Some (t,m) -> (Some t,m)
  | None -> (None,[])


let bool_unify_option f t1 t2 =
  match (t1,t2) with
  | (Some t1, Some t2) -> f t1 t2
  | (None,None) -> true
  | _ -> false


(* --------------------------------------------------------------------- *)
(* Top-level code *)
let rec unify_rule_elem re1 re2 =
  Global.metacount := 0;
  let (base_rule_elem,meta_rule_elem) =
    match (Ast.unwrap re1, Ast.unwrap re2) with
    | (Ast.FunHeader(m1,b1,fi1,nm1,lp1,params1,va1,rp1),
       Ast.FunHeader(m2,b2,fi2,nm2,lp2,params2,va2,rp2)) ->
      let (nm,mi) = unify_ident nm1 nm2 in
      let (fi,mfi) = unify_fninfo fi1 fi2 in
      let (params,mp) =
        if List.length (Ast.unwrap params1) = List.length (Ast.unwrap params2)
        then
          unify_lists unify_parameterTypeDef params1 params2
        else
          let new_id = Global.fresh_metavar () in
          let meta_name = Ast.make_mcode ("", new_id) in
          let metavar = Ast.MetaParamList
              (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
          in
          (mt [mt metavar],
	   [(new_id,(Ast.ParamDotsTag(params1),Ast.ParamDotsTag(params2)))])
      in
      let m = List.append mi (List.append mfi mp) in
      (Ast.FunHeader(m1,b1,fi,nm,lp1,params,va1,rp1),m)
    | (Ast.Decl d1,Ast.Decl d2) ->
      let (d,m) = unify_annotated_decl d1 d2 in
      (Ast.Decl d,m)
    | (Ast.SeqStart(lb1),Ast.SeqStart(lb2)) -> (Ast.SeqStart(lb1),[])
    | (Ast.SeqEnd(rb1),Ast.SeqEnd(rb2)) -> (Ast.SeqEnd(rb1),[])
    | (Ast.ExprStatement(e1,s1),Ast.ExprStatement(e2,s2)) ->
      let (e,m) = unify_option unify_expression e1 e2 in
      (Ast.ExprStatement(e,s1),m)
    | (Ast.IfHeader(if1,lp1,e1,rp1),Ast.IfHeader(if2,lp2,e2,rp2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.IfHeader(if1,lp1,e,rp1),m)
    | (Ast.Else(e1),Ast.Else(e2)) -> (Ast.Else(e1),[])
    | (Ast.WhileHeader(wh1,lp1,e1,rp1),Ast.WhileHeader(wh2,lp2,e2,rp2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.WhileHeader(wh1,lp1,e,rp1),m)
    | (Ast.DoHeader(d1),Ast.DoHeader(d2)) -> (Ast.DoHeader(d1),[])
    | (Ast.WhileTail(wh1,lp1,e1,rp1,s1),Ast.WhileTail(wh2,lp2,e2,rp2,s2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.WhileTail(wh1,lp1,e,rp1,s1),m)
    | (Ast.ForHeader(fr1,lp1,first1,e21,s21,e31,rp1),
       Ast.ForHeader(fr2,lp2,first2,e22,s22,e32,rp2)) ->
      let (first,mfirst) = match (first1,first2) with
        | (Ast.ForExp(e11,s11),Ast.ForExp(e12,s1)) ->
          let (e1,me1) = unify_option unify_expression e11 e12 in
          (Ast.ForExp(e1,s11),me1)
        | (Ast.ForDecl d1,Ast.ForDecl d2) ->
          let (d,md) = unify_annotated_decl d1 d2 in
          (Ast.ForDecl(d),md)
        | _ -> failwith "unsupported in ForHeader"
      in
      let (e2,me2) = unify_option unify_expression e21 e22 in
      let (e3,me3) = unify_option unify_expression e31 e32 in
      let m = List.append mfirst (List.append me2 me3) in
      (Ast.ForHeader(fr1,lp1,first,e2,s21,e3,rp1),m)
    | Ast.IteratorHeader(id1, lp1, e1, rp1),
      Ast.IteratorHeader(id2, lp2, e2, rp2) ->
      let id, mident = unify_ident id1 id2 in
      (* Based on FunHeader *)
      let (e,mexpr) =
        if List.length (Ast.unwrap e1) = List.length (Ast.unwrap e2)
        then
          unify_lists unify_expression e1 e2
        else
          let new_id = Global.fresh_metavar () in
          let meta_name = Ast.make_mcode ("", new_id) in
          let metavar = Ast.MetaExprList
              (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
          in
          (mt [mt metavar],
	   [(new_id,(Ast.ExprDotsTag(e1),Ast.ExprDotsTag(e2)))])
      in
      (Ast.IteratorHeader(id, lp1, e, rp1), (mident@mexpr))
    | (Ast.Break(r1,s1),Ast.Break(r2,s2)) -> (Ast.Break(r1,s1),[])
    | (Ast.Continue(r1,s1),Ast.Continue(r2,s2)) -> (Ast.Continue(r1,s1),[])
    | (Ast.Label(l1,dd1),Ast.Label(l2,dd2)) ->
      let (l,m) = unify_ident l1 l2 in
      (Ast.Label(l,dd1),m)
    | (Ast.Goto(g1,l1,dd1),Ast.Goto(g2,l2,dd2)) ->
      let (l,m) = unify_ident l1 l2 in
      (Ast.Goto(g1,l,dd1),m)
    | (Ast.Return(r1,s1),Ast.Return(r2,s2)) -> (Ast.Return(r1,s1),[])
    | (Ast.ReturnExpr(r1,e1,s1),Ast.ReturnExpr(r2,e2,s2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.ReturnExpr(r1,e,s1),m)
    | (Ast.SwitchHeader(sw1,lp1,e1,rp1),Ast.SwitchHeader(sw2,lp2,e2,rp2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.SwitchHeader(sw1,lp1,e,rp1),m)
    | (Ast.Case(r1,e1,s1),Ast.Case(r2,e2,s2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.Case(r1,e,s1),m)
    | Ast.Exp(e1), Ast.Exp(e2) ->
      let e, m = unify_expression e1 e2 in
      (Ast.Exp(e), m)
    | (_,_) ->
      let new_id = Global.fresh_metavar () in
      let meta_name = Ast.make_mcode ("", new_id) in
      let metavar = Ast.MetaRuleElem(meta_name,Ast.CstrFalse,Ast.Unitary,false)
      in
      (metavar,
       [(new_id,(Ast.Rule_elemTag(re1),Ast.Rule_elemTag(re2)))])
  in (mt base_rule_elem, meta_rule_elem)

(**Only FType is handled, the others like FInline, FStorage are ignored*)
and unify_fninfo f1 f2 =
  let get_ftype f :Ast.fullType =
    try
      let ftype = List.hd f in
      match ftype with
      | Ast.FType(typ) -> typ
      | _ -> assert false
    with
    | _ -> failwith "Type of function is empty"
  in
  let ft1 = get_ftype f1 in
  let ft2 = get_ftype f2 in
  let (ft,mft) = unify_fullType ft1 ft2 in
  ([Ast.FType(ft)],mft)


and unify_annotated_decl d1 d2 =
  match (Ast.unwrap d1,Ast.unwrap d2) with
  |  (Ast.DElem(m1,b1,d1),Ast.DElem(m2,b2,d2)) ->
    let (d,m) = unify_declaration d1 d2 in
    (mt (Ast.DElem(m1,b1,d)),m)

(* --------------------------------------------------------------------- *)
(* Variable declaration *)
(* Even if the Cocci program specifies a list of declarations, they are
   split out into multiple declarations of a single variable each. *)

and unify_declaration d1 d2 =
  let (base_decl,m) =
    match (Ast.unwrap d1,Ast.unwrap d2) with
    | (Ast.Init(stg1,ft1,id1,attr1,eq1,i1,s1),
         Ast.Init(stg2,ft2,id2,attr2,eq2,i2,s2)) ->
        if bool_unify_option unify_mcode stg1 stg2 &&
           List.for_all2 unify_mcode attr1 attr2
        then
          let (ft,mft) = unify_fullType ft1 ft2 in
          let (id,mid) = unify_ident id1 id2 in
          let (i,mi) = unify_initialiser i1 i2 in
          let m = List.append mft (List.append mid mi) in
          (Ast.Init(stg1,ft,id,attr1,eq1,i,s1),m)
        else
          let new_id = Global.fresh_metavar () in
          (replace_by_metadecl new_id,
           [(new_id,(Ast.DeclarationTag(d1),Ast.DeclarationTag(d2)))])
      | (Ast.UnInit(stg1,ft1,id1,attr1,s1),Ast.UnInit(stg2,ft2,id2,attr2,s2)) ->
        if bool_unify_option unify_mcode stg1 stg2 &&
           List.for_all2 unify_mcode attr1 attr2
        then
          let (ft,mft) = unify_fullType ft1 ft2 in
          let (id,mid) = unify_ident id1 id2 in
          let m = List.append mft mid in
          (Ast.UnInit(stg1,ft,id,attr1,s1),m)
        else
          let new_id = Global.fresh_metavar () in
          (replace_by_metadecl new_id,
           [(new_id,(Ast.DeclarationTag(d1),Ast.DeclarationTag(d2)))])
      | (Ast.MacroDecl(s1,n1,lp1,args1,rp1,sem1),
         Ast.MacroDecl(s2,n2,lp2,args2,rp2,sem2)) ->
        if bool_unify_option unify_mcode s1 s2
        then
          let (n,mn) = unify_ident n1 n2 in
          let (args,margs) =
            if List.length (Ast.unwrap args1) = List.length (Ast.unwrap args2)
            then
              unify_lists unify_expression args1 args2
            else
              let new_id = Global.fresh_metavar () in
              let meta_name = Ast.make_mcode ("", new_id) in
              let metavar = Ast.MetaExprList
                  (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
              in
              (mt[mt metavar],
	       [(new_id,(Ast.ExprDotsTag(args1),Ast.ExprDotsTag(args2)))])
          in
          let m = List.append mn margs in
          (Ast.MacroDecl(s1,n,lp1,args,rp1,sem1),m)
        else
          let new_id = Global.fresh_metavar () in
          (replace_by_metadecl new_id,
           [(new_id,(Ast.DeclarationTag(d1),Ast.DeclarationTag(d2)))])
      | (Ast.MacroDeclInit(s1,n1,lp1,args1,rp1,eq1,ini1,sem1),
         Ast.MacroDeclInit(s2,n2,lp2,args2,rp2,eq2,ini2,sem2)) ->
        if bool_unify_option unify_mcode s1 s2
        then
          let (n,mn) = unify_ident n1 n2 in
          let (args,margs) =
            if List.length (Ast.unwrap args1) = List.length (Ast.unwrap args2)
            then
              unify_lists unify_expression args1 args2
            else
              let new_id = Global.fresh_metavar () in
              let meta_name = Ast.make_mcode ("", new_id) in
              let metavar = Ast.MetaExprList
                  (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
              in
              (mt[mt metavar],
	       [(new_id,(Ast.ExprDotsTag(args1),Ast.ExprDotsTag(args2)))])
          in
          let (ini,mini) = unify_initialiser ini1 ini2 in
          let m = List.append mn (List.append margs mini) in
          (Ast.MacroDeclInit(s1,n,lp1,args,rp1,eq1,ini,sem1),m)
        else
          let new_id = Global.fresh_metavar () in
          (replace_by_metadecl new_id,
           [(new_id,(Ast.DeclarationTag(d1),Ast.DeclarationTag(d2)))])
      | _ ->
        let new_id = Global.fresh_metavar () in
        (replace_by_metadecl new_id,
         [(new_id,(Ast.DeclarationTag(d1),Ast.DeclarationTag(d2)))])
  in
  (mt base_decl,m)

and replace_by_metadecl new_id=
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaDecl(meta_name,Ast.CstrFalse,Ast.Unitary,false)

(* --------------------------------------------------------------------- *)
(* Expression *)
and unify_expression e1 e2 =
  let (base_expr,m) =
    match (Ast.unwrap e1,Ast.unwrap e2) with
    | (Ast.Ident(i1),Ast.Ident(i2)) ->
      let (iden,m) = unify_ident i1 i2 in
      (Ast.Ident (iden),m)
    | (Ast.Constant(c1),Ast.Constant(c2))->
      if unify_mcode c1 c2 then (Ast.Constant(c1),[])
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metaexpr new_id,
         [(new_id,(Ast.ExpressionTag(e1),Ast.ExpressionTag(e2)))])
    | (Ast.StringConstant(lq1,str1,rq1,isWchar1),Ast.StringConstant(lq2,str2,rq2,isWchar2)) ->
      let (str,m) =
        if List.length (Ast.unwrap str1) = List.length (Ast.unwrap str2)
        then
          unify_lists unify_string_fragment str1 str2
        else
          let new_id = Global.fresh_metavar () in
          let meta_name = Ast.make_mcode ("", new_id) in
          let metavar = Ast.MetaFormatList(Ast.make_mcode "%",meta_name,
                                           Ast.AnyListLen, Ast.CstrFalse,
                                           Ast.Unitary,false) in
          (mt[mt metavar],[new_id,(Ast.SgrepStartTag("todo"),Ast.SgrepStartTag("todo"))])
          (**FIXME: Need to replace "todo" by something more relevant**)
      in
      (* FIXME: not sure if isWchar1 is fine *)
      (Ast.StringConstant(lq1,str,rq1,isWchar1),m)
    | (Ast.FunCall(f1,lp1,args1,rp1),Ast.FunCall(f2,lp2,args2,rp2)) ->
      let (f,mf) = unify_expression f1 f2 in
      let (args,margs) =
        let l1 = List.length (Ast.unwrap args1) in
        let l2 = List.length (Ast.unwrap args2) in
	let mk_meta _ =
	  let new_id = Global.fresh_metavar () in
          let meta_name = Ast.make_mcode ("", new_id) in
          let metavar = Ast.MetaExprList
              (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
          in
          (mt[mt metavar],
	   [(new_id,(Ast.ExprDotsTag(args1),Ast.ExprDotsTag(args2)))]) in
        if l1 = l2
        then
          begin
            if l1 = 1 then
              let f = List.hd (Ast.unwrap args1) in
              match Ast.unwrap f with
              | Ast.MetaExprList(nm,_,_,_,_) -> mk_meta()
              | _ -> unify_lists unify_expression args1 args2
            else
              unify_lists unify_expression args1 args2
          end
        else mk_meta()
      in
      let m = List.append mf margs in
      (Ast.FunCall(f,lp1,args,rp1),m)
    | (Ast.Assignment(l1,op1,r1,_),Ast.Assignment(l2,op2,r2,_)) ->
      let (l,ml) = unify_expression l1 l2 in
      let (op,mop) = unify_assignOp op1 op2 in
      let (r,mr) = unify_expression r1 r2 in
      let m = List.append ml (List.append mop mr) in
      (Ast.Assignment(l,op,r,false),m)
    | (Ast.Sequence(l1,c1,r1),Ast.Sequence(l2,c2,r2)) ->
      let (l,ml) = unify_expression l1 l2 in
      let (r,mr) = unify_expression r1 r2 in
      let m = List.append ml mr in
      (Ast.Sequence(l,c1,r),m)
    | (Ast.CondExpr(tst1,q1,thn1,c1,els1),Ast.CondExpr(tst2,q2,thn2,c2,els2)) ->
      let (tst,mtst) = unify_expression tst1 tst2 in
      let (thn,mthn) = unify_option unify_expression thn1 thn2 in
      let (els,mels) = unify_expression els1 els2 in
      let m = List.append mtst (List.append mthn mels) in
      (Ast.CondExpr(tst,q1, thn,c1,els),m)
    | (Ast.Postfix(e1,op1),Ast.Postfix(e2,op2)) ->
      if unify_mcode op1 op2
      then
        let (e,m) = unify_expression e1 e2 in
        (Ast.Postfix(e,op1),m)
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metaexpr new_id,
         [new_id,(Ast.ExpressionTag(e1),Ast.ExpressionTag(e2))])
    | (Ast.Infix(e1,op1),Ast.Infix(e2,op2)) ->
      if unify_mcode op1 op2 then
        let (e,m) = unify_expression e1 e2 in
        (Ast.Infix(e,op1),m)
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metaexpr new_id,
         [new_id,(Ast.ExpressionTag(e1),Ast.ExpressionTag(e2))])
    | (Ast.Unary(e1,op1),Ast.Unary(e2,op2)) ->
      if unify_mcode op1 op2
      then
        let (e,m) = unify_expression e1 e2 in
        (Ast.Unary(e,op1),m)
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metaexpr new_id,
         [new_id,(Ast.ExpressionTag(e1),Ast.ExpressionTag(e2))])
    | (Ast.Binary(l1,op1,r1),Ast.Binary(l2,op2,r2)) ->
      let (op,mop) = unify_binaryOp op1 op2 in
      let (l,ml) = unify_expression l1 l2 in
      let (r,mr) = unify_expression r1 r2 in
      let m = List.append ml (List.append mop mr) in
      (Ast.Binary(l,op,r),m)
    | (Ast.ArrayAccess(ar1,lb1,e1,rb1),Ast.ArrayAccess(ar2,lb2,e2,rb2)) ->
      let (ar,mar) = unify_expression ar1 ar2 in
      let (e,me) = unify_expression e1 e2 in
      let m = List.append mar me in
      (Ast.ArrayAccess(ar,lb1,e,rb1),m)
    | (Ast.RecordAccess(e1,d1,fld1),Ast.RecordAccess(e2,d2,fld2)) ->
      let (e,me) = unify_expression e1 e2 in
      let (fld,mfld) = unify_ident fld1 fld2 in
      let m = List.append me mfld in
      (Ast.RecordAccess(e,d1,fld),m)
    | (Ast.RecordPtAccess(e1,pt1,fld1),Ast.RecordPtAccess(e2,pt2,fld2)) ->
      let (e,me) = unify_expression e1 e2 in
      let (fld,mfld) = unify_ident fld1 fld2 in
      let m = List.append me mfld in
      (Ast.RecordPtAccess(e,pt1,fld),m)
    | (Ast.Cast(lp1,ty1,rp1,e1),Ast.Cast(lp2,ty2,rp2,e2)) ->
      let (ty,mty) = unify_fullType ty1 ty2 in
      let (e,me) = unify_expression e1 e2 in
      let m = List.append mty me in
      (Ast.Cast(lp1,ty,rp1,e),m)
    | (Ast.SizeOfExpr(szf1,e1),Ast.SizeOfExpr(szf2,e2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.SizeOfExpr(szf1,e),m)
    | (Ast.SizeOfType(szf1,lp1,ty1,rp1),Ast.SizeOfType(szf2,lp2,ty2,rp2)) ->
      let (ty,m) = unify_fullType ty1 ty2 in
      (Ast.SizeOfType(szf1,lp1,ty,rp1),m)
    | (Ast.TypeExp(ty1),Ast.TypeExp(ty2)) ->
      let (ty,m) = unify_fullType ty1 ty2 in
      (Ast.TypeExp(ty),m)
    | (Ast.Constructor(lp1,ty1,rp1,i1),Ast.Constructor(lp2,ty2,rp2,i2)) ->
      let (ty,mty) = unify_fullType ty1 ty2 in
      let (i,mi) = unify_initialiser i1 i2 in
      let m = List.append mty mi in
      (Ast.Constructor(lp1,ty,rp1,i),m)
    | (Ast.Paren(lp1,e1,rp1),Ast.Paren(lp2,e2,rp2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.Paren(lp1,e,rp1),m)
    | (Ast.EComma(c1),Ast.EComma(c2))->
      (Ast.EComma(c1),[])
    | (_,_) ->
      let new_id = Global.fresh_metavar () in
      (replace_by_metaexpr new_id,
       [new_id,(Ast.ExpressionTag(e1),Ast.ExpressionTag(e2))])
  in (mt base_expr,m)

and replace_by_metaexpr new_id =
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaExpr(meta_name,Ast.CstrFalse,Ast.Unitary,None,Ast.ANY,false,None)


(* --------------------------------------------------------------------- *)
(* Identifier *)

and unify_ident i1 i2 =
  match (Ast.unwrap i1,Ast.unwrap i2) with
  | (Ast.Id(m1),Ast.Id(m2)) ->
    if unify_mcode m1 m2 then (i1,[])
    else
      let new_id = Global.fresh_metavar () in
      let meta_name = Ast.make_mcode ("", new_id) in
      let metavar =  (Ast.MetaId(meta_name,Ast.CstrFalse,Ast.Unitary,false)) in
      (mt metavar, [(new_id,(Ast.IdentTag(i1),Ast.IdentTag(i2)))])
  | _ ->
    let new_id = Global.fresh_metavar () in
    let meta_name = Ast.make_mcode ("", new_id) in
    let metavar =  (Ast.MetaId(meta_name,Ast.CstrFalse,Ast.Unitary,false)) in
    (mt metavar, [(new_id,(Ast.IdentTag(i1),Ast.IdentTag(i2)))])

(* --------------------------------------------------------------------- *)
(* Strings *)

and unify_string_format e1 e2 =
  match (Ast.unwrap e1, Ast.unwrap e2) with
  | (Ast.ConstantFormat(str1), Ast.ConstantFormat(str2)) ->
    if unify_mcode str1 str2 then (e1,[])
    else
      let new_id = Global.fresh_metavar () in
      (mt (replace_by_metaformat new_id),
       [(new_id,(Ast.SgrepStartTag("todo"),
		 Ast.SgrepStartTag("todo")))])
       (*Did not find a more relevent Tag*)
  | (Ast.MetaFormat(name,_,_,_),_)
  | (_,Ast.MetaFormat(name,_,_,_)) ->
    let new_id = Global.fresh_metavar () in
    (mt (replace_by_metaformat new_id),
     [(new_id,(Ast.SgrepStartTag("todo"),
	       Ast.SgrepStartTag("todo")))])
(*Did not find a more relevent Tag*)

and replace_by_metaformat new_id =
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaFormat(meta_name,Ast.CstrFalse,Ast.Unitary,false)

and unify_string_fragment e1 e2 =
  match (Ast.unwrap e1, Ast.unwrap e2) with
  | (Ast.ConstantFragment(str1),Ast.ConstantFragment(str2)) ->
    if unify_mcode str1 str2 then (e1,[])
    else
      let new_id = Global.fresh_metavar () in
      let meta_name = Ast.make_mcode ("", new_id) in
      let metavar = Ast.MetaFormatList(Ast.make_mcode "%",meta_name,
                                        Ast.AnyListLen, Ast.CstrFalse,
                                        Ast.Unitary,false) in
      (mt metavar,
       ([new_id,(Ast.StringFragmentTag(e1),Ast.StringFragmentTag(e2))]))
  | (Ast.FormatFragment(pct1,fmt1),Ast.FormatFragment(pct2,fmt2)) ->
    let (fmt,m) = unify_string_format fmt1 fmt2 in
    (mt (Ast.FormatFragment(pct1,fmt)),m)
  | _ ->
    let new_id = Global.fresh_metavar () in
    let meta_name = Ast.make_mcode ("", new_id) in
    let metavar = Ast.MetaFormatList(Ast.make_mcode "%",meta_name,
                                     Ast.AnyListLen, Ast.CstrFalse,
                                     Ast.Unitary,false) in
    (mt metavar,
     ([new_id,(Ast.StringFragmentTag(e1),Ast.StringFragmentTag(e2))]))



(* --------------------------------------------------------------------- *)
(* Op *)

and unify_assignOp op1 op2 =
  match (Ast.unwrap op1, Ast.unwrap op2) with
  | (Ast.SimpleAssign op1', Ast.SimpleAssign op2') ->
    if unify_mcode op1' op2' then (op1,[])
    else
      let new_id = Global.fresh_metavar () in
      (mt (replace_by_metaassign new_id),
       [(new_id,(Ast.AssignOpTag(op1),Ast.AssignOpTag(op2)))])
  | (Ast.OpAssign op1', Ast.OpAssign op2') ->
    if unify_mcode op1' op2' then (op1,[])
    else
      let new_id = Global.fresh_metavar () in
      (mt (replace_by_metaassign new_id),
       [(new_id,(Ast.AssignOpTag(op1),Ast.AssignOpTag(op2)))])
  | _ ->
    let new_id = Global.fresh_metavar () in
    (mt (replace_by_metaassign new_id),
     [(new_id,(Ast.AssignOpTag(op1),Ast.AssignOpTag(op2)))])

and replace_by_metaassign new_id=
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaAssign(meta_name,Ast.CstrFalse,Ast.Unitary,false)

and unify_binaryOp op1 op2 =
  match (Ast.unwrap op1, Ast.unwrap op2) with
  | (Ast.Arith op1', Ast.Arith op2') ->
    if unify_mcode op1' op2' then (op1,[])
    else
      let new_id = Global.fresh_metavar () in
      (mt (replace_by_metabinary new_id),
       [(new_id,(Ast.BinaryOpTag(op1),Ast.BinaryOpTag(op2)))])
  | (Ast.Logical op1', Ast.Logical op2') ->
    if unify_mcode op1' op2' then (op1,[])
    else
      let new_id = Global.fresh_metavar () in
      (mt (replace_by_metabinary new_id),
       [(new_id,(Ast.BinaryOpTag(op1),Ast.BinaryOpTag(op2)))])
  | _ ->
    let new_id = Global.fresh_metavar () in
    (mt (replace_by_metabinary new_id),
     [(new_id,(Ast.BinaryOpTag(op1),Ast.BinaryOpTag(op2)))])

and replace_by_metabinary new_id=
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaBinary(meta_name,Ast.CstrFalse,Ast.Unitary,false)

(* --------------------------------------------------------------------- *)
(* Initialiser *)
and unify_initialiser i1 i2 =
  match (Ast.unwrap i1,Ast.unwrap i2) with
  | (Ast.InitExpr(expa),Ast.InitExpr(expb)) ->
    let (exp,m) = unify_expression expa expb in
    (mt (Ast.InitExpr(exp)),m)
  | _ ->
    let new_id = Global.fresh_metavar () in
    let meta_name = Ast.make_mcode ("", new_id) in
    let metaini = Ast.MetaInit(meta_name,Ast.CstrFalse,Ast.Unitary,false) in
    (mt metaini,[new_id,(Ast.InitTag(i1),Ast.InitTag(i2))])


(* --------------------------------------------------------------------- *)
(* Type *)

and unify_fullType ft1 ft2 =
  match (Ast.unwrap ft1,Ast.unwrap ft2) with
  | (Ast.Type(b1,cv1,ty1),Ast.Type(b2,cv2,ty2)) ->
    if bool_unify_option unify_mcode cv1 cv2
    then
      let (ty,mty) = unify_typeC ty1 ty2 in
      (mt (Ast.Type(b1,cv1,ty)),mty)
    else
      let new_id = Global.fresh_metavar () in
      let meta_type = Ast.Type(b1,None,mt (replace_by_metatype new_id)) in
      (mt meta_type, [(new_id,(Ast.FullTypeTag(ft1),Ast.FullTypeTag(ft2)))])
  | _ -> assert false


and unify_typeC t1 t2 =
  let (base_typeC,m) =
    match (Ast.unwrap t1,Ast.unwrap t2) with
    | Ast.BaseType (Ast.Unknown, _), _
    | _, Ast.BaseType (Ast.Unknown, _) ->
      (Ast.BaseType (Ast.Unknown, [Ast.make_mcode ""]),[])
    | (Ast.BaseType(ty1,stringsa),Ast.BaseType(ty2,stringsb)) ->
      if ty1 = ty2 then (Ast.BaseType(ty1,stringsa),[])
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metatype new_id,
	 [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
    | (Ast.Pointer(ty1,s1),Ast.Pointer(ty2,s2)) ->
      let (ty,m) = unify_fullType ty1 ty2 in
      (Ast.Pointer(ty,s1),m)
    | (Ast.FunctionPointer(tya,lp1a,stara,rp1a,lp2a,paramsa,rp2a),
      Ast.FunctionPointer(tyb,lp1b,starb,rp1b,lp2b,paramsb,rp2b)) ->
      if List.for_all2 unify_mcode
    	   [lp1a;stara;rp1a;lp2a;rp2a] [lp1b;starb;rp1b;lp2b;rp2b]
      then
        let (ty,mty) = unify_fullType tya tyb in
        let (params,mp) =
          if List.length (Ast.unwrap paramsa) = List.length (Ast.unwrap paramsb)
          then
            unify_lists unify_parameterTypeDef paramsa paramsb
          else
            let new_id = Global.fresh_metavar () in
            let meta_name = Ast.make_mcode ("", new_id) in
            let metavar = Ast.MetaParamList
                (meta_name,Ast.AnyListLen,Ast.CstrFalse,Ast.Unitary,false)
            in
            (mt[mt metavar],
	     [(new_id,(Ast.ParamDotsTag(paramsa),Ast.ParamDotsTag(paramsb)))])
      in
        let m = List.append mty mp in
        (Ast.FunctionPointer(ty,lp1a,stara,rp1a,lp2a,params,rp2a),m)
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metatype new_id,
	 [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
    | (Ast.Array(ty1,lb1,e1,rb1),Ast.Array(ty2,lb2,e2,rb2)) ->
      let (ty,mty) = unify_fullType ty1 ty2 in
      let (e,me) = unify_option unify_expression e1 e2 in
      let m = List.append mty me in
      (Ast.Array(ty,lb1,e,rb1),m)
    | (Ast.Decimal(dec1,lp1,len1,comma1,prec_opt1,rp1),
       Ast.Decimal(dec2,lp2,len2,comma2,prec_opt2,rp2)) ->
      let (len,mlen) = unify_expression len1 len2 in
      let (prec_opt,mp) = unify_option unify_expression prec_opt1 prec_opt2 in
      let m = List.append mlen mp in
      (Ast.Decimal(dec1,lp1,len,comma1,prec_opt,rp1),m)
    | (Ast.EnumName(s1,Some ts1),Ast.EnumName(s2,Some ts2)) ->
      let (ts,m) = unify_ident ts1 ts2 in
      (Ast.EnumName(s1, Some ts),m)
    | (Ast.EnumName(s1,None),Ast.EnumName(s2,None)) ->
      (Ast.EnumName(s1,None),[])
    | (Ast.StructUnionName(s1,Some ts1),Ast.StructUnionName(s2,Some ts2)) ->
      if unify_mcode s1 s2
      then
        let (ts,m) = unify_ident ts1 ts2 in
        (Ast.StructUnionName (s1, Some ts),m)
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metatype new_id,
	 [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
    | (Ast.StructUnionName(s1,None),Ast.StructUnionName(s2,None)) ->
      if unify_mcode s1 s2
      then
        (Ast.StructUnionName(s1,None),[])
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metatype new_id,
	 [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
    | (Ast.TypeOfExpr(szf1,lp1,e1,rp1),Ast.TypeOfExpr(szf2,lp2,e2,rp2)) ->
      let (e,m) = unify_expression e1 e2 in
      (Ast.TypeOfExpr(szf1,lp1,e,rp1),m)
    | (Ast.TypeOfType(szf1,lp1,ty1,rp1),Ast.TypeOfType(szf2,lp2,ty2,rp2)) ->
      let (ty,m) = unify_fullType ty1 ty2 in
      (Ast.TypeOfType(szf1,lp1,ty,rp1),m)
    | (Ast.TypeName(tn1),Ast.TypeName(tn2)) ->
      if unify_mcode tn1 tn2 then (Ast.TypeName(tn1),[])
      else
        let new_id = Global.fresh_metavar () in
        (replace_by_metatype new_id,
	 [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
    | _ ->
      let new_id = Global.fresh_metavar () in
      (replace_by_metatype new_id,
       [(new_id,(Ast.TypeCTag(t1),Ast.TypeCTag(t2)))])
  in
  (mt base_typeC,m)

and unify_parameterTypeDef p1 p2 =
  match (Ast.unwrap p1,Ast.unwrap p2) with
  | (Ast.VoidParam(ft1),Ast.VoidParam(ft2)) ->
    let (ft,m) = unify_fullType ft1 ft2 in
    (mt (Ast.VoidParam(ft)),m)
  | (Ast.Param(ft1,i1),Ast.Param(ft2,i2)) ->
    let (ft,mft) = unify_fullType ft1 ft2 in
    let (i,mi) = unify_option unify_ident i1 i2 in
    let m = List.append mft mi in
    (mt (Ast.Param(ft,i)),m)
  | (Ast.PComma(c1),Ast.PComma(c2)) ->
    (mt (Ast.PComma(c1)),[])
  | _ ->
    let new_id = Global.fresh_metavar () in
    let meta_name = Ast.make_mcode ("", new_id) in
    let metaparam = Ast.MetaParam(meta_name,Ast.CstrFalse,Ast.Unitary,false) in
    (mt metaparam,[new_id,(Ast.ParamTag(p1),Ast.ParamTag(p2))])

and replace_by_metatype new_id =
  let meta_name = Ast.make_mcode ("", new_id) in
  Ast.MetaType(meta_name,Ast.CstrFalse,Ast.Unitary,false)
