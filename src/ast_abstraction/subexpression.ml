module A = Ast_c
module CFC = Control_flow_c
module AC = Ast_cocci

type nodes_type =
    | TopLevel of (CFC.node * Code.node_info) list list
    | SubExpr of (A.expression * Code.node_info) list


let fake_info = ref (None, A.Test)

let check_same_expr (expr1: A.expression) (expr2: A.expression) =
    match fst (fst expr1), fst (fst expr2) with
    | A.Ident _, A.Ident _ -> true
    | A.Constant _, A.Constant _ -> true
    | A.StringConstant _, A.StringConstant _ -> true
    | A.FunCall (_, l1), A.FunCall (_, l2)
        when List.length l1 = List.length l2 -> true
    | A.Assignment _, A.Assignment _ -> true
    | A.Sequence _, A.Sequence _ -> true
    | A.CondExpr _, A.CondExpr _ -> true
    | A.Postfix _, A.Postfix _ -> true
    | A.Infix _, A.Infix _ -> true
    | A.Unary _, A.Unary _ -> true
    | A.Binary _, A.Binary _ -> true
    | A.ArrayAccess _, A.ArrayAccess _ -> true
    | A.RecordAccess _, A.RecordAccess _ -> true
    | A.RecordPtAccess _, A.RecordPtAccess _ -> true
    | A.Cast _, A.Cast _ -> true
    | A.SizeOfExpr _, A.SizeOfExpr _ -> true
    | A.SizeOfType _, A.SizeOfType _ -> true
    | A.Constructor _, A.Constructor _ -> true
    | _ -> false


let check_same_root node1 node2 =
    match (CFC.unwrap node1, CFC.unwrap node2) with
    | CFC.FunHeader _, CFC.FunHeader _ -> true
    | CFC.Decl _, CFC.Decl _ -> true
    | CFC.SeqStart _, CFC.SeqStart _ -> true
    | CFC.SeqEnd _, CFC.SeqEnd _ -> true
    | CFC.ExprStatement (_, (opt_expr1, _)), CFC.ExprStatement (_, (opt_expr2, _)) ->
        begin match opt_expr1, opt_expr2 with
        | None, None -> true
        | Some expr1, Some expr2 -> check_same_expr expr1 expr2
        | _ -> false
        end
    | CFC.IfHeader _, CFC.IfHeader _ -> true
    | CFC.Else _, CFC.Else _ -> true
    | CFC.WhileHeader _, CFC.WhileHeader _ -> true
    | CFC.DoHeader _, CFC.DoHeader _ -> true
    | CFC.ForHeader _, CFC.ForHeader _ -> true
    | CFC.MacroIterHeader _, CFC.MacroIterHeader _ -> true
    | CFC.SwitchHeader _, CFC.SwitchHeader _ -> true
    | CFC.Break _, CFC.Break _ -> true
    | CFC.Continue _, CFC.Continue _ -> true
    | CFC.Label _, CFC.Label _ -> true
    | CFC.Goto _, CFC.Goto _ -> true
    | CFC.Return _, CFC.Return _ -> true
    | CFC.ReturnExpr _, CFC.ReturnExpr _ -> true
    | CFC.Exec _, CFC.Exec _ -> true
    | CFC.Include _, CFC.Include _ -> true
    | CFC.DefineHeader _, CFC.DefineHeader _ -> true
    | CFC.Case _, CFC.Case _ -> true
    | CFC.Default _, CFC.Default _ -> true
    | _, _ -> false


let extract_funcall node : A.expression list =
    let asts = ref [] in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.kexpr = (fun (k, bigf) (((exp, typ), ii) as expression )->
            begin match exp, ii with
            | A.FunCall (e, es), [i1;i2] ->
                asts := expression::!asts;
                k expression
            | _ -> ()
            end;
		    k expression
        );
    }
    in
    Visitor_c.vk_node bigf node;
    !asts


let remove_funcall (node: CFC.node) : CFC.node =
    let bigf = {Visitor_c.default_visitor_c_s with
        Visitor_c.kexpr_s = (fun (k, bigf) (((exp, _), ii) as expression )->
            match exp, ii with
            | A.FunCall (e, es), [i1;i2] ->
                let exp =
                    A.Constant(A.String("___SPINFER_FAKE_FUN___", A.IsChar))
                in
                (exp, fake_info), []
            | _ -> k expression
        );
    }
    in
    Visitor_c.vk_node_s bigf node


let unify_funcalls (f_list : A.expression list list) : (A.expression list) option =
    let same_name f1 f2 = match fst f1, fst f2 with
    | (A.FunCall(((e1, _), _), _), _), (A.FunCall(((e2, _), _), _), _) ->
        begin match e1, e2 with
        | A.Ident(A.RegularName(n1, _)), A.Ident(A.RegularName(n2, _))
            when n1 = n2 -> true
        | _ -> false
        end
    | _ -> assert false
    in
    match f_list with
    | [] -> None
    | head::[] ->
        begin match head with
        | [] -> None
        | one::[] -> Some([one])
        | e_head::tail -> None
        end
    | head::tail ->
        let all_compatible = List.fold_left (fun accu elt ->
            let res = List.fold_left (fun accu funcalls ->
                match accu, funcalls with
                | Some(l1), l2 ->
                    begin try
                        let correct_funcall = List.find (fun elt2 ->
                            List.for_all (same_name elt2) l1
                        ) l2
                        in
                        Some(correct_funcall::l1)
                    with Not_found ->
                        None
                    end
                | _ -> None
            ) (Some [elt]) tail
            in
            match res with
            | None -> accu
            | Some(l) -> (List.rev l)::accu
        ) [] head
        in
        begin match all_compatible with
        | [] -> None
        | one::[] -> Some(one)
        | head::tail ->
            (* TODO: Fix this case *)
            Printf.eprintf "Multiple possible funcalls, can be ambiguous\n%!";
            None
        end

let node_by_root (node_list: (CFC.node * 'a) list)
        : (CFC.node * 'a) list list =
    Utils.partition_list node_list (fun (a, _) (b, _) -> check_same_root a b)


let transform_cluster (clust: Code.forest) : nodes_type =
    (* /!\ Warning:  pack(unpack(a)) ≠ a *)
    let unpack (node: Code.forest_node) : (CFC.node * 'a) =
        let (_, cfc_node), node_info = node in
        cfc_node, node_info
    in
    let pack cfc_node node_info =
        cfc_node, node_info
    in
    let nodes, extra = List.split (List.map unpack clust) in
    let same_nodes = List.for_all (check_same_root (List.hd nodes)) nodes in
    let funcalls = List.map extract_funcall nodes in
    match unify_funcalls funcalls, same_nodes with
    | Some(l), false -> SubExpr(List.map2 pack l extra)
    | _, true -> TopLevel([List.map2 pack nodes extra])
    | _, false -> TopLevel(node_by_root (List.map2 pack nodes extra))


let toplevel_of_forest (forest: Code.forest) : nodes_type =
    let new_nodes = List.map (fun ((_, node), info) ->
        node, info
    ) forest
    in
    TopLevel(node_by_root new_nodes)


let same_context ((sta1, sub1): AC.rule_elem * AC.expression)
        ((sta2, sub2): AC.rule_elem * AC.expression) : bool =
    let rebuild_expr to_match _ k expr =
        if expr = to_match
        then
            let new_expr =
                AC.make_term (AC.Ident(AC.make_term (AC.Id(AC.make_mcode "TEST"))))
            in
            new_expr
        else k expr
    in
    let mcode mc = mc in
    let donothing r k e = k e in
    let recursor1 =
        Visitor_ast.rebuilder
        mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        donothing (rebuild_expr sub1) donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        donothing donothing
    in
    let rebuild1 = recursor1.Visitor_ast.rebuilder_rule_elem sta1 in
    let recursor2 =
        Visitor_ast.rebuilder
        mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        donothing (rebuild_expr sub2) donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        donothing donothing
    in
    let rebuild2 = recursor2.Visitor_ast.rebuilder_rule_elem sta2 in
    rebuild1 = rebuild2