module A = Ast_c
module V = Visitor_c
module CFC = Control_flow_c
module AA = Abstract_ast


module ContextSet = Set.Make(struct
    type t = int * string
    let compare = Pervasives.compare
end)

let context_dependent_nodes : (int * int, string) Hashtbl.t = Hashtbl.create 100


let extract_local_expr (node: CFC.node) : string list =
    let strings = ref [] in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.kexpr = (fun (k, bigf) (((exp, info), _) as expression) ->
            begin match fst !info with
            | Some(_, A.LocalVar _) | Some(_, A.StaticLocalVar _) ->
                begin match exp with
                | A.Ident(A.RegularName(s, _)) -> strings := s::!strings
                | _ -> ()
                end;
            | _ -> ()
            end;
		    k expression
        );
    }
    in
    Visitor_c.vk_node bigf node;
    !strings


let extract_local_param (node: CFC.node) : string list =
    let strings = ref [] in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.kparam = (fun (k, bigf) param ->
            begin match param.A.p_namei with
            | Some(A.RegularName(s, _)) -> strings := s::!strings
            | _ -> ()
            end;
		    k param
        );
    }
    in
    Visitor_c.vk_node bigf node;
    !strings


let extract_local_decl (node: CFC.node) : string list =
    let strings = ref [] in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.konedecl_opt = (fun _ (k, bigf) decl ->
            begin match decl.A.v_namei with
            | Some(A.RegularName(s, _), _) -> strings := s::!strings
            | _ -> ()
            end;
            k decl
        );
    }
    in
    Visitor_c.vk_node bigf node;
    !strings


let extract_local_all (node: CFC.node) : string list =
    extract_local_expr node @ extract_local_param node @ extract_local_decl node


let prepare (before: Code.forest) (after: Code.forest) : unit =
    let wrap info = List.map (fun a -> (fst info.Code.cfg_id), a) in
    let removed_idents = List.fold_left (fun accu ((_, elt), info) ->
        wrap info (extract_local_all elt) @ accu
    ) [] before
    in
    let added_idents = List.fold_left (fun accu ((_, elt), info) ->
        wrap info (extract_local_all elt) @ accu
    ) [] after
    in
    let removed_decl_idents = List.fold_left (fun accu ((_, elt), info) ->
        wrap info (extract_local_decl elt) @ accu
    ) [] before
    in
    let added_decl_idents = List.fold_left (fun accu ((_, elt), info) ->
        wrap info (extract_local_decl elt) @ accu
    ) [] after
    in

    let decl_indent = ContextSet.union
        (ContextSet.of_list added_decl_idents)
        (ContextSet.of_list removed_decl_idents)
    in
    ContextSet.iter (fun (i, name) ->
        Hashtbl.add Global.in_decl_ident i name
    ) decl_indent;

    let purely_added = ContextSet.diff
        (ContextSet.of_list added_decl_idents)
        (ContextSet.of_list removed_decl_idents)
    in
    ContextSet.iter (fun (i, name) ->
        Hashtbl.add Global.added_idents i name
    ) purely_added;

    let context_to_find : ContextSet.t = ContextSet.diff
        (ContextSet.of_list added_idents)
        (ContextSet.union (ContextSet.of_list removed_idents) purely_added)
    in
    List.iter (fun ((_, elt), info) ->
        let gid, nid = info.Code.cfg_id in
        List.iter (fun ident ->
            if ContextSet.mem (gid, ident) context_to_find
            then begin
                Hashtbl.add context_dependent_nodes (gid, nid) ident;
                Hashtbl.add Global.context_idents gid ident
            end
        ) (extract_local_all elt)
    ) after


let find_function_headers (both: Code.forest) (context: ContextSet.t)
        : Code.forest option =
    let node_in_context node info =
        let gid, _ = info.Code.cfg_id in
        List.exists (fun name ->
            ContextSet.mem (gid, name) context
        ) (extract_local_param node)
    in
    let function_header_nodes = List.filter (fun ((_, node), info) ->
        match CFC.unwrap node with
        | CFC.FunHeader _ when node_in_context node info -> true
        | _ -> false
    ) both
    in
    let found_context = List.fold_left (fun accu ((_, node), info) ->
        let gid, _ = info.Code.cfg_id in
        List.fold_left (fun accu name ->
            ContextSet.add (gid, name) accu
        ) accu (extract_local_param node)
    ) ContextSet.empty function_header_nodes
    in
    if
        ContextSet.subset context found_context
        &&
        not (ContextSet.is_empty context)
    then Some(function_header_nodes)
    else None


let find_new_decl (both: Code.forest) (context: ContextSet.t)
        : Code.forest option =
    let node_in_context node info =
        let gid, _ = info.Code.cfg_id in
        List.exists (fun name ->
            ContextSet.mem (gid, name) context
        ) (extract_local_decl node)
    in
    let decl_nodes = List.filter (fun ((_, node), info) ->
        match CFC.unwrap node with
        | CFC.Decl _ when node_in_context node info -> true
        | _ -> false
    ) both
    in
    let found_context = List.fold_left (fun accu ((_, node), info) ->
        let gid, _ = info.Code.cfg_id in
        List.fold_left (fun accu name ->
            ContextSet.add (gid, name) accu
        ) accu (extract_local_decl node)
    ) ContextSet.empty decl_nodes
    in
    if
        ContextSet.subset context found_context
        &&
        not (ContextSet.is_empty context)
    then Some(decl_nodes)
    else None


let fit_context_to_patterns (both: Code.forest) (patterns: AA.pattern list)
        : Code.forest list =
    let get_context_opt (pattern: Abstract_ast.pattern)
            : (Code.forest option * Code.forest option) =
        let wrap gid = List.map (fun a -> gid, a) in
        let to_find = AA.NodeIdSet.fold (fun (gid, nid) accu ->
            wrap gid (Hashtbl.find_all context_dependent_nodes (gid, nid)) @ accu
        ) pattern.AA.matched_nodes []
        in
        (
            find_function_headers both (ContextSet.of_list to_find),
            find_new_decl both (ContextSet.of_list to_find)
        )
    in
    List.fold_left (fun accu pattern ->
        let func, decl = get_context_opt pattern in
        Utils.add_opt_to_list decl (Utils.add_opt_to_list func accu)
    ) [] patterns