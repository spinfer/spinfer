module Ast = Ast_cocci

val unify_rule_elem: Ast.rule_elem -> Ast.rule_elem
  -> Ast.rule_elem * (string * (Ast.anything * Ast.anything)) list
