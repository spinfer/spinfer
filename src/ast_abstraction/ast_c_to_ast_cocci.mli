module A = Ast_cocci
module B = Ast_c
module F = Control_flow_c
module S = Subexpression

val forest_to_ast_cocci : S.nodes_type
    -> (A.rule_elem * Code.cfg_node_id) list list
