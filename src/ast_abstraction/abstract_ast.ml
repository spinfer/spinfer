module Ast = Ast_cocci
module RA = Refine_ast

module NodeIdSet = Set.Make(struct
    type t = Code.cfg_node_id
    let compare = Pervasives.compare
  end)

module IdentSet = Set.Make(struct
    type t = Ast.base_ident
    let compare = Pervasives.compare
end)

(**
   Each metavariable is defined by a list of items it abstracts
 **)
module Metavariable = Set.Make(struct
    type t = Code.cfg_node_id * Ast.anything
    let compare = Pervasives.compare
  end)

type cocci_cluster = (Ast.rule_elem * Code.cfg_node_id) list

type pattern = {
  ast : Ast.rule_elem;
  matched_nodes : NodeIdSet.t;
  metavars: (string * Metavariable.t) list;
}

(**
   Extract the common pattern from a cluster
*)

let rec build_common_ast list_asts_ids : Ast.rule_elem option =
    let transform_local elt gid =
        (* Filter out new identifiers to keep those only related to the current
         * function.
         *)
        let transform ident =
            let is_in = RA.in_table ident gid in
            if is_in Global.added_idents
            then RA.NoTransform
            else
                if is_in Global.context_idents || is_in Global.in_decl_ident
                then RA.Identifier
                else RA.Expression
        in
        RA.local_id_to_meta transform elt
    in
    List.fold_left (fun rest (elt, (gid, _)) ->
        let elt = transform_local elt gid in
        match rest with
        | None -> Some(elt)
        | Some(rest) -> Some(fst (Unify_ast_cocci.unify_rule_elem rest elt))
    ) None list_asts_ids

let union_two_metavars meta1 meta2 =
    if meta2 = []
    then meta1
    else
        List.map2 (fun (nm1, m1) (nm2, m2) ->
	        if nm1 = nm2
	        then nm1, Metavariable.union m1 m2
	        else failwith "inconsistent names"
        ) meta1 meta2

let rec union_metavars common_ast list_asts_ids
        : Ast.rule_elem * ((string * Metavariable.t) list) =
    List.fold_left (fun (_, metavars) (ast, ids) ->
        (* New common ast is the same ast but metavar names can change *)
        let new_common_ast, matched_items =
            Unify_ast_cocci.unify_rule_elem common_ast ast
        in
        let new_metavars = List.map (fun (nm, (_, a)) ->
            (nm, Metavariable.singleton (ids, a))
        ) matched_items
        in
        new_common_ast, union_two_metavars new_metavars metavars
    ) (common_ast, []) list_asts_ids


(*********************************************
 *  SPINFER pattern transformation workflow  *
 *********************************************
 * INPUT:  One Ast_c cluster as found by Python clustering
 *               |
 *               v
 *         Multiple Ast_c specialized clusters:
 *         1. If all members in previous cluster share the same root then
 *         it's a top level unique cluster.
 *         2. If all members share the a common subexpression then
 *         it's a subexpression unique cluster.
 *         3. Otherwise it is multiple top level clusters, one cluster for each
 *         type of root.
 *               |
 *               v
 *         Multiple Ast_Cocci clusters
 *               |
 *               v
 *         Each Ast_Cocci is split into more cluster if member contains
 *         different set of newly declared identifiers.
 *         If `+ int k;` is found then `k` is considered a newly declared
 *         identifier in all added members in the same function in where `k` was
 *         declared.
 *         Two members with set of different newly declared identifier
 *         absolutely need to be in different pattern otherwise the identifiers
 *         will be transformed as a metavariable that can't be binded to anything.
 *              |
 *              v
 * OUTPUT: Multiple SPINFER patterns
 *)


let c_to_cocci_clusters (allow_subexpr: bool) (c_cluster: Code.forest)
        : cocci_cluster list =
    let transformed_cluster =
        if allow_subexpr
        then Subexpression.transform_cluster c_cluster
        else Subexpression.toplevel_of_forest c_cluster
    in
    Ast_c_to_ast_cocci.forest_to_ast_cocci transformed_cluster


let extract_patterns (cocci_cluster: cocci_cluster): pattern list =
    let augmented_ast_ids = List.map (fun (a, (gid, nid)) ->
        let all_ids = Refine_ast.collect_all_ids a in
        let separate_ids = List.filter (fun id ->
            RA.in_table id gid Global.added_idents
            ||
            RA.in_table id gid Global.context_idents
        ) all_ids
        in
        IdentSet.of_list separate_ids, (a, (gid, nid))
    ) cocci_cluster
    in
    let same_ident (i1, _) (i2, _) = i1 = i2 in
    let compatible_ast_ids =
        List.map Utils.assoc_right (Utils.partition_list augmented_ast_ids same_ident)
    in
    if compatible_ast_ids = []
    then []
    else
        List.map (fun list_ast_ids ->
            let ids = Utils.assoc_right list_ast_ids in
            let common_ast = match build_common_ast list_ast_ids with
            | None -> failwith "Fails when building the common ast"
            | Some (ca) -> ca
            in
            let matched_nodes = NodeIdSet.of_list ids in
            let new_common_ast, metavars =
                union_metavars common_ast list_ast_ids
            in
            (* Removing hack annotations.
             * This is very important because identifiers in decl do not have
             * the same annotation as identifiers elsewhere, so that
             * metavariable unification can fail as it is using standard `=`
             * operator.
             *)
            let cleaned_metavars = List.map (fun (name, meta) ->
                name,
                Metavariable.map (fun (i, ast) ->
                    i, Cocci_hack.clear_all_hacks ast
                ) meta
            ) metavars
            in
            {
                ast = new_common_ast;
                matched_nodes = matched_nodes;
                metavars = cleaned_metavars;
            }
        ) compatible_ast_ids


let mine_patterns (clusters : Code.forest list) : pattern list =
    let cocci_clusters =
        List.flatten (List.map (c_to_cocci_clusters true) clusters)
    in
    List.flatten (List.map extract_patterns cocci_clusters)


let pattern_from_ids (ids: (int * int) list) (code: Code.t)
        (allow_subexpr: bool) : pattern =
    let forest = List.map (fun (gid, nid) ->
        let graph, _, _ =
            List.find (fun (_, _, id) -> id = gid) code
        in
        let diff_node =
            Control_flow_c.KeyMap.find nid (graph#nodes)
        in
        let node_info = {
            Code.source_files = Global.id_to_filenames gid;
            Code.cfg_id = (gid, nid)
        }
        in
        (diff_node, node_info)
    ) ids
    in
    let cocci_clusters =
        c_to_cocci_clusters allow_subexpr forest
    in
    match cocci_clusters with
    | cluster::[] ->
        begin match extract_patterns cluster with
        | pattern::[] -> pattern
        | _ -> raise (Failure("Too generic cluster"))
        end
    | _ -> raise (Failure("Too generic pattern"))


let rule_is_toplevel (rule: Ast.rule_elem) : bool =
    match rule.Ast.node with
    | Ast.Exp _ -> false
(*
    | Ast.Ty _ -> false
*)
    | _ -> true
