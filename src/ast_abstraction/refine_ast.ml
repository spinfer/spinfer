module CH = Cocci_hack
module V = Visitor_ast
module A = Ast_cocci

let bind x y = x @ y
let option_default = []
let mcode r m = option_default
let donothing recursor k e = k e (* just combine in the normal way *)

type transform_type =
    | NoTransform
    | Expression
    | Identifier

let in_table (ident: A.base_ident) (gid: int) (table: (int, string) Hashtbl.t)
        : bool =
    match ident with
    | A.Id(name) -> List.mem (A.unwrap_mcode name) (Hashtbl.find_all table gid)
    | _ -> false


let get_metaid () =
    let name = A.make_mcode ("", Global.fresh_metavar ()) in
    A.make_term (A.MetaId(name, A.CstrFalse, A.Unitary, false))


let collect_new_ids rule_elem : A.base_ident list =
    let collected id =
        if List.mem CH.NewIdent (CH.of_wrap id)
        then [A.unwrap id]
        else option_default
    in
    let collector _ k id = bind (k id) (collected id) in
    let recursor =
        V.combiner bind option_default
        mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        collector donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
        donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
    in
    recursor.V.combiner_rule_elem rule_elem


let collect_all_ids rule_elem : A.base_ident list =
    let collected id =
        [A.unwrap id]
    in
    let collector _ k id = bind (k id) (collected id) in
    let recursor =
        V.combiner bind option_default
        mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        collector donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
        donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
    in
    recursor.V.combiner_rule_elem rule_elem


let local_id_to_meta (trans_fun: A.base_ident -> transform_type)
        (rule_elem: A.rule_elem) : A.rule_elem =
    let transform_exp _ k exp =
        let exp = match A.unwrap exp with
        | A.Ident(id) ->
            let is_local = List.mem CH.Local (CH.of_wrap id) in
            if is_local
            then match trans_fun (A.unwrap id) with
            | Expression ->
                let meta_name = A.make_mcode ("", Global.fresh_metavar ()) in
                { exp with
                    A.node = A.MetaExpr(meta_name, A.CstrFalse,
                        A.Unitary,None,A.ANY,false,None
                    );
                }
            | Identifier ->
                { exp with A.node = A.Ident(get_metaid ()); }
            | NoTransform -> exp
            else
                exp
        | _ -> exp
        in
        k exp
    in
    let transform_param _ k param =
        let param = match A.unwrap param with
        | A.Param(ty, Some(id)) ->
            let is_local = List.mem CH.Local (CH.of_wrap id) in
            if is_local
            then match trans_fun (A.unwrap id) with
            | Expression | Identifier ->
                { param with A.node = A.Param(ty, Some(get_metaid ())); }
            | NoTransform -> param
            else
                param
        | _ -> param
        in
        k param
    in
    let transform_decl _ k decl =
        let decl = match A.unwrap decl with
        | A.UnInit(a1, a2, id, a4, a5) ->
            begin match trans_fun (A.unwrap id) with
            | Expression | Identifier ->
                { decl with
                    A.node = A.UnInit(a1, a2, get_metaid (), a4, a5);
                }
            | NoTransform -> decl
            end
        | A.Init(a1, a2, id, a4, a5, a6, a7) ->
            begin match trans_fun (A.unwrap id) with
            | Expression | Identifier ->
                { decl with
                    A.node = A.Init(a1, a2, get_metaid (), a4, a5, a6, a7);
                }
            | NoTransform -> decl
            end
        | _ -> decl
        in
        k decl
    in

    let mcode mc = mc in
    let recursor =
        V.rebuilder
        mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        donothing transform_exp donothing donothing donothing donothing
        donothing donothing donothing transform_param donothing transform_decl
        donothing donothing donothing donothing donothing donothing
        donothing donothing
    in
    recursor.V.rebuilder_rule_elem rule_elem
