module Ast = Ast_cocci

module NodeIdSet : Set.S with type elt = Code.cfg_node_id
module Metavariable : Set.S with type elt = Code.cfg_node_id * Ast.anything

type pattern = {
  ast : Ast.rule_elem;
  matched_nodes : NodeIdSet.t;
  metavars: (string * Metavariable.t) list;
}

val mine_patterns : Code.forest list -> pattern list
val pattern_from_ids : (int * int) list ->  Code.t -> bool -> pattern
val rule_is_toplevel : Ast.rule_elem -> bool