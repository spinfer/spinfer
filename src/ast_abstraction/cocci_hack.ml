module A = Ast_cocci
module V = Visitor_ast

type property =
    | NewIdent
    | Local


let hackname = "##HACK##"


let property_to_string = function
    | NewIdent -> "NewIdent"
    | Local -> "Local"


let string_to_property = function
    | "NewIdent" -> NewIdent
    | "Local" -> Local
    | _ -> assert false


let properties_to_metanames (properties: property list) : A.meta_name list =
    List.map (fun prop -> (hackname, property_to_string prop)) properties


let metanames_to_properties (metanames: A.meta_name list) : property list =
    match metanames with
    | [] -> []
    | (h, _)::tail when h = hackname ->
        List.map (fun (_, s) -> string_to_property s) metanames
    | _ -> failwith "Incorrect metanames, hack failure"


let to_wrap (properties: property list) (wrap: 'a A.wrap) : 'a A.wrap =
    {wrap with A.inherited = properties_to_metanames properties;}


let to_constraints (properties: property list) : A.constraints =
    A.CstrSub(properties_to_metanames properties)


let of_wrap (wrap: 'a A.wrap) : property list =
    metanames_to_properties wrap.A.inherited


let of_constraints (constraints: A.constraints) : property list =
    match constraints with
    | A.CstrSub(l) -> metanames_to_properties l
    | _ -> []


let clear_wrap (wrap: 'a A.wrap) : 'a A.wrap = to_wrap [] wrap


let clear_all_hacks (ast: A.anything) : A.anything =
    let transform _ k wrap = k (clear_wrap wrap) in
    let donothing recursor k e = k e in
    let mcode mc = mc in
    let recursor =
        V.rebuilder
        mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode mcode mcode
        transform transform transform transform transform transform
        transform transform transform transform transform transform
        transform transform transform transform transform transform
        transform transform transform transform transform transform
        transform donothing
    in
    recursor.V.rebuilder_anything ast