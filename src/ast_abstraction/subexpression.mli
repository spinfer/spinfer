type nodes_type =
    | TopLevel of (Control_flow_c.node * Code.node_info) list list
    | SubExpr of (Ast_c.expression * Code.node_info) list

val transform_cluster : Code.forest -> nodes_type
val toplevel_of_forest : Code.forest -> nodes_type
val same_context : Ast_cocci.rule_elem * Ast_cocci.expression ->
    Ast_cocci.rule_elem * Ast_cocci.expression -> bool