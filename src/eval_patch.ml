module SPC = Semantic_patch_coccinelle
module SPM = Semantic_patch_mining


let copy_to_tmp (specfile: string) : string =
    let files_list = Specfile.read_spec specfile in
    let files_dir = (Debug.get_tmp_directory ()) ^ "/eval_patch/" in
    let src_dir = !Debug.original_pwd in

    let prg_name = Printf.sprintf "%s/normalize.py" Debug.static_dir in
    let file_json = Debug.get_tmp_directory () ^ "/norm_file.json" in
    let file_oc = open_out file_json in

    let files_str = List.map (fun file ->
        Printf.sprintf "\"%s\": null" (String.escaped file);
    ) (List.fold_left (fun l (f1, f2) -> f1::f2::l) [] files_list)
    in
    Printf.fprintf file_oc "{\n%s\n}" (String.concat ",\n" files_str);
    close_out file_oc;

    let files_cmd = Printf.sprintf "%s --copy-only %s %s %s"
        prg_name src_dir files_dir file_json
    in
    ignore (Sys.command files_cmd);
    files_dir


let () =
    Debug.patches_file := "/dev/stdout";
    Debug.create_tmp_directory ();
    Debug.set_coccinelle_profile ();

    let patch, specfile = match Sys.argv with
    | [|_; patch; specfile|] -> patch, specfile
    | _ ->
        Printf.eprintf "Usage: %s patch index\n" (Array.get Sys.argv 0);
        exit 1
    in

    let patch = Debug.make_absolute patch in
    let specfile = Debug.make_absolute specfile in
    let tmp_dir = copy_to_tmp specfile in
    Sys.chdir tmp_dir;


    let cocci_result = SPC.run_coccinelle patch specfile in
    let analyzed_result = SPC.analyze_cocci_result cocci_result specfile in

    let patch_object = SPM.{
        text = "";
        nb_rules = 0; (* FIXME: compute nb_rules *)
        cocci_result = cocci_result;
        analyzed = analyzed_result;
    }
    in
    SPM.write_semantic_patch_to_files patch_object
