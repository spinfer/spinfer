(* SPDX-License-Identifier: GPL-2.0-only
 * Copyright (C) 2016 Denis Merigoux
 * Copyright (C) 2016 Julia Lawall
 * Copyright (C) 2017 Van-Anh Nguyen
 * Copyright (C) 2017 Lucas Serrano
*)

let set_specfile file =
  Global.specfile := Debug.make_absolute file
let set_specfile_test file =
  Global.specfile_test := Debug.make_absolute file
let set_specfile_final_test file =
  Global.specfile_final_test := Debug.make_absolute file
let set_headerfiles file =
  Global.headerfiles := Debug.make_absolute file
let set_testheaderfiles file =
  Global.testheaderfiles := Debug.make_absolute file
let set_diff_graph_dir dir =
  Debug.diff_graphs_dir := Debug.make_absolute dir
let set_patches_file file =
  Debug.patches_file := Debug.make_absolute file
let set_debug_file file =
  Debug.debug_file := Debug.make_absolute file
let set_patch_debug_file file =
  Debug.patch_debug_file := Debug.make_absolute file
let set_unknown_bug_file file =
  Debug.unknown_bug_file := Debug.make_absolute file
let set_commit commit =
  Debug.commits := commit::!Debug.commits
let set_git_directory directory =
  Debug.git_directory := directory
let add_ignored_file file =
  Debug.ignored_files:= file::!Debug.ignored_files
let only_encode file =
  Global.only_encode := Debug.make_absolute file
let spatch_path path =
  Global.spatch_path := Debug.make_absolute path;
  Global.iso_file := "";
  Global.builtins_file := ""
let staged_input = ref false
let unstaged_input = ref false
let no_clean = ref false
let macro_file = ref ""

(** Main execution*)

let parse_cli_args () =
  (** Code block to retrieve and parse command-line arguments. *)
  let speclist = Arg.align [
      ("-f", Arg.String (set_specfile),
       "<file> Reads the input files on a file containing their names\
        , one pair of modified files per line separated by a space.");
      ("--specfile", Arg.String (set_specfile),
       "<filename>");
      ("-h", Arg.String (set_headerfiles),
       "<file> Reads the header files from a file containing their names\
        , one header file per line.");
      ("-t", Arg.String (set_specfile_test),
       "<file> Tests are on this file instead of input file.");
      ("-th", Arg.String (set_testheaderfiles),
       "<file> Reads the header files for tests from a file containing \
        their names, one header file per line.");
      ("--final-test", Arg.String (set_specfile_final_test),
       "<file> Final tests are on this file instead of input file.");
      ("-g", Arg.String (set_git_directory),
       "<directory> Sets the git repository directory from which the input is \
        read. Default is the current directory.");
      ("--git-dir", Arg.String (set_git_directory), "<directory> ");
      ("-c", Arg.String (set_commit),
       "<hash> Analyse the code modified by a  specific commit from the current \
        git directory. Repeat the option to analyse multiple commits.");
      ("--commit", Arg.String (set_commit),"<hash>");
      ("-s", Arg.Set staged_input,
       " Analyse the files staged in the current \
        git directory.");
      ("--git-staged-changes", Arg.Set staged_input," ");
      ("-u", Arg.Set unstaged_input,
       " Analyse the files modified (staged and \
        unstaged) in the current git directory.");
      ("-i", Arg.String (add_ignored_file),
       "<file> Ignores a specific file in the set provided by the git input \
        options. Path is specfied relative to the selected git respository \
        directory. Ignore multiple files by repeating the option.");
      ("--ignore-file", Arg.String (add_ignored_file), "<file> ");
      ("--git-all-changes", Arg.Set unstaged_input," ");
      ("-r", Arg.Clear Debug.abstract_all,
       " Always keeps concrete record fields and function names.");
      ("--custom-abstraction-rules", Arg.Clear Debug.abstract_all, " ");
      ("--graphs-dir", Arg.String(set_diff_graph_dir),
       "<directory> Enables the output of the diff graphs produced from the \
        pairs of modified files in the specified directory.");
      ("-o", Arg.String(set_patches_file),
       "<file> Enables the output of the semantic patch found in the specified \
        file.");
      ("--output-file", Arg.String(set_patches_file),
       " ");
      ("-d", Arg.String(set_debug_file),
        "<file> Enables the output of the semantic patch found in the specified \
         file (with the debugging information).");
      ("--debug-clusters", Arg.Set Clusterize.debug_clusters,
        " print cluster info on standard error, requires cocci clustering");
      ("--debug-file", Arg.String(set_debug_file),
        " ");
      ("--debug-patch-file", Arg.String(set_patch_debug_file),
        " ");
      ("--unknown-bugs", Arg.String(set_unknown_bug_file),
          "<file> Enables the output of the semantic patch found in the specified \
           file (with the unknown bug of coccinelle).");
      ("--macro-file", Arg.Set_string macro_file,
       " default macro definitions");
      ("-v", Arg.Set Debug.verbose, " Enables verbose mode.");
      ("--verbose", Arg.Set Debug.verbose, " ");
      ("--no-clean", Arg.Set no_clean,
       " Doesn't delete the temporary files after execution.");
      ("--show-patch-logs", Arg.Set Debug.show_logs,
       " Show coccinelle logs in case of patch failure.");
      ("--no-progress", Arg.Clear Global.progress,
       " Disable progress bar");
      ("--only-encode", Arg.String only_encode,
       "<filename> Stop after vector encoding and copy encoding to filename");
      ("-j", Arg.Set_int Global.jobs,
       "<jobs> Number of threads to use when running spatch");
      ("--use-cache", Arg.Set Global.use_cache,
       "Use normalization cache");
      ("--no-single-match", Arg.Set Global.no_single_match,
      " Remove patches matching single example" );
      ("--oracle", Arg.Set Debug.oracle,
      " Debug and test option: use cluster oracle");
      ("--feasible-metrics", Arg.Set Global.feasible_metrics,
      " Do not count new function and struct definitions in metrics");
      ("--spatch-path", Arg.String spatch_path,
       "<path> Use this spatch executable instead of the bundled one")
    ]
  in let usage_msg =
       "SmPL semantic patch inference over examples of modified C code. Type \
        man spinfer for more info.\n\
        Usage:
          spinfer -c <commit_hash> -o <output_file>\n\
        Options:"
  in Arg.parse speclist print_endline usage_msg;
  Debug.set_coccinelle_profile ()

let main_workflow () =
  (** Reads the specfile *)
  let speclist : Specfile.t =
    if (!Global.specfile <> "") then begin
      Debug.print "[INFO] Reading the specfile.";
      Specfile.read_spec !Global.specfile
    end
    else begin
      if (!Debug.git_directory <> "") then
        Sys.chdir !Debug.git_directory;
      if (!Debug.commits <> []) then begin
        Debug.print ("[INFO] Reading commit contents : "^
                     (List.fold_left
                        (fun accu commit -> accu^commit^" ") ""
                        !Debug.commits));
        Git_interface.get_specfile_from_commits !Debug.commits
      end
      else begin if (!staged_input) then begin
          Debug.print "[INFO] Retrieving changes from staged files.";
          Git_interface.get_files_from_staged_changes ()
        end else begin if (!unstaged_input) then begin
          Debug.print "[INFO] Retrieving mchanges from staged and \
                       unstaged files.";
          Git_interface.get_files_from_unstaged_changes ()
        end else
            failwith "[ERROR] No input specified for the modified files."
        end
      end
    end
  in
  if (!Debug.git_directory <> "") then
    Sys.chdir (!Debug.original_pwd);
  Debug.data := {!Debug.data with Debug.pairs_of_files = speclist};
  (** Build the CFGs *)
  Debug.print "[INFO] Building the CFGs.";

  (if (!macro_file <> "")
    then Parse_c.init_defs_builtins !macro_file
    else begin
      let file = Debug.static_dir ^ "standard.h" in
      Parse_c.init_defs_builtins file
    end
  );


  let speclist_to_files_w_diff (spec: Specfile.t) : (string * (int list)) list =
    let position_to_int = function
    | Diff.Line(i) -> i
    | Diff.Range(i, _) -> i
    in
    List.fold_left (fun accu (before, after) ->
      let diff_list = Diff.exec_diff (before, after) in
      let positions_before = List.map (fun diff ->
        position_to_int diff.Diff.before
      ) diff_list
      in
      let positions_after = List.map (fun diff ->
        position_to_int diff.Diff.after
      ) diff_list
      in
      (before, positions_before)::(after, positions_after)::accu
    ) [] spec
  in

  let all_files_infer = speclist_to_files_w_diff speclist in
  let headers =
    if !Global.headerfiles = ""
    then []
    else
      List.map String.trim
	(Common.cmd_to_list ("cat "^(!Global.headerfiles))) in
  let infer_dir =
    Normalize.normalize_files all_files_infer headers "infer_files" in
  let test_dir =
    if (!Global.specfile_test = "")
    then begin
      Global.specfile_test := !Global.specfile;
      infer_dir
    end
    else
      let all_files_test =
        speclist_to_files_w_diff (Specfile.read_spec !Global.specfile_test)
      in
      let headers =
        if !Global.testheaderfiles = ""
        then []
        else
          List.map String.trim
            (Common.cmd_to_list ("cat "^(!Global.testheaderfiles)))
      in
      Normalize.normalize_files all_files_test headers "test_files"
  in
  let final_test_dir =
    if (!Global.specfile_final_test = "")
    then begin
      Global.specfile_final_test := !Global.specfile_test;
      test_dir
    end
    else
      let all_files_final_test =
        speclist_to_files_w_diff (Specfile.read_spec !Global.specfile_final_test)
      in
      let headers =
        if !Global.headerfiles = ""
        then []
        else
          List.map String.trim
      (Common.cmd_to_list ("cat "^(!Global.headerfiles)))
      in
      Normalize.normalize_files all_files_final_test headers "final_test_files"
  in

  Global.infer_dir := infer_dir;
  Global.test_dir := test_dir;
  Global.final_test_dir := final_test_dir;
  Sys.chdir infer_dir;

  let program_flow_graphs : (CFG.program * CFG.program) list =
    CFG.speclist_to_CFGslist speclist in

  (** Execute diff and read the output *)
  Debug.print "[INFO] Diffing the pairs of files.";
  let diffs_list : (Diff.t list) list =
    List.map Diff.memoized_exec_diff speclist in

  (** Build the diff CFG *)
  Debug.print "[INFO] Analysing the differences.";
  let pairs_of_diff_programs
    : (CFGDiff.diff_program * CFGDiff.diff_program) list =
    List.map2 CFGDiff.mark_program_nodes program_flow_graphs diffs_list in
  let (diff_programs, _) : ((CFGDiff.numbered_diff_program option) list * int) =
    List.fold_left2 (fun (accu, i) (x, y) diffs ->
        let programs, index = CFGDiff.merge_diff x y i diffs in
        (programs::accu, index)
    ) ([], 0) pairs_of_diff_programs diffs_list
  in
  let code : Code.t = Code.programs_to_code diff_programs in
  (** Finding common changes patterns *)
  if code <> []
  then
    Semantic_patch_mining.mine_sequences code
  else
    Printf.printf (
      "Found no code to learn from: either examples contain no changes, "
      ^^ "or contain only functions that have been completely added or removed, "
      ^^ "or all changes are outside functions.\n%!"
    )

let () =
  (**Printf.printf "%s \n" Sys.executable_name;*)
  parse_cli_args ();
  Debug.create_tmp_directory ();
  main_workflow ();
  if (not !no_clean) then
    Debug.delete_tmp_directory ()
