let print_ast_debug (pattern: Abstract_ast.pattern) : string =
    let tmp_hbtl = Hashtbl.create 10 in
    Craft_metavar.print_pattern_ast tmp_hbtl None pattern
