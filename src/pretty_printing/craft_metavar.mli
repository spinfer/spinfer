module Ast = Ast_cocci
module V = Visitor_ast
module AA = Abstract_ast

val decl_metavar : Ast.anything list -> string
val shortname_metavar : Ast.anything list -> string
val shrink_metavar : AA.Metavariable.t -> (int * int) list -> AA.Metavariable.t
val rewrite_metaname : Ast.rule_elem -> string -> string -> Ast.rule_elem

val print_pattern_ast : (Ast.anything list, string * string) Hashtbl.t
  -> (int * int) list option ->  AA.pattern -> string
