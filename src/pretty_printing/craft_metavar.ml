module Ast = Ast_cocci
module V = Visitor_ast
module AA = Abstract_ast

let extract_iter_name (elem: Ast.rule_elem) : string option =
    match elem.Ast.node with
    | Ast.IteratorHeader(id, _, _, _) ->
        begin match id.Ast.node with
        | Ast.Id(name, _, _, _) -> Some(name)
        | _ -> None
        end
    | _ -> None


let decl_metavar matched_items : string =
    match List.hd matched_items with
    | Ast.ExpressionTag(_) -> "expression"
    | Ast.ConstantTag(_) -> "constant"
    | Ast.ExprDotsTag(_) -> "expression list"
    | Ast.IdentTag(_) -> "identifier"
    | Ast.TypeCTag(_)
    | Ast.FullTypeTag(_) -> "type"
    | Ast.DeclarationTag(_) -> "declaration"
    | Ast.BinaryOpTag(_) -> "binary operator"
    | Ast.AssignOpTag(_) -> "assignment operator"
    | Ast.InitTag(_) -> "initializer"
    | Ast.ParamTag(_) -> "parameter"
    | Ast.ParamDotsTag(_)  -> "parameter list"
    | Ast.Rule_elemTag(elem) when extract_iter_name elem <> None ->
        "iterator name"
    | Ast.Rule_elemTag(_)
    | Ast.SgrepStartTag(_) (*FIXME: Need to review these cases**)
    | Ast.StringFragmentTag(_) -> "expression"
    | _ -> assert false


let shortname_metavar matched_items : string =
  match List.hd matched_items with
  | Ast.ConstantTag(_) -> "C"
  | Ast.ExpressionTag(_)
  | Ast.ExprDotsTag(_) -> "E"
  | Ast.InitTag(_)
  | Ast.IdentTag(_) -> "I"
  | Ast.TypeCTag(_)
  | Ast.FullTypeTag(_) -> "T"
  | Ast.BinaryOpTag(_) -> "B"
  | Ast.Rule_elemTag(_) -> "R"
  | Ast.DeclarationTag(_) -> "D"
  | Ast.AssignOpTag(_) -> "A"
  | Ast.ParamDotsTag(_)
  | Ast.ParamTag(_) -> "P"
  | Ast.SgrepStartTag(_)
  | Ast.StringFragmentTag(_) -> "S"
  | _ -> assert false


let rewrite_metaname (ast: Ast.rule_elem) (on:string) (nn:string) :
  Ast.rule_elem =
  let metaname m = match Ast.unwrap_mcode m with
    | (a,b) when b = on -> Ast.make_mcode("",nn)
    | _ -> m
  in
  let mcode mc = mc in
  let donothing r k e = k e in
  let recursor =
    V.rebuilder
      metaname mcode mcode mcode mcode mcode mcode mcode
      mcode mcode mcode mcode mcode mcode
      donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing
      donothing donothing
  in
  recursor.V.rebuilder_rule_elem ast


let check_expr e1 e2 =
  match (Ast.unwrap e1, Ast.unwrap e2) with
  | Ast.Constant(_), Ast.Constant(_)
  | Ast.Ident(_), Ast.Ident(_)
  | Ast.TypeExp(_), Ast.TypeExp(_)
  | Ast.EComma(_),Ast.EComma(_) -> true
  | _, _ -> false

let check_expr_dots d1 d2 =
  if List.length d1 = List.length d2
  then
    List.for_all2(fun e1 e2 -> check_expr e1 e2) d1 d2
  else false



(**
   The order in examples is important
*)
let shrink_metavar (metavar) (examples: (int * int) list)
        : AA.Metavariable.t =
    AA.Metavariable.filter (fun (id, _) ->
        List.mem id examples
    ) metavar


let collect_func_ident ast : 'a list =
  let bind x y = x @ y in
  let option_default = [] in
  let mcode r m = option_default in
  let donothing recursor k e = k e in (* just combine in the normasl way *)
  let mc ((_,x),_,_,_) = x in

  let astfvexpr recursor k e =
      bind (k e)
        (match Ast.unwrap e with
         | Ast.FunCall(f,_,_,_) ->
           begin
             match Ast.unwrap f with
             | Ast.Ident(i) ->
               begin
                 match Ast.unwrap i with
                 | Ast.MetaId(metaname,_,_,_) -> [mc metaname]
                 | _ -> option_default
               end
             | _ -> option_default
           end
         | _ -> option_default
        )
  in
  let recursor =
    V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode mcode mcode
    donothing donothing donothing donothing donothing donothing
    donothing astfvexpr donothing donothing donothing donothing
    donothing donothing donothing donothing donothing
    donothing donothing donothing donothing
    donothing donothing donothing donothing donothing
  in
  recursor.V.combiner_rule_elem ast



let concretise_metavar matched_items : string =
    let old_f = Format.get_formatter_out_functions () in
    let print_single_space n =
        let current_out_functions = Format.get_formatter_out_functions () in
        let out_string = current_out_functions.Format.out_string in
        out_string " " 0 1
    in
    Format.set_formatter_out_functions {old_f with
        Format.out_newline=print_single_space;
        Format.out_spaces=print_single_space
    };
    let strs = List.map (fun a ->
        Common.format_to_string
            (function _ -> Pretty_print_cocci.pp_print_anything a)
    ) matched_items
    in
    Format.set_formatter_out_functions old_f;
    String.concat "," strs

let concretise_const matched_items : bool =
  List.for_all (fun a ->
      match a with
      | Ast.ConstantTag(c) ->
        begin match c with
          | Ast.Int(str) ->
            begin try
              ignore (int_of_string str);
              true
            with Failure _ -> false
            end
          | _ -> false
        end
      | _ -> false
    ) matched_items

let register_metavar
        (metavars_tbl:(Ast.anything list,string * string) Hashtbl.t)
        (ids: (int * int) list option)
        (metavariable: AA.Metavariable.t)
        (on: string)
        (ast: Ast.rule_elem)
    : string * string =

    let metavar = match ids with
    | None -> metavariable
    | Some(examples) -> shrink_metavar metavariable examples
    in

    let matched_items = Utils.assoc_right (AA.Metavariable.elements metavar) in
    let matched_items = List.sort_uniq compare matched_items in
    try
        let metaname, content = Hashtbl.find metavars_tbl matched_items in
        (metaname, content)
    with Not_found ->
        let count = Hashtbl.length metavars_tbl in
        let metaname = shortname_metavar matched_items ^ string_of_int count in
        (*FIXME: This is a "cool" hack, so it will crash sooner or later *)
        let metaname = match List.hd matched_items with
        | Ast.Rule_elemTag(elem) when extract_iter_name elem <> None ->
            Utils.option_get (extract_iter_name elem)
        | _ -> metaname
        in
        let str, inline = match List.hd matched_items with
        | Ast.ConstantTag(_) ->
            if (List.length matched_items) = 1
            then
                (concretise_metavar matched_items, true)
            else begin
                if concretise_const matched_items
                then (concretise_metavar matched_items, false)
                else ("", false)
            end
        | Ast.IdentTag(_) ->
            if List.mem on (collect_func_ident ast)
            then begin
                let concr = concretise_metavar matched_items in
                if (List.length matched_items) = 1
                then (concr,true)
                else (concr,false)
            end
            else
                ("", false)
        | Ast.BinaryOpTag(_) ->
            let concr = concretise_metavar matched_items in
            if (List.length matched_items) = 1
            then (concr, true)
            else (concr, false)
        | _ -> ("", false)
        in
        if inline
        then
            (str, "")
        else begin
            let value = (metaname, str) in
            Hashtbl.add metavars_tbl matched_items value;
            value
        end


let print_pattern_ast
        (metavars_tbl:(Ast.anything list,string * string) Hashtbl.t)
        (ids: (int * int) list option)
        (pattern: AA.pattern)
        : string =
    let rec adjust_ast l =
        match l with
        | [] -> pattern.AA.ast
        | (on, metavar)::rest ->
              let nn, _ =
                register_metavar metavars_tbl ids metavar on pattern.AA.ast
              in
              rewrite_metaname (adjust_ast rest) on nn
    in
    let new_ast = adjust_ast pattern.AA.metavars in

    (* Remove newlines
      TODO: patch coccinelle instead
     *)
    let old_f = Format.get_formatter_out_functions () in
    let print_single_space n =
        let current_out_functions = Format.get_formatter_out_functions () in
        let out_string = current_out_functions.Format.out_string in
        out_string " " 0 1
    in
    Format.set_formatter_out_functions {old_f with
        Format.out_newline=print_single_space;
        Format.out_spaces=print_single_space
    };
    let res = Pretty_print_cocci.rule_elem_to_string new_ast in
    Format.set_formatter_out_functions old_f;
    res
