(**
   Provides a git interface.
   @author Denis Merigoux
*)

let get_command_output (command : string) : string list =
  let process_output_to_list2 (command : string) =
    let chan = Unix.open_process_in command in
    let res = ref ([] : string list) in
    let rec process_otl_aux () =
      let e = input_line chan in
      res := e::!res;
      process_otl_aux() in
    try process_otl_aux ()
    with End_of_file ->
      let stat = Unix.close_process_in chan in (List.rev !res,stat)
  in
  let (l,_) = process_output_to_list2 command in l

let create_files_blob_from_list
    (commit : string)
    (files : string list)
  : Specfile.t =
  let files =
    List.filter (fun file -> not (List.mem file !Debug.ignored_files)) files
  in
  let temp_dir = Debug.get_tmp_directory () in
  let files_dir = Printf.sprintf "%s/spinfer_source_files" temp_dir in
  ignore (Sys.command ("mkdir -p "^files_dir));
  List.fold_left (fun accu file ->
      let old_file =
        files_dir^"/"^(Str.global_replace (Str.regexp "/") "_" file)^"."^commit^".before"
      in
      let new_file =
        files_dir^"/"^(Str.global_replace (Str.regexp "/") "_" file)^"."^commit^".after"
      in
      if (commit <> "HEAD") then begin
        ignore
          (Sys.command ("git cat-file blob \
                        "^commit^"^:"^file^" 1> "^old_file^" 2> /dev/null"));
        ignore
          (Sys.command ("git cat-file blob \
                        "^commit^":"^file^" 1> "^new_file^" 2> /dev/null"));
      end else begin
        ignore
          (Sys.command ("git cat-file blob \
                         HEAD:"^file^" 1> "^old_file^" 2> /dev/null"));
        ignore
          (Sys.command ("cat "^file^"1> "^new_file^" 2> /dev/null"));
      end;
      (old_file,new_file)::accu
    ) [] files


let get_specfile_from_commits
    (commits : string list) : Specfile.t =
  List.fold_left (fun accu commit ->
  let modified_files =
    get_command_output ("git diff-tree -w --no-commit-id --name-only -r "^commit)
  in
  let new_speclist = create_files_blob_from_list commit modified_files in
  accu@new_speclist) [] commits


let get_files_from_staged_changes () : Specfile.t =
  let modified_files =
    get_command_output ("git ls-files --modified --deleted")
  in
  create_files_blob_from_list "HEAD" modified_files

let get_files_from_unstaged_changes () : Specfile.t =
  let modified_files =
    get_command_output ("git ls-files --other --modified --deleted \
                         --exclude-standard")
  in
  create_files_blob_from_list "HEAD" modified_files
