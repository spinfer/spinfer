(**
   Provides an interface to read input files from git.
   @author Denis Merigoux
*)

(** Gives as input the files modified by a specific commit. *)
val get_specfile_from_commits : string list -> Specfile.t

(** Gives as input the files staged as modified.*)
val get_files_from_staged_changes : unit -> Specfile.t

(** Gives as input all the modified files in the working tree. *)
val get_files_from_unstaged_changes : unit -> Specfile.t
