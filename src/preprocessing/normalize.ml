let normalize_files (old_files: (string * (int list)) list)
                    (headers: string list) (name: string) : string =
    let files_dir = (Debug.get_tmp_directory ()) ^ "/" ^ name ^ "/" in
    let src_dir = !Debug.original_pwd in

    let prg_name = Printf.sprintf "%s/normalize.py" Debug.static_dir in
    let header_json = Debug.get_tmp_directory () ^ "/norm_header.json" in
    let file_json = Debug.get_tmp_directory () ^ "/norm_file.json" in

    let header_oc = open_out header_json in
    let headers_str = List.map (fun header ->
        Printf.sprintf "\"%s\": null" (String.escaped header);
    ) headers
    in
    Printf.fprintf header_oc "{\n%s\n}" (String.concat ",\n" headers_str);
    close_out header_oc;
    let header_cmd = Printf.sprintf "%s --copy-only %s %s %s"
        prg_name src_dir files_dir
        header_json
    in

    let file_oc = open_out file_json in
    let files_str = List.map (fun (file, positions) ->
        let pos_str =
            if positions = []
            then "null"
            else "\"" ^ String.concat "," (List.map string_of_int positions) ^ "\""
        in
        Printf.sprintf "\"%s\": %s" (String.escaped file) pos_str;
    ) old_files
    in
    Printf.fprintf file_oc "{\n%s\n}" (String.concat ",\n" files_str);
    close_out file_oc;
    let files_cmd = Printf.sprintf "%s --jobs %d %s %s %s %s"
        prg_name
        !Global.jobs
        (if !Global.use_cache then "--use-cache" else "")
        src_dir files_dir
        file_json
    in

    if headers <> [] then ignore (Sys.command header_cmd);
    ignore (Sys.command files_cmd);
    files_dir
