module CFC = Control_flow_c
module G = CFC.G
module S = Utils.IntSet
module SPC = Semantic_patch_coccinelle
module M = Metrics

let all_pairs (l: 'a list) : ('a * 'a) list =
    let rec unfold accu = function
    | [] -> accu
    | head::[] -> accu
    | head::tail ->
        let new_elts = List.rev_map (fun elt -> head, elt) tail in
        unfold (new_elts @ accu) tail
    in
    unfold [] l


let topo_sort (f: 'a -> 'a -> int) (l: 'a list) : 'a list =
    let graph = new G.ograph_mutable in
    let keys = List.map (fun elt -> graph#add_node elt) l in
    let pairs = all_pairs keys in

    let no_pred_nodes () = CFC.KeyMap.filter (fun key _ ->
        CFC.KeyEdgeSet.is_empty (graph#predecessors key)
    ) graph#nodes
    in

    let add_edge (i1: int) (i2: int) : unit =
        let n1 = CFC.KeyMap.find i1 graph#nodes in
        let n2 = CFC.KeyMap.find i2 graph#nodes in
        let res = f n1 n2 in
        if res > 0
        then graph#add_arc ((i1, i2), CFC.Direct)
        else if res < 0
        then graph#add_arc ((i2, i1), CFC.Direct)
    in
    List.iter (fun (i1, i2) -> add_edge i1 i2) pairs;
    (* At this step dependencies are built, time to construct a list using
     * Kahn's algorithm
     *)

    let rec kahn sorted map : 'a list =
        if CFC.KeyMap.is_empty map
        then
            if CFC.KeyMap.is_empty graph#nodes
            then List.rev sorted
            else
            let to_remove, _ = CFC.KeyMap.min_binding graph#nodes in
            CFC.KeyEdgeSet.iter (fun (k, e) ->
                graph#del_arc ((k, to_remove), e)
            ) (graph#predecessors to_remove);
            Printf.eprintf "Warning: Topo sort cyclic dependencies\n%!";
            kahn sorted (no_pred_nodes ())
        else
            let to_remove, node = CFC.KeyMap.min_binding map in
            CFC.KeyEdgeSet.iter (fun (k, e) ->
                graph#del_arc ((to_remove, k), e)
            ) (graph#successors to_remove);
            graph#del_node to_remove;
            kahn (node::sorted) (no_pred_nodes ())
    in
    kahn [] (no_pred_nodes ())


let partition_patches (patches: SPC.analyzed_set list)
        : (SPC.NodeSet.t * SPC.analyzed_set) list list =
    let extract_support (set: SPC.analyzed_set) : SPC.NodeSet.t =
        SPC.NodeSet.filter (fun (node, _, _, _) ->
            match node with
            | CFGDiff.Before, _ | CFGDiff.Both, _ -> true
            | _ -> false
        ) (SPC.NodeSet.union set.M.true_positives set.M.false_positives)
    in
    let partition support patch : (SPC.NodeSet.t * SPC.analyzed_set) list =
        let htbl = Hashtbl.create 50 in
        List.iter (fun ((_, uid, _, func) as elt) ->
            let key = uid, func in
            if Hashtbl.mem htbl key
            then
                let existing = Hashtbl.find htbl key in
                Hashtbl.replace htbl key (SPC.NodeSet.add elt existing)
            else
                Hashtbl.add htbl key (SPC.NodeSet.singleton elt)
        ) (SPC.NodeSet.elements support);
        Hashtbl.fold (fun (uid, func) support accu ->
            let filter =
                SPC.NodeSet.filter (fun (_, u, _, f) -> u = uid && f = func)
            in
            let set = {patch with
                M.true_positives = filter patch.M.true_positives;
                M.false_positives = filter patch.M.false_positives;
                M.false_negatives = filter patch.M.false_negatives;
            }
            in
            (support, set)::accu
        ) htbl []
    in
    List.map (fun p -> partition (extract_support p) p) patches


let compute_score (patches: SPC.analyzed_set list) : float =
    let partitioned = partition_patches patches in
    let _, analyzed = List.fold_left (fun (old_support, analyzed) part_patch ->
        let union = SPC.NodeSet.union in
        let inter = SPC.NodeSet.inter in
        let applied, not_applied = List.partition (fun (support, set) ->
            SPC.NodeSet.is_empty (inter support old_support)
        ) part_patch
        in
        let applied_sup, applied_set = List.split applied in
        let new_support = List.fold_left union old_support applied_sup in
        let analyzed = List.fold_left (fun accu set ->
            {accu with
                M.true_positives = union accu.M.true_positives set.M.true_positives;
                M.false_positives = union accu.M.false_positives set.M.false_positives;
                M.false_negatives = union accu.M.false_negatives set.M.false_negatives;
            }
        ) analyzed applied_set
        in
        (* Unapplied true positives are now false negatives *)
        let new_fn = List.fold_left (fun accu set ->
               union (union accu set.M.true_positives) set.M.false_negatives
        ) SPC.NodeSet.empty (Utils.assoc_right not_applied)
        in
        let analyzed = {analyzed with
            M.false_negatives = union analyzed.M.false_negatives new_fn
        }
        in
        new_support, analyzed
    ) (SPC.NodeSet.empty, SPC.empty_analyzed_set) partitioned
    in

    (* True positives from some patch remove false negatives from others *)
    let analyzed = {analyzed with
        M.false_negatives = SPC.NodeSet.diff
            analyzed.M.false_negatives analyzed.M.true_positives;
    }
    in
    M.fscore 2. (SPC.set_to_int_result SPC.NodeSet.cardinal analyzed)


let index_of_unapplied (patches: SPC.analyzed_set list) : int list =
    let partitioned = partition_patches patches in
    let _, indices, _ = List.fold_left (fun (old_support, indices, i) part_patch ->
        let union = SPC.NodeSet.union in
        let inter = SPC.NodeSet.inter in
        let applied = List.filter (fun (support, set) ->
            SPC.NodeSet.is_empty (inter support old_support)
        ) part_patch
        in
        if applied <> []
        then
            let applied_sup = Utils.assoc_left applied in
            let new_support = List.fold_left union old_support applied_sup in
            (new_support, indices, i+1)
        else begin
            (old_support, i::indices, i+1)
        end
    ) (SPC.NodeSet.empty, [], 0) partitioned
    in
    indices
