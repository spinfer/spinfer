module SPC = Semantic_patch_coccinelle
module S = SPC.NodeSet
module M = Metrics

type comp =
    | Subset
    | Superset
    | Disjoint

let compare_ids_set (result1: S.t M.analyzed_result)
                    (result2: S.t M.analyzed_result) : comp =
    let tp1 = result1.M.true_positives in
    let tp2 = result2.M.true_positives in
    let fp1 = result1.M.false_positives in
    let fp2 = result2.M.false_positives in
    if S.subset tp2 tp1 && S.subset fp1 fp2
    then Superset
    else if S.subset tp1 tp2 && S.subset fp2 fp1
    then Subset
    else Disjoint

let merge_full_patch (patches: ('a * string * S.t M.analyzed_result option) list)
                     (merge_extra: 'a -> 'a -> 'a)
                     (specfile: string)
                     : ('a * string * S.t M.analyzed_result) list =
    let all_sets = List.map (fun (extra, patch, analysis) ->
        let result = match analysis with
        | Some(result) -> result
        | None -> SPC.analyze_patch patch specfile
        in
        (extra, patch, result)
    ) patches
    in
    let all_sets = List.filter (fun (_, _, result) ->
        match SPC.check_safety (SPC.set_to_int_result SPC.NodeSet.cardinal result) with
        | SPC.Unknown -> false
        | _ -> true
    ) all_sets
    in
    let rec iter to_return accu remaining =
    match accu, remaining with
    | [], [] -> List.rev to_return
    | _, [] ->
        let remaining = List.rev accu in
        iter (List.hd remaining::to_return) [] (List.tl remaining)
    | _, head::tail ->
        let current = List.hd to_return in
        let cur_extra, cur_patch, cur_set = current in
        let test_extra, test_patch, test_set = head in
        let to_return = List.tl to_return in
        match compare_ids_set cur_set test_set with
        | Disjoint -> iter (current::to_return) (head::accu) tail
        | Superset ->
            let new_current =
                ((merge_extra cur_extra test_extra), cur_patch, cur_set)
            in
            iter (new_current::to_return) accu tail
        | Subset ->
            let new_current =
                ((merge_extra cur_extra test_extra), test_patch, test_set)
            in
            (* If subset we need to retest on all previous elements *)
            iter (new_current::to_return) [] ((List.rev accu)@tail)
    in
    iter [] (List.rev all_sets) []


let is_garbage (patch: 'a * string * S.t M.analyzed_result) : bool =
    let _, _, result = patch in
    let contains_added = S.exists (fun ((prefix, _), _, _, _) ->
        match prefix with
        | CFGDiff.After -> true
        | _ -> false
    ) result.M.true_positives
    in
    let multiple_matching = S.exists (fun (_, uid1, _, fun_name1) ->
        S.exists (fun (_, uid2, _, fun_name2) ->
            uid1 <> uid2 || fun_name1 <> fun_name2
        ) result.M.true_positives
    ) result.M.true_positives
    in
    let precision = M.precision (SPC.set_to_int_result SPC.NodeSet.cardinal result) in
    (* Reject if this doesn't add any added node and is not safe *)
    not contains_added && not (S.is_empty result.M.false_positives)
    ||
    precision < 0.25
    ||
    (!Global.no_single_match && not (multiple_matching))