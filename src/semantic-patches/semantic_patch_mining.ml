(**
   Extract sequences of patterns from a codebase.
   @author Denis Merigoux
*)

module SPC = Semantic_patch_coccinelle
module M = Metrics

type patch_object = {
    text: string;
    nb_rules: int;
    cocci_result: SPC.cocci_result;
    analyzed: SPC.analyzed_set
}

let write_semantic_patch_to_files (patch: patch_object) : unit =

    (* Groups function by transformation success *)
    let partial_transform, missing_transform, incorrect_transform =
        SPC.get_transform_groups patch.analyzed
    in
    let str_of_group header group =
        let str_of_elt (file, func) = file ^ ": " ^ func in
        if group = []
        then ""
        else
            let elts_strs = List.map str_of_elt group in
            Printf.sprintf "/*\n%s:\n - %s\n*/\n"
                header
                (String.concat "\n - " elts_strs)
    in
    let int_result_node =
        SPC.set_to_int_result SPC.NodeSet.cardinal patch.analyzed
    in
    let int_result_fun =
        SPC.set_to_int_result
            SPC.LocSet.cardinal
            (SPC.transform_node_set_to_location patch.analyzed)
    in
    let nb_cover, nb_funs = SPC.get_fully_patched_score patch.analyzed in
    let cover_percent = (nb_cover * 100) / nb_funs in
    let file_names = String.concat "\n" patch.cocci_result.SPC.modified_files in

    if (!Debug.patches_file <> "") then begin
    let oc = open_out_gen
        [Open_wronly; Open_creat; Open_text; Open_append]
        0o640 !Debug.patches_file
    in
    let recall_node, precision_node = match SPC.check_safety int_result_node with
    | SPC.Unsafe _ | SPC.Safe ->
        (Metrics.recall int_result_node), (Metrics.precision int_result_node)
    | _ -> 0., 1.
    in
    let recall_fun, precision_fun = match SPC.check_safety int_result_fun with
    | SPC.Unsafe _ | SPC.Safe ->
        (Metrics.recall int_result_fun), (Metrics.precision int_result_fun)
    | _ -> 0., 1.
    in
    let str_after_patch =
        (Printf.sprintf
            ("// Final metrics (for the combined %d rules):\n" ^^
            "// -- Edit Location --\n" ^^
            "// Recall: %.2f, Precision: %.2f\n" ^^
            "// -- Node Change --\n" ^^
            "// Recall: %.2f, Precision: %.2f\n" ^^
            "// -- General --\n" ^^
            "// Functions fully changed: %d/%d(%d%%)\n\n")
            patch.nb_rules
            recall_fun precision_fun
            recall_node precision_node
            nb_cover nb_funs cover_percent
        )
        ^
        str_of_group "Functions where the patch applied partially" partial_transform
        ^
        str_of_group "Functions where the patch did not apply" missing_transform
        ^
        str_of_group "Functions where the patch produced incorrect changes" incorrect_transform
    in
    Printf.fprintf oc "%s%s\n// ---------------------------------------------\n"
        patch.text str_after_patch;
    close_out oc;
    end;

  if (!Debug.debug_file <> "") then begin
  let oc_cocci_debug = open_out_gen
  [Open_wronly; Open_creat; Open_text; Open_append]
  0o640
  !Debug.debug_file
  in
  let lines_to_print_debug = Printf.sprintf
    ("\n%s\n" ^^
    "\n *** Names of the files that contributed \
           to this semantic patch:  *** \n%s" ^^
    "\nLogs:\n%s\n***\n")
    patch.text
    file_names
    (String.concat "\n" patch.cocci_result.SPC.logs)
  in
  Printf.fprintf oc_cocci_debug "%s" lines_to_print_debug;
  close_out oc_cocci_debug;
  end;
  ()

let mine_sequences (code : Code.t) : unit  =
    (if (!Debug.patches_file <> "") && (Sys.file_exists !Debug.patches_file)
    then Sys.remove !Debug.patches_file);
    (if (!Debug.debug_file <> "") && (Sys.file_exists !Debug.debug_file)
    then Sys.remove !Debug.debug_file);
    (if (!Debug.unknown_bug_file <> "") && (Sys.file_exists !Debug.unknown_bug_file)
    then Sys.remove !Debug.unknown_bug_file);


    Sys.chdir !Global.test_dir;
    Printf.eprintf "Begin code clone clustering... %!";
    let deletion_list, context_list, addition_list =
      Fragment.build_A_list code
    in
    Printf.eprintf "Done.\n%!";
    let before_graphs, after_graphs =
        List.split (
            List.map (fun (graph, _, i) ->
                let before, after = Code.dissociate_merged_CFG graph in
                ((before, i), (after, i))
            ) code
        )
    in
    let before_dom, before_post =
      Graph.get_dominators before_graphs CFGDiff.first_key CFGDiff.last_key
    in

    let after_dom, after_post =
      Graph.get_dominators after_graphs CFGDiff.first_key CFGDiff.last_key
    in

    let graphs = List.map (fun (graph, _, id) -> (graph, id)) code in
    let pre_csets = Graph.compute_context_set graphs false in
    let post_csets = Graph.compute_context_set graphs true in

    let before_doms = before_dom, before_post in
    let after_doms = after_dom, after_post in
    let csets = (pre_csets, post_csets) in

    let print_fragments alist : unit = List.iter (fun fragment ->
        Debug.print_patch_debug (
            "\t" ^
            (Fragment.stringify_fragment fragment)
        )
      ) (Rules_assembling.sort_alist_by_matched_nodes alist)
    in
    Debug.print_patch_debug "Deletion list:";
    print_fragments deletion_list;
    Debug.print_patch_debug "Context list:";
    print_fragments context_list;
    Debug.print_patch_debug "Addition list:";
    print_fragments addition_list;

    let init_del_len  = List.length deletion_list in
    let rec build_results (del, con, add) accu = match del with
    | [] -> accu
    | _ ->
        let fragments, results =
              Rules_assembling.construct_graphs (del, con, add)
                    (before_doms, after_doms) csets code
        in
        if !Global.progress
        then
            Utils.print_progress init_del_len (init_del_len - (List.length del));
        build_results fragments (results::accu)
    in
    Printf.eprintf "Assembling fragments. This might take a while.\n%!";
    let results_list =
        build_results (deletion_list, context_list, addition_list) []
    in
    Printf.eprintf "\n%!";
    let format_sources sources : string =
        let sources_str_list = List.map (fun ((before, after), func) ->
            let prefix = Utils.common_prefix before after in
            let pre_len = String.length prefix in
            let before_suffix =
                String.sub before pre_len (String.length before - pre_len)
            in
            let after_suffix =
                String.sub after pre_len (String.length after - pre_len)
            in
            Printf.sprintf "(%s{%s,%s}: %s)" prefix before_suffix after_suffix func
        ) sources
        in
        let sources_str = String.concat ", " sources_str_list in
        Printf.sprintf "// Infered from: %s\n" sources_str
    in
    let get_sources gids = List.map (fun gid ->
        let (graph, _, _) =
            List.find (fun (_, _, id) -> id = gid) code
        in
        let stringify_func =
        match CFGDiff.fun_header_of_graph graph with
        | Some(def) -> Cocci_addon.stringify_name (fst def).Ast_c.f_name
        | None -> assert false
        in
        (Global.id_to_filenames gid, stringify_func)
    ) gids
    in
    let format_fp_sources analyzed =
        let fp_sources = List.map (fun (_, _, file_name, fun_name) ->
            (file_name, fun_name)
        ) (SPC.NodeSet.elements analyzed.SPC.M.false_positives)
        in
        let uniq_sources = List.sort_uniq compare fp_sources in
        let sources_str_list = List.map (fun (file_name, fun_name) ->
            Printf.sprintf "(%s: %s)" file_name fun_name
        ) uniq_sources
        in
        if sources_str_list <> []
        then
            let sources_str = String.concat ", " sources_str_list in
            Printf.sprintf "// False positives: %s\n" sources_str
        else ""
    in
    let raw_results, merged_result =
        let flattened = List.flatten results_list in
        let raw_results = List.mapi (fun i (result, analysis) ->
            let result = Rules_assembling.add_dots result code in
            let gids = Array.to_list result.Rule_graph.graph_ids in
            let name = Printf.sprintf "raw_patch_%d.gv" i in
            Debug.exec_if_graphs_dir
              (Rule_graph.print_result_graph result) name;
            let patch = Rule_graph.get_semantic_patch result code in
            (gids, patch, analysis)
        ) flattened
        in
        let merge_fst gid1 gid2 =
                List.sort_uniq compare (gid1@gid2)
        in
        let merged_results =
            Patch_refining.merge_full_patch raw_results merge_fst
                (!Global.specfile_test)
        in
        let filtered_merged_results =
            List.filter (fun a -> not (Patch_refining.is_garbage a)) merged_results
        in
        let get_patch_str (gids, patch, analyzed) =
            let fp_str = match analyzed with
            | Some(analyzed) -> format_fp_sources analyzed
            | None -> ""
            in
            patch ^ (format_sources (get_sources gids)) ^ fp_str
        in

        let results_with_score = List.map (fun ((_, _, analyzed) as p) ->
            let int_analyzed = SPC.set_to_int_result SPC.NodeSet.cardinal analyzed in
            let match_rec = SPC.get_matching_recall analyzed in
            (p, (M.precision int_analyzed, M.recall int_analyzed, match_rec))
        ) filtered_merged_results
        in
        let sort a b =
            let _, _, a = fst a in
            let _, _, b = fst b in
            let score1 = Patch_order.compute_score [a; b] in
            let score2 = Patch_order.compute_score [b; a] in
            if score1 > score2 then 1 else if score1 < score2 then -1 else 0
        in
        let sorted_results = Patch_order.topo_sort sort results_with_score in
        let unapplied_indices = Patch_order.index_of_unapplied
            (List.map (fun ((_, _, a), _) -> a) sorted_results)
        in
        let sorted_results = Utils.filteri (fun i _ ->
            not (List.mem i unapplied_indices)
        ) sorted_results
        in

        let get_patch_analyzed_str ((gids, patch, analyzed), (precision, recall, matching)) =
            get_patch_str (gids, patch, Some(analyzed)) ^
            (Printf.sprintf "// Recall: %.2f, Precision: %.2f, Matching recall: %.2f\n"
                recall precision matching
            ) ^
            "\n// ---------------------------------------------\n"
        in
        (
            List.map get_patch_str raw_results,
            List.map get_patch_analyzed_str sorted_results
        )
    in


    let full_patch = String.concat "" merged_result in
    (*Debug.print ("semantic patch: \n" ^ full_patch);*)
    let (temp_sp,oc_cocci) = Filename.open_temp_file "temp_sp" ".cocci" in
    Printf.fprintf oc_cocci "%s" full_patch;
    close_out oc_cocci;
    let specfile = if (!Global.specfile_final_test <> "")
        then !Global.specfile_final_test
        else !Global.specfile_test
    in
    Sys.chdir !Global.final_test_dir;
    let cocci_result = SPC.run_coccinelle temp_sp specfile in
    let analyzed_result = SPC.analyze_cocci_result cocci_result specfile in
    Sys.remove temp_sp;
    let patch_object = {
        text = full_patch;
        nb_rules = List.length merged_result;
        cocci_result = cocci_result;
        analyzed = analyzed_result;
    }
    in
    write_semantic_patch_to_files patch_object
