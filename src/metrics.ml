type 'a analyzed_result = {
    true_positives: 'a;
    true_negatives: 'a;
    false_positives: 'a;
    false_negatives: 'a;
}

let recall (res: int analyzed_result) : float =
    let top = float_of_int res.true_positives in
    let bottom = float_of_int (res.true_positives + res.false_negatives) in
    top /. bottom

let precision (res: int analyzed_result) : float =
    let top = float_of_int res.true_positives in
    let bottom = float_of_int (res.true_positives + res.false_positives) in
    top /. bottom

let fscore (beta: float) (res: int analyzed_result) : float =
    let bsquare = beta *. beta in
    let pre = precision res in
    let re = recall res in
    (1. +. bsquare)*.(pre*.re)/.(bsquare*.pre +. re)
