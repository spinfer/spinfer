module CFC = Control_flow_c
module SCFCB = Spinfer_control_flow_c_build
module Ast = Ast_cocci
module AA = Abstract_ast

type node_id = int
type subfrag_id = int

type structure =
  | BlockStart
  | BlockEnd
  | Fake of string

type content =
  | Pattern of AA.pattern
  | String of string
  | Structure of structure

type node = {
  node_id: node_id;
  diff_type: CFGDiff.prefix;
  content: content;
}


type subfragment = {
  nodes: node list;
  edges: (node_id * node_id) list;
}

type frag_ids = int * int
type cfg_ids = int * int
type example = (frag_ids * cfg_ids) list


type fragment = {
  subfragments: subfragment list;
  entry: frag_ids;
  exit: frag_ids;
  secondary_connections: (frag_ids * frag_ids) list;
  examples: example list
}

let stringify_fragment (fragment: fragment) : string =
  let strings = List.mapi (fun subfrag_id subfragment ->
      List.map (fun node ->
          let matched_nodes = List.map (fun example ->
              List.assoc (subfrag_id, node.node_id) example
            ) fragment.examples
          in
          let str = match node.content with
            | String(str) -> str
            | Pattern(pattern) -> Print4debug.print_ast_debug pattern
            | Structure(structure) ->
              begin match structure with
                | BlockStart ->"{"
                | BlockEnd -> "}"
                | Fake(str) -> str
              end
          in
          (CFGDiff.stringify_diff_prefix node.diff_type) ^
          str ^ " " ^ (Dumper.dump matched_nodes)
        ) subfragment.nodes
    ) fragment.subfragments
  in
  String.concat " | " (List.flatten strings)


let partition_with_indices (f: fragment) (indices: int list)
  : ((fragment option) * (fragment option)) =
  let frag_to_frag_opt f =
    if f.examples = [] then None else Some(f)
  in
  let true_ex, false_ex = List.partition (fun (i, _) ->
      List.mem i indices
    ) (Utils.list_index f.examples)
  in
  let true_f = {f with examples=Utils.list_unindex true_ex} in
  let false_f = {f with examples=Utils.list_unindex false_ex} in
  (frag_to_frag_opt true_f, frag_to_frag_opt false_f)


let extract_ids_for_port (port: frag_ids) (f: fragment): cfg_ids list =
    List.map (List.assoc port) f.examples

let build_single_fragment (pattern : AA.pattern) (prefix : CFGDiff.prefix)
  : fragment =
  let node = {
    node_id = 0;
    diff_type = prefix;
    content = Pattern(pattern);
  }
  in

  let matched_nodes = pattern.AA.matched_nodes in
  let examples = List.map (fun cfg_ids ->
      [((0, 0), cfg_ids)]
    ) (AA.NodeIdSet.elements matched_nodes)
  in

  let sub_fragment = {
    nodes = [node];
    edges = [];
  }
  in
  {
    subfragments = [sub_fragment];
    entry = (0, 0);
    exit = (0, 0);
    secondary_connections = [];
    examples = examples
  }


let build_loop_fragment (pattern: AA.pattern) (prefix : CFGDiff.prefix)
    (code : Code.t) : fragment =
  let matched_nodes = pattern.AA.matched_nodes in
  let branches_prefix = ref None in
  let examples = List.map (fun (graph_id, node_id) ->
      let graph, _, _ =
        List.find (fun (_, _, id) -> id = graph_id) code
      in
      let branching_id, _ = CFGDiff.KeyEdgeSet.choose (graph#successors node_id) in
      let succs = CFGDiff.KeyEdgeSet.elements (graph#successors branching_id) in
      if (List.length succs < 2) then assert false;
      (*A loop header always has two successors*)
      let (succ_0, _) = List.nth succs 0 in
      let (succ_1, _) = List.nth succs 1 in
      (* FIXME: that doesn't feel safe *)
      let in_loop_id, out_loop_id =
        if (succ_0 < succ_1) then (succ_0, succ_1) else (succ_1, succ_0)
      in
      let in_loop_prefix = (fst (CFC.KeyMap.find in_loop_id graph#nodes)) in

      if (
        in_loop_prefix = prefix
        || prefix = CFGDiff.Before && in_loop_prefix = CFGDiff.Both
      )
      then branches_prefix := Some(in_loop_prefix);

      let header_labels =
        CFC.extract_labels (snd (CFC.KeyMap.find node_id graph#nodes))
      in
      let in_loop_end_id =
        let res = CFGDiff.KeyEdgeSet.fold (fun elt accu ->
            let key, _ = elt in
            try
              let labels =
                CFC.extract_labels (snd (CFC.KeyMap.find key graph#nodes))
              in
              if (labels = header_labels && (fst elt) <> node_id)
              then begin match accu with
                | Some(_) -> failwith "Found two in_loop_end"
                | None -> Some(key)
              end
              else accu
            with Not_found -> accu
          ) (graph#predecessors branching_id) None
        in
        match res with
        | Some(id) -> id
        | None ->
          Printf.eprintf "BUILD_LOOP: Error at node %d of graph %d\n%!"
            node_id graph_id;
          failwith "No in_loop_end found"
      in
      [
        ((0, 0), (graph_id, node_id));
        ((0, 1), (graph_id, branching_id));
        ((0, 2), (graph_id, in_loop_id));
        ((0, 3), (graph_id, in_loop_end_id));
        ((0, 4), (graph_id, out_loop_id))
      ]
    ) (AA.NodeIdSet.elements matched_nodes)
  in

  let loop_header = {
    node_id = 0;
    diff_type = prefix;
    content = Pattern(pattern) ;
  }
  in

  match !branches_prefix with
  | None -> {
      subfragments = [{
          nodes=[loop_header];
          edges=[];
        }];
      entry = (0, 0);
      exit = (0, 0);
      secondary_connections = [];
      examples = examples
    }
  | Some(prefix) ->
    let branching = {
      node_id = 1;
      diff_type = prefix;
      content = Structure(Fake("loop_branching"));
    }
    in
    let in_loop_start = {
      node_id = 2;
      diff_type = prefix;
      content = Structure(BlockStart);
    }
    in
    let in_loop_end = {
      node_id = 3;
      diff_type = prefix;
      content = Structure(BlockEnd);
    }
    in
    let out_loop = {
      node_id = 4;
      diff_type = prefix;
      content = Structure(Fake("out_loop"));
    }
    in
    let sub_fragment = {
      nodes = [loop_header; branching; in_loop_start; in_loop_end; out_loop];
      edges = [(0,1); (1,2); (3,1); (1,4)];
    }
    in
    {
      subfragments = [sub_fragment];
      entry = (0, 0);
      exit = (0, 4);
      secondary_connections = [((0,2), (0,3))];
      examples = examples
    }


let build_if_fragment (pattern : AA.pattern) (prefix : CFGDiff.prefix)
    (code : Code.t) : fragment list =
  let matched_nodes = pattern.AA.matched_nodes in
  let examples_htbl = Hashtbl.create 30 in

  List.iter (fun (graph_id, node_id) ->
      let keymap_find (f: CFC.KeyMap.key -> 'a -> bool) (map: 'a CFC.KeyMap.t)
        : cfg_ids =
        let result = CFC.KeyMap.fold (fun key node accu ->
            match accu with
            | Some(_) -> accu
            | None ->
              if f key node then Some((key, node)) else None
          ) map None
        in
        match result with
        | Some(result_key, _) -> (graph_id, result_key)
        | None -> raise Not_found
      in

      let find_similar (sim_node: CFC.node2) (str: string)
          (map: CFGDiff.diff_node CFC.KeyMap.t)
        : cfg_ids =
        let full_sim_node = SCFCB.mk_node sim_node [] [] str in
        keymap_find (fun _ node ->
            Cocci_addon.node_semantic_equality (snd node) full_sim_node
          ) map
      in

      let graph, _, _ =
        List.find (fun (_, _, id) -> id = graph_id) code
      in
      let nodes = graph#nodes in

      let find_else successors = CFGDiff.KeyEdgeSet.fold (fun (key, _) accu ->
          let prefix, cfg_node = CFC.KeyMap.find key nodes in
          begin match CFC.unwrap cfg_node, accu with
            | (CFC.Else _, None) -> Some((prefix, key))
            | (CFC.Else _, Some(_)) -> assert false
            | (CFC.TrueNode _, _)
            | (CFC.FallThroughNode, _) -> accu
            | _-> assert false
          end
        ) successors None
      in
      let find_labels key =
        let cfg_node = snd (CFC.KeyMap.find key nodes) in
        CFC.extract_labels cfg_node
      in

      let labels_if = find_labels node_id in

      let if_nodes = CFC.KeyMap.filter (fun _ node ->
          if (CFC.extract_labels (snd node)) = labels_if
          then true
          else false
        ) nodes
      in

      let succs = graph#successors node_id in
      let else_node = find_else succs in

      let if_type = match CFGDiff.KeyEdgeSet.cardinal succs, else_node with
        | (2, None) -> `TwoBranchesNoElse
        | (2, Some(else_prefix, _)) when else_prefix = prefix
          -> `TwoBranchesElse
        | (2, Some(else_prefix, _)) when else_prefix <> prefix
          -> `TwoBranchesHeaderChange
        | (3, Some(else_prefix, _)) when else_prefix = CFGDiff.After ->
          `ThreeBranchesElseAdded
        | (3, Some(else_prefix, _)) when else_prefix = CFGDiff.Before ->
          `ThreeBranchesElseRemoved
        | _ -> failwith "Unexpected if construct"
      in

      let else_nodes = match else_node with
        | None -> CFC.KeyMap.empty
        | Some(_, key) ->
          let labels_else = find_labels key in
          CFC.KeyMap.filter (fun _ node ->
              if (CFC.extract_labels (snd node)) = labels_else
              then true
              else false
            ) nodes
      in

      let get_ifnodes () =
        [
          (graph_id, node_id);
          keymap_find (fun _ node ->
              match node with
              | (_, ((CFC.EndStatement _, _), _)) -> true
              | _ -> false
            ) if_nodes
        ]
      in
      let get_truenodes () =
        let r_false = ref false in
        [
          find_similar (CFC.TrueNode(r_false)) SCFCB.then_start_str if_nodes;
          find_similar (CFC.TrueNode(r_false)) SCFCB.then_end_str if_nodes
        ]
      in
      let get_fallthroughnodes () =
        [
          find_similar (CFC.FallThroughNode) SCFCB.fallthrough_start_str if_nodes;
          find_similar (CFC.FallThroughNode) SCFCB.fallthrough_end_str if_nodes
        ]
      in
      let get_elsenodes () =
        [
          keymap_find (fun _ node ->
              match node with
              | (_, ((CFC.Else _, _), _)) -> true
              | _ -> false
            ) else_nodes;
          find_similar (CFC.FalseNode) SCFCB.else_start_str else_nodes;
          find_similar (CFC.FalseNode) SCFCB.else_end_str else_nodes
        ]
      in

      match if_type, prefix with
      | (`TwoBranchesNoElse, _) ->
        Hashtbl.add examples_htbl if_type [
          get_ifnodes ();
          get_truenodes ();
          get_fallthroughnodes()
        ]
      (* FIXME: Hack *)
      | (`TwoBranchesHeaderChange, _)
      | (`TwoBranchesElse, _) ->
      (*
        Hashtbl.add examples_htbl if_type [
      *)
        Hashtbl.add examples_htbl `TwoBranchesElse [
          get_ifnodes ();
          get_truenodes ();
          get_elsenodes ()
        ]
      (*
      | (`TwoBranchesHeaderChange, CFGDiff.Before) ->
        Hashtbl.add examples_htbl if_type [
          get_ifnodes ();
          get_truenodes ();
          get_elsenodes ()
        ]
      | (`TwoBranchesHeaderChange, CFGDiff.After) ->
        Hashtbl.add examples_htbl `HeaderOnly [
          get_ifnodes ();
          get_truenodes ()
        ]
      *)
      | (`ThreeBranchesElseAdded, CFGDiff.Both)
      | (`ThreeBranchesElseRemoved, CFGDiff.Both) ->
        Hashtbl.add examples_htbl if_type [
          get_ifnodes ();
          get_truenodes ();
          get_fallthroughnodes();
          get_elsenodes ()
        ]
      | _ -> assert false
    ) (AA.NodeIdSet.elements matched_nodes);

  let mk_node node_id diff_type content = {node_id; diff_type; content} in
  let node_if = mk_node 0 prefix (Pattern(pattern)) in

  let list_noelse =
    let l = Hashtbl.find_all examples_htbl `TwoBranchesNoElse in
    if l = []
    then []
    else
      let examples = List.rev_map (fun example ->
          let if_start, if_end, then_start, then_end,
              fall_start, fall_end =
            match example with
            | [[a; b]; [c; d]; [e; f]] -> (a, b, c, d, e, f)
            | _ -> assert false
          in
          [
            ((0, 0), if_start);
            ((0, 1), then_start);
            ((0, 2), fall_start);
            ((0, 3), then_end);
            ((0, 4), fall_end);
            ((0, 5), if_end)
          ]
        ) l
      in
      [{
        subfragments = [
          {
            nodes = [
              node_if;
              mk_node 1 prefix (Structure(BlockStart));
              mk_node 2 prefix (Structure(Fake("fall start")));
              mk_node 3 prefix (Structure(BlockEnd));
              mk_node 4 prefix (Structure(Fake("fall end")));
              mk_node 5 prefix (Structure(Fake("if end")))
            ];
            edges = [(0, 1); (0, 2); (2, 4); (3, 5); (4, 5)]
          }
        ];
        entry = (0, 0);
        exit = (0, 5);
        secondary_connections = [((0, 1), (0, 3))];
        examples = examples
      }]
  in

  let list_else =
    let l = Hashtbl.find_all examples_htbl `TwoBranchesElse in
    if l = []
    then []
    else
      let examples = List.rev_map (fun example ->
          let if_start, if_end, then_start, then_end,
              else_node, else_start, else_end =
            match example with
            | [[a; b]; [c; d]; [e; f; g]] -> (a, b, c, d, e, f, g)
            | _ -> assert false
          in
          [
            (* First subfragment *)
            ((0, 0), if_start);
            ((0, 1), then_start);
            ((0, 2), else_node);
            ((0, 3), else_start);

            (* Second subfragment *)
            ((1, 4), then_end);
            ((1, 5), else_end);
            ((1, 6), if_end)
          ]
        ) l
      in
      [{
        subfragments = [
          {
            nodes = [
              node_if;
              mk_node 1 prefix (Structure(BlockStart));
              mk_node 2 prefix (String("else"));
              mk_node 3 prefix (Structure(BlockStart))
            ];
            edges = [(0, 1); (0, 2); (2, 3)]
          };
          {
            nodes = [
              mk_node 4 prefix (Structure(BlockEnd));
              mk_node 5 prefix (Structure(BlockEnd));
              mk_node 6 prefix (Structure(Fake("if end")))
            ];
            edges = [(4, 6); (5, 6)]
          }
        ];
        entry = (0, 0);
        exit = (1, 6);
        secondary_connections = [
          ((0, 1), (1, 4));
          ((0, 3), (1, 5))
        ];
        examples = examples
      }]
  in

  let list_common_else =
    let l = Hashtbl.find_all examples_htbl `TwoBranchesHeaderChange in
    if l = []
    then []
    else
      let examples = List.rev_map (fun example ->
          let if_start, if_end, then_start, then_end,
              else_node, else_start, else_end =
            match example with
            | [[a; b]; [c; d]; [e; f; g]] -> (a, b, c, d, e, f, g)
            | _ -> assert false
          in
          [
            (* First subfragment *)
            ((0, 0), if_start);
            ((0, 1), then_start);
            ((0, 2), else_node);
            ((0, 3), else_start);

            (* Second subfragment *)
            ((1, 4), then_end);
            ((1, 5), else_end);
            ((1, 6), if_end)
          ]
        ) l
      in
      [{
        subfragments = [
          {
            nodes = CFGDiff.[
                node_if;
                mk_node 1 Before (Structure(BlockStart));
                mk_node 2 Both (String("else"));
                mk_node 3 Both (Structure(BlockStart))
              ];
            edges = [(0, 1); (0, 2); (2, 3)]
          };
          {
            nodes = CFGDiff.[
                mk_node 4 Before (Structure(BlockEnd));
                mk_node 5 Both (Structure(BlockEnd));
                mk_node 6 Before (Structure(Fake("if end")))
              ];
            edges = [(4, 6); (5, 6)]
          }
        ];
        entry = (0, 0);
        exit = (1, 6);
        secondary_connections = [
          ((0, 1), (1, 4));
          ((0, 3), (1, 5))
        ];
        examples = examples
      }]
  in

  let list_header =
    let l = Hashtbl.find_all examples_htbl `HeaderOnly in
    if l = []
    then []
    else
      let examples = List.rev_map (fun example ->
          let if_start, if_end, then_start, then_end =
            match example with
            | [[a; b]; [c; d]] -> (a, b, c, d)
            | _ -> assert false
          in
          [
            ((0, 0), if_start);
            ((0, 1), then_start);

            ((1, 2), then_end);
            ((1, 3), if_end)
          ]
        ) l
      in
      [{
        subfragments = [
          {
            nodes = [
              node_if;
              mk_node 1 prefix (Structure(BlockStart));
            ];
            edges = [(0, 1)]
          };
          {
            nodes = [
              mk_node 2 prefix (Structure(BlockEnd));
              mk_node 3 prefix (Structure(Fake("if end")))
            ];
            edges = [(2, 3)]
          }
        ];
        entry = (0, 0);
        exit = (1, 3);
        secondary_connections = [((0, 1), (1, 2))];
        examples = examples
      }]
  in

  let get_examples_else_added_removed =
    List.rev_map (fun example ->
        let if_start, if_end, then_start, then_end,
            fall_start, fall_end, else_node, else_start, else_end =
          match example with
          | [[a; b]; [c; d]; [e; f]; [g; h; i]] ->
            (a, b, c, d, e, f, g, h, i)
          | _ -> assert false
        in
        [
          ((0, 0), if_start);
          ((0, 1), then_start);
          ((0, 2), fall_start);
          ((0, 3), else_node);
          ((0, 4), else_start);
          ((0, 5), then_end);
          ((0, 6), fall_end);
          ((0, 7), else_end);
          ((0, 8), if_end)
        ]
      )
  in
  let list_else_added =
    let l = Hashtbl.find_all examples_htbl `ThreeBranchesElseAdded in
    if l = []
    then []
    else
      let examples = get_examples_else_added_removed l in
      [{
        subfragments = [
          {
            nodes = CFGDiff.[
                node_if;
                mk_node 1 Both (Structure(BlockStart));
                mk_node 2 Before (Structure(Fake("fall start")));
                mk_node 3 After (String("else"));
                mk_node 4 After (Structure(BlockStart));
                mk_node 5 Both (Structure(BlockEnd));
                mk_node 6 Before (Structure(Fake("fall end")));
                mk_node 7 After (Structure(BlockEnd));
                mk_node 8 Both (Structure(Fake("if end")))
              ];
            edges = [
              (0, 1); (0, 2); (0, 3); (3, 4); (2, 6);
              (5, 8); (6, 8); (7, 8)
            ]
          }
        ];
        entry = (0, 0);
        exit = (0, 8);
        secondary_connections = [
          ((0, 1), (0, 5));
          ((0, 3), (0, 7))
        ];
        examples = examples
      }]
  in
  let list_else_removed =
    let l = Hashtbl.find_all examples_htbl `ThreeBranchesElseRemoved in
    if l = []
    then []
    else
      let examples = get_examples_else_added_removed l in
      [{
        subfragments = [
          {
            nodes = CFGDiff.[
                node_if;
                mk_node 1 Both (Structure(BlockStart));
                mk_node 2 After (Structure(Fake("fall start")));
                mk_node 3 Before (String("else"));
                mk_node 4 Before (Structure(BlockStart));
                mk_node 5 Both (Structure(BlockEnd));
                mk_node 6 After (Structure(Fake("fall end")));
                mk_node 7 Before (Structure(BlockEnd));
                mk_node 8 Both (Structure(Fake("if end")))
              ];
            edges = [
              (0, 1); (0, 2); (0, 3); (3, 4); (2, 6);
              (5, 8); (6, 8); (7, 8)
            ]
          }
        ];
        entry = (0, 0);
        exit = (0, 8);
        secondary_connections = [
          ((0, 1), (0, 5));
          ((0, 3), (0, 7))
        ];
        examples = examples
      }]
  in
  list_noelse @ list_else @ list_common_else @
  list_header @ list_else_added @ list_else_removed

let build_switch_fragment (pattern : AA.pattern) (prefix : CFGDiff.prefix)
    (code : Code.t) : fragment =
  let switch_header = {
    node_id = 0;
    diff_type = prefix;
    content = Pattern(pattern);
  }
  in
  let end_switch = {
    node_id = 1;
    diff_type = prefix;
    content = Structure(Fake("end switch"));
  }
  in
  let matched_nodes = pattern.AA.matched_nodes in
  let examples = List.map (fun (graph_id, node_id) ->
      let graph, _, _ =
        List.find (fun (_, _, id) -> id = graph_id) code
      in
      let nodes = graph#nodes in
      let switch_node = CFC.KeyMap.find node_id nodes in
      let labels_switch = CFC.extract_labels (snd switch_node) in

      let key_end_switch, _ = List.find (fun (_, (_, cfg_node)) ->
          if (CFC.extract_labels cfg_node = labels_switch)
          then match CFC.unwrap cfg_node with
            | CFC.SwitchHeader(_,_) -> false
            | CFC.EndStatement(_) -> true
            | _ -> assert false
          else false
        ) (CFC.KeyMap.bindings nodes)
      in
      [
        ((0, 0), (graph_id, node_id));
        ((1, 1), (graph_id, key_end_switch))
      ]
    ) (AA.NodeIdSet.elements matched_nodes)
  in

  let head_sub_fragment = {
    nodes = [switch_header];
    edges = [];
  }
  in
  let tail_sub_fragment = {
    nodes = [end_switch];
    edges = [];
  }
  in
  {
    subfragments = [head_sub_fragment; tail_sub_fragment];
    entry = (0, 0);
    exit = (1, 1);
    secondary_connections = [((0, 0), (1, 1))];
    examples = examples
  }


let build_function_fragment (pattern: AA.pattern) (prefix: CFGDiff.prefix)
    (code: Code.t) : fragment =

  let matched_nodes = pattern.AA.matched_nodes in

  let graph_keys = ref [] in
  let block_prefix = ref None in

  List.iter (fun (graph_id, node_id) ->
      let graph, _, _ =
        List.find (fun (_, _, id) -> id = graph_id) code
      in
      let nodes = graph#nodes in
      let succs = CFGDiff.KeyEdgeSet.elements (graph#successors node_id) in

      if (List.length succs) <> 1
      then failwith "Incorrect number of successors";

      let start_id, _ = List.nth succs 0 in
      graph_keys :=
        (graph_id, (node_id, start_id, CFGDiff.last_key graph))::!graph_keys;

      let start_prefix, _ = CFC.KeyMap.find start_id nodes in
      if (
        start_prefix = prefix ||
        (start_prefix = CFGDiff.Both && prefix = CFGDiff.Before)
      )
      then block_prefix := Some(start_prefix)
      else ()
    ) (AA.NodeIdSet.elements matched_nodes);

  let node_header = {
    node_id = 0;
    diff_type = prefix;
    content = Pattern(pattern);
  }
  in
  match !block_prefix with
  | None ->
    let head_sub_fragment = {
      nodes = [node_header];
      edges = [];
    }
    in
    {
      subfragments = [head_sub_fragment];
      entry = (0, 0);
      exit = (0, 0);
      secondary_connections = [];
      examples = List.rev_map (fun (graph_id, (node_id, _, _)) ->
          [
            ((0, 0), (graph_id, node_id))
          ]
        ) !graph_keys
    }

  | Some(prefix) ->
    let node_start = {
      node_id = 1;
      diff_type = prefix;
      (*content = Structure(BlockStart("", false));*)
      content = String("{");
    }
    in
    let node_end = {
      node_id = 2;
      diff_type = prefix;
      (*content = Structure(BlockEnd(false));*)
      content = String("}");
    }
    in

    let head_sub_fragment = {
      nodes = [node_header; node_start];
      edges = [(0,1)];
    }
    in

    let tail_sub_fragment = {
      nodes = [node_end];
      edges = [];
    }
    in
    {
      subfragments = [head_sub_fragment; tail_sub_fragment];
      entry = (0, 0);
      exit = (1, 2);
      secondary_connections = [((0, 1), (1, 2))];
      examples = List.rev_map (fun (graph_id, (node_id, start_id, end_id)) ->
          [
            ((0, 0), (graph_id, node_id));
            ((0, 1), (graph_id, start_id));
            ((1, 2), (graph_id, end_id))
          ]
        ) !graph_keys
    }


let build_fragment (pattern: AA.pattern) (prefix : CFGDiff.prefix) (code : Code.t)
        : fragment list  =
    match Ast.unwrap pattern.AA.ast with
    | Ast.FunHeader(_) -> [build_function_fragment pattern prefix code]
    | Ast.WhileHeader(_) | Ast.IteratorHeader _
    | Ast.ForHeader(_) -> [build_loop_fragment pattern prefix code]
    | Ast.IfHeader(_) -> build_if_fragment pattern prefix code
    | Ast.SwitchHeader(_) -> [build_switch_fragment pattern prefix code]
    | _ -> [build_single_fragment pattern prefix]


let build_A_list (code : Code.t)
        : (fragment list * fragment list * fragment list) =
    let forest = Code.code_to_forest code in
    let interesting_forest = List.filter (fun ((_, node), _) ->
        match CFC.unwrap node with
        | CFC.SeqStart _ | CFC.SeqEnd _
        | CFC.Fake | CFC.EndStatement _
        | CFC.FallThroughNode | CFC.LoopFallThroughNode | CFC.InLoopNode
        | CFC.TrueNode _ | CFC.FalseNode | CFC.Else _ -> false
        | _ -> true
    ) forest
    in
    let removed_nodes, added_nodes, context_nodes =
        Code.filter_modified_code interesting_forest
    in
    let try_build_fragment prefix accu pattern =
    try (build_fragment pattern prefix code) @ accu
    with e ->
        Printf.eprintf "[ERROR] Error building fragment %s\n%!"
            (Printexc.to_string e);
        accu
    in

    (* Need to use this function now to populate global hash tables *)
    Find_context.prepare removed_nodes added_nodes;

    let clusters_removed = if removed_nodes <> []
    then Clusterize.clusterize_python removed_nodes
    else []
    in
    let removed_patterns = AA.mine_patterns clusters_removed in
    let removed_fragments =
        List.fold_left (try_build_fragment CFGDiff.Before) [] removed_patterns
    in

    let clusters_added = if added_nodes <> []
    then Clusterize.clusterize_python added_nodes
    else []
    in
    let added_patterns = AA.mine_patterns clusters_added in
    let added_fragments =
        List.fold_left (try_build_fragment CFGDiff.After) [] added_patterns
    in

    let clusters_context =
        Find_context.fit_context_to_patterns context_nodes added_patterns
    in
    let context_patterns = AA.mine_patterns clusters_context in
    let context_fragments =
        List.fold_left (try_build_fragment CFGDiff.Both) [] context_patterns
    in

    (removed_fragments, context_fragments, added_fragments)
