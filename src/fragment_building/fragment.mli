module CFC = Control_flow_c
module SCFCB = Spinfer_control_flow_c_build
module Ast = Ast_cocci
module AA = Abstract_ast

type node_id = int
type subfrag_id = int

type structure =
  | BlockStart
  | BlockEnd
  | Fake of string

type content =
  | Pattern of AA.pattern
  | String of string
  | Structure of structure

type node = {
  node_id: node_id;
  diff_type: CFGDiff.prefix;
  content: content;
}


type subfragment = {
  nodes: node list;
  edges: (node_id * node_id) list;
}

type frag_ids = int * int
type cfg_ids = int * int
type example = (frag_ids * cfg_ids) list


type fragment = {
  subfragments: subfragment list;
  entry: frag_ids;
  exit: frag_ids;
  secondary_connections: (frag_ids * frag_ids) list;
  examples: example list
}

val stringify_fragment : fragment -> string

val partition_with_indices : fragment -> int list
  -> ((fragment option) * (fragment option))
val extract_ids_for_port : frag_ids -> fragment -> cfg_ids list

val build_A_list : Code.t -> (fragment list * fragment list * fragment list)
