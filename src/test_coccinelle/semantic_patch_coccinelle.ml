(**
 @author Van-Anh T. Nguyen
*)

type file_name = string
type fun_name = string
type uid = int
type tgroup = (string * string) list

(*
 * Set of node to compute metrics on.
 * As file name differs between before and after of the same change,
 * we use a unique id (uid) to distinguish between examples.
 *)
module NodeSet = Utils.UnorderedSetMake(struct
    type t = (CFGDiff.diff_node * uid * file_name * fun_name)
    let equal = (fun (node1, uid1, file1, fun1) (node2, uid2, file2, fun2) ->
        uid1 = uid2 && fun1 = fun2 &&
        CFGDiff.equal_diff_nodes node1 node2
    )
end)

module LocSet = Set.Make(struct
    type t = (uid * fun_name)
    let compare = compare
end)

module M = Metrics
module CFC = Control_flow_c

type analyzed_set = NodeSet.t M.analyzed_result
let empty_analyzed_set = {
    M.true_positives = NodeSet.empty;
    M.true_negatives = NodeSet.empty;
    M.false_positives = NodeSet.empty;
    M.false_negatives = NodeSet.empty;
}

type cocci_result = {
    logs: string list;
    modified_files: string list;
    status: int;
}

type safety_status =
    | Safe
    | Unsafe of int
    | Unknown
    | UnboundAdded

let execute_coccinelle file_semantic_patch file_groups =
  (* Execute coccinelle and get output and status *)
    let param_iso =
        if !Global.iso_file <> ""
        then "--iso-file " ^ !Global.iso_file
        else ""
    in
    let param_builtins =
        if !Global.builtins_file <> ""
        then "--macro-file-builtins " ^ !Global.builtins_file
        else ""
    in
    let command_cocci = Printf.sprintf
        ("%s --very-quiet --timeout 30 --out-place --include-headers "
        ^^ " --no-show-diff --use-cache --allow-inconsistent-paths "
        ^^ "%s %s --sp-file %s --file-groups %s 2>&1")
        !Global.spatch_path param_iso param_builtins file_semantic_patch file_groups
    in
    (if !Debug.show_logs then Printf.eprintf "command: %s\n" command_cocci);
    let process_chan = Unix.open_process_in command_cocci in

    let process_out = ref [] in
    let rec accumulate_out () =
        let line = input_line process_chan in
        process_out := line::!process_out;
        accumulate_out ()
    in
    try accumulate_out ()
    with End_of_file ->
        let process_status = Unix.close_process_in process_chan in
        match process_status with
        | Unix.WEXITED(process_status) -> (process_status, (List.rev !process_out))
        | _ -> failwith("Spatch stopped")


let run_coccinelle (file_semantic_patch: string) (specfile: string)
        : cocci_result =
    let pairs_of_file =
        if (specfile <> "")
        then Specfile.read_spec specfile
        else []
    in
    let before_files, after_files = List.split pairs_of_file in
    let file_groups_path = (Debug.get_tmp_directory () ^ "/file_groups") in

    (* Cleanup old res files *)
    let res_files = List.map (fun str -> str ^ ".cocci_res") before_files in
    ignore (Sys.command ("rm -f "^(String.concat " " res_files)));
    ignore (Sys.command ("rm -f " ^ file_groups_path));
    let chan = open_out file_groups_path in

    output_string chan (String.concat "\n\n" before_files);
    close_out chan;
    let cocci_status, cocci_output =
        execute_coccinelle file_semantic_patch file_groups_path
    in

    if !Debug.show_logs
    then begin
        Printf.eprintf "Trying:\n"; flush stderr;
        List.iter
            (function x -> Printf.eprintf "%s\n" x)
            (Common.cmd_to_list ("cat "^file_semantic_patch));
        Printf.eprintf "Obtained:\n"; flush stderr;
        List.iter (fun x -> Printf.eprintf "%s\n" x) cocci_output;
        Printf.eprintf "\n"; flush stderr;
    end;

    let modified_files = List.filter (fun file ->
        Sys.file_exists (file ^ ".cocci_res")
    ) before_files
    in
    {
        logs = cocci_output;
        modified_files = modified_files;
        status = cocci_status;
    }


let analyze_file_list (files: (string * string * string option) list)
        : analyzed_set =
    let extract_marked_nodes
            ((before, after): CFGDiff.diff_program * CFGDiff.diff_program)
            (uid: int)
            : NodeSet.t =
        let filter_nodes = List.filter (fun ((prefix, node), _, _, _) ->
            match prefix with
            | CFGDiff.Before | CFGDiff.After ->
                begin match CFC.unwrap node with
                (* We don't care about changes in braces *)
                | CFC.SeqStart _ | CFC.SeqEnd _ -> false
                | node when CFGDiff.check_if_cocci_created node -> false
                | _ -> true
                end
            | _ -> false
        )
        in
        let before, after =
            if !Global.feasible_metrics
            then
                let paired_functions = CFGDiff.pair_functions (fst before) (fst after) in
                (
                    (Utils.assoc_left paired_functions, snd before),
                    (Utils.assoc_right paired_functions, snd after)
                )
            else before, after
        in
        let file_name = fst (snd before) in
        let before_set = List.fold_left (fun initial_set before ->
            let fun_name =
            match CFGDiff.fun_header_of_graph before with
            | Some(def) -> Cocci_addon.stringify_name (fst def).Ast_c.f_name
            | None -> ""
            in
            let before_nodes = List.map (fun (_, node) ->
                (node, uid, file_name, fun_name)
            ) (Control_flow_c.KeyMap.bindings before#nodes)
            in
            NodeSet.union (NodeSet.of_list (filter_nodes before_nodes)) initial_set
        ) NodeSet.empty (fst before)
        in

        let after_set = List.fold_left (fun initial_set after ->
            let fun_name =
            match CFGDiff.fun_header_of_graph after with
            | Some(def) -> Cocci_addon.stringify_name (fst def).Ast_c.f_name
            | None -> ""
            in
            let after_nodes = List.map (fun (_, node) ->
                (node, uid, file_name, fun_name)
            ) (Control_flow_c.KeyMap.bindings after#nodes)
            in
            NodeSet.union (NodeSet.of_list (filter_nodes after_nodes)) initial_set
        ) NodeSet.empty (fst after)
        in

        (* FIXME: This part is not necessary is diff is accurate, but sometimes
        a node is tagged as modified even if it's not, because of different
        whitespace char. *)
        let incorrect_nodes = List.fold_left (fun accu before_elt ->
            let (_, node), uid, file, func = before_elt in
            let fake_after = (CFGDiff.After, node), uid, file, func in
            if NodeSet.mem fake_after after_set
            then NodeSet.add fake_after (NodeSet.add before_elt accu)
            else accu
        ) NodeSet.empty (NodeSet.elements before_set)
        in
        NodeSet.diff (NodeSet.union before_set after_set) incorrect_nodes
    in
    let res = List.map (fun (before, truth, gen) ->
        let uid = Global.register_files (before, truth) in
        let truth_CFGs =
            (CFG.memoized_get_CFG_from_file before),
            (CFG.memoized_get_CFG_from_file truth)
        in
        let truth_diff = Diff.memoized_exec_diff (before, truth) in
        let truth_program = CFGDiff.mark_program_nodes truth_CFGs truth_diff in
        let truth_set = extract_marked_nodes truth_program uid in

        let gen_set, mixed_set = match gen with
        | Some(gen) ->
            let gen_CFGs = (fst truth_CFGs), (CFG.get_CFG_from_file gen) in
            let gen_diff = Diff.exec_diff (before, gen) in
            let gen_program = CFGDiff.mark_program_nodes gen_CFGs gen_diff in
            let gen_set = extract_marked_nodes gen_program uid in

            let mixed_CFGs = (snd truth_CFGs), (snd gen_CFGs) in
            let mixed_diff = Diff.exec_diff (truth, gen) in
            let mixed_program = CFGDiff.mark_program_nodes mixed_CFGs mixed_diff in
            let mixed_set = extract_marked_nodes mixed_program uid in

            gen_set, mixed_set
        | None -> NodeSet.empty, NodeSet.empty
        in
        {
            M.true_positives = NodeSet.inter gen_set truth_set;
            M.true_negatives = NodeSet.empty;
            M.false_positives = NodeSet.inter gen_set mixed_set;
            M.false_negatives = NodeSet.diff truth_set gen_set;
        }
    ) files
    in

    List.fold_left (fun accu res -> {
        M.true_positives = NodeSet.union accu.M.true_positives res.M.true_positives;
        M.true_negatives = NodeSet.union accu.M.true_negatives res.M.true_negatives;
        M.false_positives = NodeSet.union accu.M.false_positives res.M.false_positives;
        M.false_negatives = NodeSet.union accu.M.false_negatives res.M.false_negatives;
    }) empty_analyzed_set res


let analyze_cocci_result (cocci_result: cocci_result) (specfile: string)
                         : NodeSet.t M.analyzed_result =
    let pairs_of_files = Specfile.read_spec specfile in

    let modified_files = List.filter (fun (before, _) ->
        List.mem before cocci_result.modified_files
    ) pairs_of_files
    in
    let files_names = List.map (fun ((before, truth) as files) ->
        (
            before,
            truth,
            if List.mem files modified_files
                then Some(before ^ ".cocci_res")
                else None
        )
    ) pairs_of_files
    in
    analyze_file_list files_names


let analyze_patch (patch: string) (specfile: string)
                  : NodeSet.t M.analyzed_result =
  let patch_file, patch_chan =
    Filename.open_temp_file "patch_test" ".cocci"
  in

  output_string patch_chan patch;
  close_out patch_chan;

  Debug.print_patch_debug (Printf.sprintf "\nRunning Coccinelle on:\n%s\n" patch);
  let cocci_result = run_coccinelle patch_file specfile in
  let res = analyze_cocci_result cocci_result specfile in
  Sys.remove patch_file;
  res

let set_to_int_result (card: 'a -> int) (set: 'a M.analyzed_result)
                      : (int M.analyzed_result) =
    {
        M.true_positives = card set.M.true_positives;
        M.true_negatives = card set.M.true_negatives;
        M.false_positives = card set.M.false_positives;
        M.false_negatives = card set.M.false_negatives;
    }


let extract_relevant (set: NodeSet.t) =
    let elements = NodeSet.elements set in
    List.sort_uniq compare
        (List.map (fun (_, uid, _, func) -> uid, func) elements)


(* Get the file name and function name of different groups in analyzed set
 * Groups are:
 * - Partial: the transformation missed some nodes in the function
 * - Missing: the transformation missed all the nodes in the function
 * - Incorrect: the transformation generated false positives in the function
 *
 * Groups are mutually exclusives and the incorrect one takes the priority.
 *)
let get_transform_groups (analyzed: analyzed_set) : tgroup * tgroup * tgroup =
    let create_table (elts: NodeSet.elt list) =
        let tbl = Hashtbl.create 30 in
        List.iter (fun (_, uid, file_name, fun_name) ->
            Hashtbl.replace tbl (uid, fun_name) file_name
        ) elts;
        tbl
    in
    let is_before (n, _, _, _) = fst n = CFGDiff.Before in

    let tp_before = create_table
        (List.filter is_before (NodeSet.elements analyzed.M.true_positives))
    in
    let tp_all = create_table (NodeSet.elements analyzed.M.true_positives)
    in
    let fn_before = create_table
        (List.filter is_before (NodeSet.elements analyzed.M.false_negatives))
    in
    let fn_all = create_table (NodeSet.elements analyzed.M.false_negatives)
    in
    let fp_all = create_table (NodeSet.elements analyzed.M.false_positives)
    in

    let in_tp = Hashtbl.mem tp_all in
    let in_fp = Hashtbl.mem fp_all in
    let in_fn = Hashtbl.mem fn_all in

    let partial_transform = Hashtbl.fold (fun ((uid, func) as key) file accu ->
        if in_fn key && not (in_fp key) then (file, func)::accu else accu
    ) tp_before []
    in
    let missing_transform = Hashtbl.fold (fun ((uid, func) as key) file accu ->
        if not (in_tp key) && not (in_fp key) then (file, func)::accu else accu
    ) fn_before []
    in
    let incorrect_transform = Hashtbl.fold (fun (uid, func) file accu ->
        let file, _ = Hashtbl.find Global.uid_files_htbl uid in
        (file, func)::accu
    ) fp_all []
    in

    partial_transform, missing_transform, incorrect_transform


let transform_node_set_to_location (set: analyzed_set)
        : LocSet.t Metrics.analyzed_result =
    let create_set_from old_set =
        LocSet.of_list (
            List.map (fun (_, uid, _, func) -> uid, func) (NodeSet.elements old_set)
        )
    in
    let contains_tp = create_set_from set.Metrics.true_positives in
    let contains_fp = create_set_from set.Metrics.false_positives in
    let contains_fn = create_set_from set.Metrics.false_negatives in
    Metrics.{
        (* Location is TP if at least one node is changed correctly, or
         * one node is changed incorrectly and function contains nodes to change
         *)
        true_positives = LocSet.union contains_tp (LocSet.inter contains_fn contains_fp);
        (* Location is FP if all nodes are FP *)
        false_positives = LocSet.diff contains_fp (LocSet.union contains_tp contains_fn);
        (* Location is FN if all nodes are FN *)
        false_negatives = LocSet.diff contains_fn (LocSet.union contains_tp contains_fp);
        true_negatives = LocSet.empty;
    }


let get_matching_recall (analyzed: analyzed_set) : float =
    let matching_examples = extract_relevant
        (NodeSet.union analyzed.M.true_positives analyzed.M.false_positives)
    in
    let filter_matching = NodeSet.filter (fun (_, uid, _, func) ->
        List.mem (uid, func) matching_examples
    )
    in
    let filtered_analyzed = { analyzed with
        M.false_negatives = filter_matching analyzed.M.false_negatives;
    }
    in
    let res = M.recall (set_to_int_result NodeSet.cardinal filtered_analyzed) in
    match classify_float res with
    | FP_normal -> res
    | _ -> 0.0


let get_fully_patched_score (analyzed: analyzed_set) : int * int =
    let all_funs = extract_relevant (NodeSet.union
        (NodeSet.union analyzed.M.true_positives analyzed.M.false_positives)
        analyzed.M.false_negatives
    )
    in
    let partially_correct_functions =
        extract_relevant analyzed.M.true_positives
    in
    let incorrect_functions = extract_relevant
        (NodeSet.union analyzed.M.false_positives analyzed.M.false_negatives)
    in
    let fully_correct_functions = List.filter (fun elt ->
        not (List.mem elt incorrect_functions)
    ) partially_correct_functions
    in
    (List.length fully_correct_functions, List.length all_funs)



let check_safety (res: int M.analyzed_result) : safety_status =
    let fp = res.M.false_positives in
    let tp = res.M.true_positives in
    let tn = res.M.true_negatives in
    match (fp + tp + tn, fp) with
    | (0, _) -> Unknown
    | (_, 0) -> Safe
    | (_, fp) -> Unsafe(fp)


let test_semantic_patch_safety (patch: string) (specfile: string)
                               : (safety_status * NodeSet.t M.analyzed_result) =
    let analysis = analyze_patch patch specfile in
    check_safety (set_to_int_result NodeSet.cardinal analysis), analysis
