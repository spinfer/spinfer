module SPC = Semantic_patch_coccinelle

let () =
    let argv = Sys.argv in
    let before, truth, after = argv.(1), argv.(2), argv.(3) in
    let analyzed_result = SPC.analyze_file_list [(before, truth, after)] in
    let recall = Metrics.recall analyzed_result in
    let precision = Metrics.precision analyzed_result in
    match SPC.check_safety analyzed_result with
    | SPC.Safe ->
        Printf.printf "Safe, recall: %.2f \n%!" recall
    | SPC.Unsafe(_) ->
        Printf.printf "Unsafe, recall: %.2f, precision: %.2f\n%!"
            recall precision
