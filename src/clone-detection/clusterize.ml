let find_node (nodes: Code.forest) (id: Code.cfg_node_id) : Code.forest_node =
    List.find (fun (_, info) -> info.Code.cfg_id = id) nodes


(** Python clustering **)
let encode_python_input (filename: string) (vectors: (string * int) list list)
        : unit =
    let pp_vector (vm: (string * int) list) : string =
        let str_value_list = List.map (fun (key, value) ->
            Printf.sprintf "\"%s\": %d" (String.escaped key) value
        ) vm
        in
        Printf.sprintf "{%s}" (String.concat ", " str_value_list)
    in
    let chan_out = open_out filename in
    List.iter (fun vector ->
        Printf.fprintf chan_out "%s\n" (pp_vector vector)
    ) vectors;
    close_out chan_out

let debug_clusters = ref false
let decode_python_output (filename: string) : int list list =
    let clusters = ref [] in
    let chan_in = open_in filename in
    let sep = Str.regexp_string " " in
    begin try
        while true do
            let line = input_line chan_in in
            if line <> ""
            then
                let ids = List.map (fun str_ids ->
                    int_of_string str_ids
                ) (Str.split sep line)
                in
                clusters := ids::!clusters
        done;
    with End_of_file -> ()
    end;
    close_in chan_in;
    if !debug_clusters
    then begin
        let lines = Common.cmd_to_list "cat cluster.in" in
        let tbl = Hashtbl.create 101 in
        List.iteri (fun i ln -> Hashtbl.add tbl i ln) lines;
        Printf.eprintf "clusters step 1 ----------------------\n";
        List.iter (function ids ->
            List.iter (function id ->
                Printf.eprintf "%s\n" (Hashtbl.find tbl id)
            ) ids;
            Printf.eprintf "\n"
        )
        !clusters;
        let lines = Common.cmd_to_list "cat aux_cluster.in" in
        let tbl = Hashtbl.create 101 in
        List.iteri (fun i ln -> Hashtbl.add tbl i ln) lines;
        Printf.eprintf "clusters step 2 ----------------------\n";
        List.iter (function ids ->
            List.iter (function id ->
                Printf.eprintf "%s\n" (Hashtbl.find tbl id)
            ) ids;
            Printf.eprintf "\n"
        )
        !clusters
    end;
    !clusters


let gen_debug_files (fin, fout, fex, frepr) (prefix: string) (nodes: Code.forest)
        : unit =
    ignore (Sys.command (Printf.sprintf "cp %s %s.in" fin prefix));
    ignore (Sys.command (Printf.sprintf "cp %s %s.out" fout prefix));
    ignore (Sys.command (Printf.sprintf "cp %s %s.json" fex prefix));
    ignore (Sys.command (Printf.sprintf "cp %s %s.repr" frepr prefix))


let clusterize_python (nodes: Code.forest) : Code.forest list =
    let file_in = "cluster.in" in
    let file_out = "cluster.out" in
    let example_file = "example_file.json" in
    let file_repr = "cluster.repr" in

    let is_before = List.exists (fun ((prefix, _), _)->
        prefix = CFGDiff.Before
    ) nodes
    in

    let oc = open_out file_repr in
    List.iter (fun ((_, node), _) ->
        let node2 = Control_flow_c.unwrap node in
        Printf.fprintf oc "%s\n" (Cocci_addon.stringify_CFG_node2 node2)
    ) nodes;
    close_out oc;

    let cmd = if not !Debug.oracle
    then
        Printf.sprintf "%s/clusterize.py %s %s %s"
            Debug.static_dir file_in file_out example_file
    else begin
        let name =
            !Debug.original_pwd ^ "/oracle"
            ^ (if is_before then ".before" else ".after")
        in
        Printf.sprintf "%s/oracle.py %s %s %s"
            Debug.static_dir name file_repr file_out
        end
    in
    let vectors, ids =
        let vectors, ids =
	  List.fold_left
	    (fun (vectors,ids) ((_, node), info) ->
	      (Subtree_printer.ast_encode node :: vectors,
	       info.Code.cfg_id :: ids))
	    ([],[]) nodes in
	let vectors = List.rev vectors in
	let ids = List.rev ids in
        let ids_htbl = Hashtbl.create 50 in
        List.iteri (fun i (gid, _) ->
            let current =
                if Hashtbl.mem ids_htbl gid
                then Hashtbl.find ids_htbl gid
                else []
            in
            Hashtbl.replace ids_htbl gid (i::current)
        ) ids;
        let examples_line_ids = Hashtbl.fold (fun _ line_no accu ->
            line_no::accu
        ) ids_htbl []
        in
        let oc = open_out example_file in
        let examples_line_str = List.map (fun group ->
            Printf.sprintf "[%s]"
                (String.concat ", " (List.map string_of_int group))
        ) examples_line_ids
        in
        Printf.fprintf oc "[%s]" (String.concat ", " examples_line_str);
        close_out oc;
        vectors, ids
    in
    encode_python_input file_in vectors;
    if Sys.command cmd <> 0
    then failwith ("Issue when calling " ^ cmd);
    let vector_ids = List.map (fun cluster ->
        List.map (List.nth ids) cluster
    ) (decode_python_output file_out)
    in
    if !Global.only_encode <> ""
    then begin
        let name =
            !Global.only_encode ^ (if is_before then ".before" else ".after")
        in
        gen_debug_files (file_in, file_out, example_file, file_repr) name nodes;
        if not is_before then exit 0
    end;
    List.map (fun cluster ->
        List.map (find_node nodes) cluster
    ) vector_ids
