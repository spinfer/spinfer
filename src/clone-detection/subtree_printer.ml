(* Yoann Padioleau, Julia Lawall
 *
 * Copyright (C) 2010, University of Copenhagen DIKU and INRIA.
 * Copyright (C) 2006, 2007, 2008, 2009 Ecole des Mines de Nantes and DIKU
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)
open Common

open Ast_c

module F = Control_flow_c

let init_depth = 10

let strlist_stack : (string * int) list Stack.t = Stack.create ()

let print_string str =
    let cur_list = Stack.pop strlist_stack in
    let new_list =
      List.rev
	(List.fold_left (fun prev (elt, i) -> (elt ^ str, i) :: prev)
	   [] cur_list) in
    Stack.push new_list strlist_stack

let pr_elem elem =
    let str = Ast_c.str_of_info elem in
    print_string str

let pr_space elem = print_string " "

let enter () = Stack.push [("", 1)] strlist_stack

let exit generic_name =
    let child_list = Stack.pop strlist_stack in
    let parent_list = Stack.pop strlist_stack in

    (* Limit the size of the children to prevent stack overflow *)
    let stack_overflow_list =
        if List.length child_list * List.length parent_list > 10000
        then []
        else child_list
    in
    let cur_list = match generic_name with
    | Some(name) -> (name, 0)::stack_overflow_list
    | None -> child_list
    in
    let new_list =
        Utils.cart_prod parent_list cur_list (fun (a, i) (b, j) -> a ^ b, i+j)
    in
    Stack.push new_list strlist_stack


let decr_stack () : unit =
    let list = Stack.pop strlist_stack in
    Stack.push (List.map (fun (a, i) -> a, i-1) list) strlist_stack


(*****************************************************************************)
(* Wrappers *)
(*****************************************************************************)
let pr2, pr2_once = Common.mk_pr2_wrappers Flag_parsing_c.verbose_unparsing

type 'a printer = int -> 'a -> unit

type pretty_printers = {
  expression      : Ast_c.expression printer;
  statement       : Ast_c.statement printer;
  ty              : Ast_c.fullType printer;
  decl            : Ast_c.declaration printer;
  param           : Ast_c.parameterType printer;
  flow            : Control_flow_c.node printer;
  funcall         : Ast_c.expression printer;
}

let mk_pretty_printers ~pr_nl ~pr_indent ~pr_outdent ~pr_unindent =
  let start_block () = () in
  let end_block   () = () in
  let indent_if_needed st f = () in

  let pp_list printer l =
    enter ();
    l +> List.iter (fun (e, opt) ->
    assert (List.length opt <= 1); (* opt must be a comma? *)
    opt +> List.iter (function x -> pr_elem x; pr_space());
    printer e);
    exit None
  in

  let pp_list2 printer l = (* no comma case *)
    enter ();
    l +> List.iter printer;
    exit None
  in

  let rec pp_do_expression ?(skip=false) fromarg n = fun ((exp, typ), ii) ->
    enter ();
    let pp_expression ?(skip=false) n e = pp_do_expression ~skip false n e in
    if n = 0 then print_string "__Expr__" else
    let n = n - 1 in
    (match exp, ii with
    | Ident (ident),         []     ->
	(match !typ with
	  (Some(ty, Ast_c.NotLocalVar),_) ->
        if n = 0 then pp_type 1 ty else pp_name ident
	| (Some(ty,_),_) ->
	    print_string "__local__";
	    if not fromarg then pp_type (n+1) ty (* pp_type will -1 *)
	| (None,_) ->
        if n = 0 then print_string "__Identifier__" else pp_name ident)
    (* only a MultiString can have multiple ii *)
    | Constant (MultiString _), is     ->
	is +> Common.print_between pr_space pr_elem
    | Constant (c),         [i]     -> pr_elem i
    | StringConstant(s,os,w),  [i1;i2] ->
	pr_elem i1;
	s +> (List.iter pp_string_fragment);
	pr_elem i2
    | FunCall (e, es), [i1;i2] -> pp_funcall false n ((exp, typ), ii)
    | CondExpr (e1, e2, e3),    [i1;i2]    ->
        pp_expression n e1; pr_space(); pr_elem i1; pr_space();
	do_option (function x -> pp_expression n x; pr_space()) e2; pr_elem i2;
        pr_space(); pp_expression n e3
    | Sequence (e1, e2),          [i]  ->
        pp_expression n e1; pr_elem i; pr_space(); pp_expression n e2
    | Assignment (e1, op, e2),    []  ->
        pp_expression n e1; pr_space(); pr_assignOp op; pr_space(); pp_expression n e2

    | Postfix  (e, op),    [i] -> pp_expression n e; pr_elem i;
    | Infix    (e, op),    [i] -> pr_elem i; pp_expression n e;
    | Unary    (e, op),    [i] -> pr_elem i; pp_expression n e
    | Binary   (e1, op, e2),    [] ->
	let n1 =
	  match Ast_c.unwrap e1 with
	    (Ident(RegularName("NULL",_)),_) -> n+1
	  | _ -> n in
	let n2 =
	  match Ast_c.unwrap e2 with
	    (Ident(RegularName("NULL",_)),_) -> n+1
	  | _ -> n in
        pp_expression n1 e1; pr_space(); pr_binaryOp op; pr_space(); pp_expression n2 e2

    | ArrayAccess    (e1, e2),   [i1;i2] ->
        pp_expression n e1; pr_elem i1; pp_expression n e2; pr_elem i2
    | RecordAccess   (e, name),     [i1] ->
        pp_expression n e; pr_elem i1; pp_name name;
    | RecordPtAccess (e, name),     [i1] ->
        pp_expression n e; pr_elem i1; pp_name name;

    | SizeOfExpr  (e),     [i] ->
	pr_elem i;
	(match Ast_c.unwrap e with
	  ParenExpr (e), _ -> ()
	| _ -> pr_space());
	pp_expression n e
    | SizeOfType  (t),     [i1;i2;i3] ->
        pr_elem i1; pr_elem i2; pp_type n t; pr_elem i3
    | Cast    (t, e),      [i1;i2] ->
        pr_elem i1; pp_type n t; pr_elem i2; pp_expression n e

    | StatementExpr (statxs, [ii1;ii2]),  [i1;i2] ->
        pr_elem i1;
        pr_elem ii1;
        statxs +> List.iter (pp_statement_seq n);
        pr_elem ii2;
        pr_elem i2;
    | Constructor (t, init), [lp;rp] ->
        pr_elem lp;
        pp_type n t;
        pr_elem rp;
	pp_init n init

    | ParenExpr (e), [i1;i2] -> pr_elem i1; pp_expression n e; pr_elem i2;

    | New   (None, t),     [i1] -> pr_elem i1; pp_argument n t
    | New   (Some ts, t),     [i1; i2; i3] ->
	pr_elem i1; pr_elem i2; pp_arg_list n ts; pr_elem i3; pp_argument n t
    | Delete(false,t), [i1] -> pr_elem i1; pr_space(); pp_expression n t
    | Delete(true,t), [i1;i2;i3] ->
	pr_elem i1; pr_space(); pr_elem i2; pr_elem i3; pr_space();
	pp_expression n t

    | Defined name, [i1] ->
        pr_elem i1; (* defined *) pr_space();
        pp_name name;
    | Defined name, [i1;i2;i3] ->
        pr_elem i1; (* defined *) pr_space();
        pr_elem i2; (* ( *)
        pp_name name;
        pr_elem i3; (* ) *)

    | (Ident (_) | Constant _ | StringConstant _ | FunCall (_,_)
    | CondExpr (_,_,_) | Sequence (_,_) | Assignment (_,_,_)
    | Postfix (_,_) | Infix (_,_) | Unary (_,_) | Binary (_,_,_)
    | ArrayAccess (_,_) | RecordAccess (_,_) | RecordPtAccess (_,_)
    | SizeOfExpr (_) | SizeOfType (_) | Cast (_,_)
    | StatementExpr (_) | Constructor _
    | ParenExpr (_) | New (_) | Delete (_,_)
    | Defined (_)),_ -> raise (Impossible 95)
    );

    if !Flag_parsing_c.pretty_print_type_info
    then begin
      pr_elem (Ast_c.fakeInfo() +> Ast_c.rewrap_str "/*");
      !typ +>
      (fun (ty,_test) -> ty +>
	Common.do_option
	  (fun (x,l) -> pp_type n x;
	    let s = match l with
	      Ast_c.LocalVar _ -> ", local"
	    | _ -> "" in
	    pr_elem (Ast_c.fakeInfo() +> Ast_c.rewrap_str s)));
      pr_elem (Ast_c.fakeInfo() +> Ast_c.rewrap_str "*/");
    end;

    (* Do not print `__Expr__;` *)
    if skip
    then exit None
    else exit (Some("__Expr__"))

  and pr_assignOp (_,ii) =
    enter ();
    let i = Common.tuple_of_list1 ii in
    pr_elem i;
    exit (Some("__AssignOp__"))

  and pr_binaryOp (_,ii) =
    let i = Common.tuple_of_list1 ii in
    pr_elem i

  and pp_arg_list n es = pp_list (pp_argument n) es

  and pp_argument n argument =
    let pp_action (ActMisc ii) = ii +> List.iter pr_elem in
    match argument with
    | Left e -> pp_do_expression true n e
    | Right weird ->
	(match weird with
	| ArgType param -> pp_param n param
	| ArgAction action -> pp_action action)

(* ---------------------- *)
  and pp_name = function
    | RegularName (s, ii) ->
        let (i1) = Common.tuple_of_list1 ii in
        pr_elem i1
    | CppConcatenatedName xs ->
        xs +> List.iter (fun ((x,ii1), ii2) ->
          ii2 +> List.iter pr_elem;
          ii1 +> List.iter pr_elem;
        )
    | CppVariadicName (s, ii) ->
        ii +> List.iter pr_elem
    | CppIdentBuilder ((s,iis), xs) ->
        let (iis, iop, icp) = Common.tuple_of_list3 iis in
        pr_elem iis;
        pr_elem iop;
        xs +> List.iter (fun ((x,iix), iicomma) ->
          iicomma +> List.iter pr_elem;
          iix +> List.iter pr_elem;
        );
        pr_elem icp

and pp_string_fragment (e,ii) =
  match (e,ii) with
    ConstantFragment(str), ii ->
      let (i) = Common.tuple_of_list1 ii in
      pr_elem i
  | FormatFragment(fmt), ii ->
      let (i) = Common.tuple_of_list1 ii in
      pr_elem i;
      pp_string_format fmt

(*and pp_string_fragment_list sfl = pp_list2 pp_string_fragment sfl*)

and pp_string_format (e,ii) =
  match (e,ii) with
    ConstantFormat(str), ii ->
      let (i) = Common.tuple_of_list1 ii in
      pr_elem i

(* ---------------------- *)

  and pp_statement_seq_list n statxs =
    statxs +> Common.print_between pr_nl (pp_statement_seq n)

  and pp_statement n = fun st ->
    let pp_expression ?(skip=false) n e = pp_do_expression ~skip false n e in
    if n = 0 then print_string "__Statement__" else
    let n = n - 1 in
    match Ast_c.get_st_and_ii st with
    | Labeled (Label (name, st)), ii ->
        let (i2) = Common.tuple_of_list1 ii in
	pr_outdent(); pp_name name; pr_elem i2; pr_nl(); pp_statement n st
    | Labeled (Case  (e, st)), [i1;i2] ->
	pr_unindent();
        pr_elem i1; pr_space(); pp_expression n e; pr_elem i2; pr_nl();
	pr_indent(); pp_statement n st
    | Labeled (CaseRange  (e, e2, st)), [i1;i2;i3] ->
	pr_unindent();
        pr_elem i1; pr_space(); pp_expression n e; pr_elem i2;
	pp_expression n e2; pr_elem i3; pr_nl(); pr_indent();
        pp_statement n st
    | Labeled (Default st), [i1;i2] ->
	pr_unindent(); pr_elem i1; pr_elem i2; pr_nl(); pr_indent();
	pp_statement n st
    | Compound statxs, [i1;i2] ->
        pr_elem i1; start_block(); pp_statement_seq_list n statxs;
        end_block(); pr_elem i2;

    | ExprStatement (None), [i] -> pr_elem i;
    | ExprStatement (None), [] -> ()
    | ExprStatement (Some e), [i] -> pp_expression ~skip:true n e; pr_elem i
        (* the last ExprStatement of a for does not have a trailing
           ';' hence the [] for ii *)
    | ExprStatement (Some e), [] -> pp_expression ~skip:true n e;
    | Selection  (If (e, st1, st2)), i1::i2::i3::is ->
        pp_ifthen n e st1 i1 i2 i3;
        pp_else n st2 is
    | Selection  (Ifdef_Ite (e, st1, st2)), i1::i2::i3::i4::is ->
        pr_elem i1;
        pp_ifthen n e st1 i2 i3 i4;
        pp_else n st2 is
    | Selection (Ifdef_Ite2 (e, st1, st2, st3)), i1::i2::i3::i4::is ->
        pr_elem i1;
        pp_ifthen n e st1 i2 i3 i4;
        (* else #else S #endif *)
	(match is with
          [i4;i5;i6;iifakend] ->
            pr_elem i4; (* else *)
            pr_elem i5; (* #else *)
            indent_if_needed st2 (function _ -> pp_statement n st2);
            pr_elem i6; (* #endif *)
            indent_if_needed st3 (function _ -> pp_statement n st3);
            pr_elem iifakend
	| _ -> raise (Impossible 90))
    | Selection  (Switch (e, st)), [i1;i2;i3;iifakend] ->
        pr_elem i1; pr_space(); pr_elem i2; pp_expression n e; pr_elem i3;
	indent_if_needed st (function _-> pp_statement n st); pr_elem iifakend
    | Iteration  (While (e, st)), [i1;i2;i3;iifakend] ->
        pr_elem i1; pr_space(); pr_elem i2; pp_expression n e; pr_elem i3;
	indent_if_needed st (function _-> pp_statement n st); pr_elem iifakend
    | Iteration  (DoWhile (st, e)), [i1;i2;i3;i4;i5;iifakend] ->
        pr_elem i1;
	indent_if_needed st (function _ -> pp_statement n st);
	pr_elem i2; pr_elem i3; pp_expression n e;
        pr_elem i4; pr_elem i5;
        pr_elem iifakend


    | Iteration  (For (first,(e2opt,il2),(e3opt, il3),st)),
        [i1;i2;i3;iifakend] ->

          pr_elem i1; pr_space();
          pr_elem i2;
	  (match first with
	    ForExp (e1opt,il1) ->
              pp_statement n (Ast_c.mk_st (ExprStatement e1opt) il1)
	  | ForDecl decl -> pp_decl n decl);
	  pr_space();
          pp_statement n (Ast_c.mk_st (ExprStatement e2opt) il2);
          assert (il3 = []);
	  pr_space();
          pp_statement n (Ast_c.mk_st (ExprStatement e3opt) il3);
          pr_elem i3;
          indent_if_needed st (function _ -> pp_statement n st);
          pr_elem iifakend

    | Iteration  (MacroIteration (s,es,st)), [i1;i2;i3;iifakend] ->
        pr_elem i1; pr_space();
        pr_elem i2;

        es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
        );

        pr_elem i3;
        indent_if_needed st (function _ -> pp_statement n st);
        pr_elem iifakend

    | Jump (Goto name), ii               ->
        let (i1, i3) = Common.tuple_of_list2 ii in
        pr_elem i1; pr_space(); pp_name name; pr_elem i3;
    | Jump ((Continue|Break|Return)), [i1;i2] -> pr_elem i1; pr_elem i2;
    | Jump (ReturnExpr e), [i1;i2] ->
	pr_elem i1; pr_space(); pp_expression n e; pr_elem i2
    | Jump (GotoComputed e), [i1;i2;i3] ->
        pr_elem i1; pr_elem i2; pp_expression n e; pr_elem i3

    | Decl decl, [] -> pp_decl n decl
    | Asm asmbody, ii ->
        (match ii with
        | [iasm;iopar;icpar;iptvirg] ->
            pr_elem iasm; pr_elem iopar;
            pp_asmbody n asmbody;
            pr_elem icpar; pr_elem iptvirg
        | [iasm;ivolatile;iopar;icpar;iptvirg] ->
            pr_elem iasm; pr_elem ivolatile; pr_elem iopar;
            pp_asmbody n asmbody;
            pr_elem icpar; pr_elem iptvirg
        | _ -> raise (Impossible 97)
        )

    | NestedFunc def, ii ->
        assert (ii = []);
        pp_def n def
    | MacroStmt, ii ->
        ii +> List.iter pr_elem ;

    | Exec(code), [exec;lang;sem] ->
	pr_elem exec; pr_space(); pr_elem lang; pr_space();
	pp_list2 (pp_exec_code n) code; pr_elem sem

    | (Labeled (Case  (_,_))
    | Labeled (CaseRange  (_,_,_)) | Labeled (Default _)
    | Compound _ | ExprStatement _
    | Selection  (If (_, _, _)) | Selection  (Switch (_, _))
    | Selection (Ifdef_Ite _) | Selection (Ifdef_Ite2 _)
    | Iteration  (While (_, _)) | Iteration  (DoWhile (_, _))
    | Iteration  (For (_, (_,_), (_, _), _))
    | Iteration  (MacroIteration (_,_,_))
    | Jump ((Continue|Break|Return)) | Jump (ReturnExpr _)
    | Jump (GotoComputed _)
    | Decl _ | Exec _ | IfdefStmt1 (_, _)
	), _ -> raise (Impossible 98)

  and pp_statement_seq n = function
    | StmtElem st -> pp_statement n st
    | IfdefStmt ifdef -> pp_ifdef ifdef
    | CppDirectiveStmt cpp -> pp_directive n cpp
    | IfdefStmt2 (ifdef, xxs) -> pp_ifdef_tree_sequence n ifdef xxs

  and pp_ifthen n e st1 i1 i2 i3 =
        (* if (<e>) *)
        pr_elem i1; pr_space(); pr_elem i2; pp_do_expression false n e; pr_elem i3;
        indent_if_needed st1 (function _ -> pp_statement n st1);
  and pp_else n st2 is =
        match (Ast_c.get_st_and_ii st2, is) with
          | ((ExprStatement None, []), [])  -> ()
          | ((ExprStatement None, []), [iifakend])  -> pr_elem iifakend
          | _st2, [i4;iifakend] ->
              pr_elem i4;
              indent_if_needed st2 (function _ -> pp_statement n st2);
              pr_elem iifakend
          | _st2, [i4;i5;iifakend] -> (* else #endif S *)
              pr_elem i4;
              pr_elem i5;
              indent_if_needed st2 (function _ -> pp_statement n st2);
              pr_elem iifakend
          | x -> raise (Impossible 96)


(* ifdef XXX elsif YYY elsif ZZZ endif *)
  and pp_ifdef_tree_sequence n ifdef xxs =
    match ifdef with
    | if1::ifxs ->
        pp_ifdef if1;
        pp_ifdef_tree_sequence_aux n ifxs xxs
    | _ -> raise (Impossible 99)

(* XXX elsif YYY elsif ZZZ endif *)
  and pp_ifdef_tree_sequence_aux n ifdefs xxs =
    Common.zip ifdefs xxs +> List.iter (fun (ifdef, xs) ->
      xs +> List.iter (pp_statement_seq n);
      pp_ifdef ifdef
    )

(* ---------------------- *)
  and pp_asmbody n (string_list, colon_list) =
    string_list +> List.iter pr_elem ;
    colon_list +> List.iter (fun (Colon xs, ii) ->
      ii +> List.iter pr_elem;
      xs +> List.iter (fun (x,iicomma) ->
	assert ((List.length iicomma) <= 1);
	iicomma +> List.iter (function x -> pr_elem x; pr_space());
	(match x with
	| ColonMisc, ii -> ii +> List.iter pr_elem;
	| ColonExpr e, [istring;iopar;icpar] ->
            pr_elem istring;
            pr_elem iopar;
            pp_do_expression false n e;
            pr_elem icpar
	  (* the following case used to be just raise Impossible, but
	     the code __asm__ __volatile__ ("dcbz 0, %[input]"
                                ::[input]"r"(&coherence_data[i]));
	     in linux-2.6.34/drivers/video/fsl-diu-fb.c matches this case *)
	| (ColonExpr e), ii ->
	    (match List.rev ii with
	      icpar::iopar::istring::rest ->
		List.iter pr_elem (List.rev rest);
		pr_elem istring;
		pr_elem iopar;
		pp_do_expression false n e;
		pr_elem icpar
	    | _ -> raise (Impossible 100)))
        ))

  and pp_exec_code n = function
    ExecEval name, [colon] -> pr_elem colon; pp_do_expression false n name
  | ExecToken, [tok] -> pr_elem tok
  | _ -> raise (Impossible 101)


and pp_funcall c n ((exp, typ), ii) : unit =
    match exp, ii with
    | FunCall (e, es), [i1;i2] ->
        let skip = ref false in
        print_string (Printf.sprintf "__FunCall_%d__" (List.length es));
        begin match !typ with
        | Some(t, _), _  -> pp_type (n+1) t
        | _ -> skip := true; print_string "__Unknown_type__"
        end;
        pp_do_expression ~skip:!skip false n e; pr_elem i1;
        if c then pp_arg_list n es;
        pr_elem i2
    | _ -> assert false



(* ---------------------- *)

(*
   pp_type_with_ident
   pp_base_type
   pp_type_with_ident_rest
   pp_type_left
   pp_type_right
   pp_type

   pp_decl
*)
  and (pp_type_with_ident:
	 int -> (unit -> unit) option -> (storage * il) option ->
	   fullType -> attribute list -> attribute list ->
	     unit) =
    fun n ident sto ft attrs endattrs ->
      pp_base_type n ft  sto;
      (match (ident, Ast_c.unwrap_typeC ft) with
	(Some _,_) | (_,Pointer _) -> pr_space()
      |	_ -> ());
      pp_type_with_ident_rest n ident ft attrs endattrs


  and (pp_base_type: int -> fullType -> (storage * il) option -> unit) =
    fun n (qu, (ty, iity)) sto ->
      let pp_expression n e = pp_do_expression false n e in
      let get_sto sto =
        match sto with
        | None -> [] | Some (s, iis) -> (*assert (List.length iis = 1);*) iis
      in
      let print_sto_qu (sto, (qu, iiqu)) =
        let all_ii = get_sto sto @ iiqu in
        all_ii
          +> List.sort Ast_c.compare_pos
          +> Common.print_between pr_space pr_elem

      in
      let print_sto_qu_ty (sto, (qu, iiqu), iity) =
        let all_ii = get_sto sto @ iiqu @ iity in
        let all_ii2 = all_ii +> List.sort Ast_c.compare_pos in

        if all_ii <> all_ii2
        then begin
            (* TODO in fact for pointer, the qualifier is after the type
             * cf -test strangeorder
             *)
          pr2 "STRANGEORDER";
          all_ii2 +> Common.print_between pr_space pr_elem
        end
        else all_ii2 +> Common.print_between pr_space pr_elem
      in

      match ty, iity with
      |	(NoType,_) -> ()
      | (Pointer t, [i])                           -> pp_base_type n t sto
      | (ParenType t, _)                           -> pp_base_type n t sto
      | (Array (eopt, t), [i1;i2])                 -> pp_base_type n t sto
      | (FunctionType (returnt, paramst), [i1;i2]) ->
          pp_base_type n returnt sto;


      | (StructUnion (su, sopt, fields),iis) ->
          print_sto_qu (sto, qu);

          (match sopt,iis with
          | Some s , [i1;i2;i3;i4] ->
              pr_elem i1; pr_space(); pr_elem i2; pr_space(); pr_elem i3;
          | None, [i1;i2;i3] ->
              pr_elem i1; pr_space(); pr_elem i2;
          | x -> raise (Impossible 101)
	  );

          fields +> List.iter (fun x -> pr_nl(); pr_indent(); pp_field n x);
	  pr_nl();

          (match sopt,iis with
          | Some s , [i1;i2;i3;i4] -> pr_elem i4
          | None, [i1;i2;i3] ->       pr_elem i3;
          | x -> raise (Impossible 102)
	  );



      | (Enum  (sopt, enumt), iis) ->
          print_sto_qu (sto, qu);

          (match sopt, iis with
          | (Some s, ([i1;i2;i3;i4]|[i1;i2;i3;i4;_])) ->
              pr_elem i1; pr_elem i2; pr_elem i3;
          | (None, ([i1;i2;i3]|[i1;i2;i3;_])) ->
              pr_elem i1; pr_elem i2
          | x -> raise (Impossible 103)
	  );

          enumt +> List.iter (fun ((name, eopt), iicomma) ->
            assert (List.length iicomma <= 1);
            iicomma +> List.iter (function x -> pr_elem x; pr_space());
            pp_name name;
            eopt +> Common.do_option (fun (ieq, e) ->
              pr_elem ieq;
              pp_expression n e;
	  ));

          (match sopt, iis with
          | (Some s, [i1;i2;i3;i4]) ->    pr_elem i4
          | (Some s, [i1;i2;i3;i4;i5]) ->
              pr_elem i5; pr_elem i4 (* trailing comma *)
          | (None, [i1;i2;i3]) ->         pr_elem i3
          | (None, [i1;i2;i3;i4]) ->
              pr_elem i4; pr_elem i3 (* trailing comma *)


          | x -> raise (Impossible 104)
	  );


      | (BaseType _, iis) ->
          print_sto_qu_ty (sto, qu, iis);

      | (StructUnionName (s, structunion), iis) ->
          assert (List.length iis = 2);
          print_sto_qu_ty (sto, qu, iis);

      | (EnumName  s, iis) ->
          assert (List.length iis = 2);
          print_sto_qu_ty (sto, qu, iis);

      | (Decimal(l,p), [dec;lp;cm;rp]) ->
	  (* hope that sto before qu is right... cf print_sto_qu_ty *)
	  let stoqulp = get_sto sto @ (snd qu) @ [dec] in
	  Common.print_between pr_space pr_elem stoqulp;
	  pr_elem lp; pp_expression n l; pr_elem cm;
	  do_option (pp_expression n) p; pr_elem rp

      | (TypeName (name,typ), noii) ->
          assert (noii = []);
          let (_s, iis) = get_s_and_info_of_name name in
          print_sto_qu_ty (sto, qu, [iis]);

          if !Flag_parsing_c.pretty_print_typedef_value
          then begin
            pr_elem (Ast_c.fakeInfo() +> Ast_c.rewrap_str "{*");
            typ +> Common.do_option (fun typ ->
                pp_type n typ;
            );
            pr_elem (Ast_c.fakeInfo() +> Ast_c.rewrap_str "*}");
          end;

      | (FieldType (t, _, _), iis) ->
	  pp_base_type n t sto

      | (TypeOfExpr (e), iis) ->
          print_sto_qu (sto, qu);
          (match iis with
          | [itypeof;iopar;icpar] ->
              pr_elem itypeof; pr_elem iopar;
              pp_expression n e;
              pr_elem icpar;
          | _ -> raise (Impossible 105)
          )

      | (TypeOfType (t), iis) ->
          print_sto_qu (sto, qu);
          (match iis with
          | [itypeof;iopar;icpar] ->
              pr_elem itypeof; pr_elem iopar;
              pp_type n t;
              pr_elem icpar;
          | _ -> raise (Impossible 106)
	  )

      | (Pointer _ | (*ParenType _ |*) Array _ | FunctionType _ | Decimal _
            (* | StructUnion _ | Enum _ | BaseType _ *)
            (* | StructUnionName _ | EnumName _ | TypeName _  *)
            (* | TypeOfExpr _ | TypeOfType _ *)
         ), _ -> raise (Impossible 107)

  (*and pp_field_list fields n =
     fields +>  Common.print_between pr_nl (pp_field n)*)
  and pp_field n = function
      DeclarationField
	(FieldDeclList(onefield_multivars,[iiptvirg;ifakestart])) ->
	pr_elem ifakestart;
        (match onefield_multivars with
          x::xs ->
	    (* handling the first var. Special case, with the
               first var, we print the whole type *)

	    (match x with
	      (Simple (nameopt, typ)), iivirg ->
              (* first var cannot have a preceding ',' *)
		assert (List.length iivirg = 0);
		let identinfo =
                  match nameopt with
		  | None -> None
                  | Some name -> Some (function _ -> pp_name name)
                in
		pp_type_with_ident n identinfo None typ Ast_c.noattr
		  Ast_c.noattr;

	    | (BitField (nameopt, typ, iidot, expr)), iivirg ->
                      (* first var cannot have a preceding ',' *)
		assert (List.length iivirg = 0);
		(match nameopt with
		| None ->
		    pp_type n typ;
		| Some name ->
		    pp_type_with_ident n (Some (function _ -> pp_name name))
		      None typ Ast_c.noattr Ast_c.noattr;
		    );
                pr_elem iidot;
		pp_do_expression false n expr

                  ); (* match x, first onefield_multivars *)

                      (* for other vars *)
	    xs +> List.iter (function
	      | (Simple (nameopt, typ)), iivirg ->
		  iivirg +> List.iter pr_elem;
		  let identinfo =
		    match nameopt with
		    | None -> None
		    | Some name -> Some (function _ -> pp_name name)
		  in
		  pp_type_with_ident_rest n identinfo typ
		    Ast_c.noattr Ast_c.noattr

	      | (BitField (nameopt, typ, iidot, expr)), iivirg ->
		  iivirg +> List.iter pr_elem;
		  (match nameopt with
		  | Some name ->
		      pp_type_with_ident_rest n
			(Some (function _ -> pp_name name))
			typ Ast_c.noattr Ast_c.noattr;
		      pr_elem iidot;
		      pp_do_expression false n expr
		  | None ->
		      (* was raise Impossible, but have no idea why because
			 nameless bit fields are accepted by the parser and
			 nothing seems to be done to give them names *)
		      pr_elem iidot;
		      pp_do_expression false n expr
			)); (* iter other vars *)

	| [] -> raise (Impossible 108)
	      ); (* onefield_multivars *)
	pr_elem iiptvirg

    | DeclarationField(FieldDeclList(onefield_multivars,_)) ->
	failwith "wrong number of tokens"

    | MacroDeclField ((s, es), ii)  ->
        let (iis, lp, rp, iiend, ifakestart) =
          Common.tuple_of_list5 ii in
                 (* iis::lp::rp::iiend::ifakestart::iisto
	            iisto +> List.iter pr_elem; (* static and const *)
                 *)
	pr_elem ifakestart;
	pr_elem iis;
	pr_elem lp;
	es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
	  );

	pr_elem rp;
	pr_elem iiend;



    | EmptyField iipttvirg_when_emptyfield ->
        pr_elem iipttvirg_when_emptyfield

    | CppDirectiveStruct cpp -> pp_directive n cpp
    | IfdefStruct ifdef -> pp_ifdef ifdef

(* used because of DeclList, in    int i,*j[23];  we don't print anymore the
   int before *j *)
  and (pp_type_with_ident_rest: int -> (unit -> unit) option ->
    fullType -> attribute list -> attribute list -> unit) =

    fun n ident (((qu, iiqu), (ty, iity)) as fullt) attrs endattrs ->

      let print_ident ident = Common.do_option (fun f ->
        (* XXX attrs +> pp_attributes pr_elem pr_space; *)
        f();
	(if not(endattrs = []) then pr_space());
        endattrs +> pp_attributes pr_elem pr_space
	) ident
      in

      match ty, iity with
      (* the work is to do in base_type !! *)
      | (NoType, iis)                           -> ()
      | (BaseType _, iis)                       -> print_ident ident
      | (Enum  (sopt, enumt), iis)              -> print_ident ident
      | (StructUnion (_, sopt, fields),iis)     -> print_ident ident
      | (StructUnionName (s, structunion), iis) -> print_ident ident
      | (EnumName  s, iis)                      -> print_ident ident
      | (Decimal _, iis)                        -> print_ident ident
      | (TypeName (_name,_typ), iis)            -> print_ident ident
      | (FieldType (_typ,_,_), iis)             -> print_ident ident
      | (TypeOfExpr (e), iis)                   -> print_ident ident
      | (TypeOfType (e), iis)                   -> print_ident ident



      | (Pointer t, [i]) ->
          (* subtil:  void ( *done)(int i)   is a Pointer
             (FunctionType (return=void, params=int i) *)
          (*WRONG I THINK, use left & right function *)
          (* bug: pp_type_with_ident_rest None t;      print_ident ident *)
          pr_elem i;
          iiqu +> List.iter pr_elem; (* le const est forcement apres le '*' *)
          pp_type_with_ident_rest n ident t attrs Ast_c.noattr;

      (* ugly special case ... todo? maybe sufficient in practice *)
      | (ParenType ttop, [i1;i2]) ->
          (match Ast_c.get_ty_and_ii ttop with
          | (_q1, (Pointer t2, [ipointer])) ->
              (match Ast_c.get_ty_and_ii t2 with
              | (q2, (FunctionType t, ii3)) ->

		  pp_type_left (q2, mk_tybis (FunctionType t) ii3);
		  pr_elem i1;
		  pr_elem ipointer;
		  print_ident ident;
		  pr_elem i2;
		  pp_type_right n (q2, mk_tybis (FunctionType t) ii3);
              | _ ->
                  pr2 "PB PARENTYPE ZARB, I forget about the ()";
                  pp_type_with_ident_rest n ident ttop attrs Ast_c.noattr;
              )
          (* another ugly special case *)
          | _q1, (Array (eopt,t2 ), [iarray1;iarray2]) ->
              (match Ast_c.get_ty_and_ii t2 with
              | (_q2, (Pointer t3, [ipointer])) ->
                  (match Ast_c.get_ty_and_ii t3 with
                  | (q3, (FunctionType t, iifunc)) ->

		      pp_type_left (q3, mk_tybis (FunctionType t) iifunc);
		      pr_elem i1;
		      pr_elem ipointer;
		      print_ident ident;
		      pr_elem iarray1;
		      do_option (pp_do_expression false n) eopt;
		      pr_elem iarray2;
		      pr_elem i2;
		      pp_type_right n (q3, mk_tybis (FunctionType t) iifunc)
                  | _ ->
                      pr2 "PB PARENTYPE ZARB, I forget about the ()";
                      pp_type_with_ident_rest n ident ttop attrs Ast_c.noattr;
                  )
              | _ ->
                  pr2 "PB PARENTYPE ZARB, I forget about the ()";
                  pp_type_with_ident_rest n ident ttop attrs Ast_c.noattr;
              )
          | _t ->

              pr2 "PB PARENTYPE ZARB, I forget about the ()";
              pp_type_with_ident_rest n ident ttop attrs Ast_c.noattr;
          )


      | (Array (eopt, t), [i1;i2]) ->
          pp_type_left fullt;

          iiqu +> List.iter pr_elem;
          print_ident ident;

          pp_type_right n fullt;


      | (FunctionType (returnt, paramst), [i1;i2]) ->
          pp_type_left fullt;

          iiqu +> List.iter pr_elem;
          print_ident ident;

          pp_type_right n fullt;


      | (FunctionType _ | Array _ | ParenType _ | Pointer _), _ ->
	  raise (Impossible 109)


  and (pp_type_left: fullType -> unit) =
    fun ((qu, iiqu), (ty, iity)) ->
      match ty, iity with
	(NoType,_) -> failwith "pp_type_left: unexpected NoType"
      | (Pointer t, [i]) ->
          pr_elem i;
          iiqu +> List.iter pr_elem; (* le const est forcement apres le '*' *)
          pp_type_left t

      | (Array (eopt, t), [i1;i2]) -> pp_type_left t
      | (FunctionType (returnt, paramst), [i1;i2]) -> pp_type_left returnt

      | (ParenType t, _) ->  failwith "parenType"


      | (BaseType _, iis)    -> ()
      | (Enum  (sopt, enumt), iis) -> ()
      | (StructUnion (_, sopt, fields),iis)  -> ()
      | (StructUnionName (s, structunion), iis) -> ()
      | (EnumName  s, iis) -> ()
      | (Decimal(l,p), iis) -> ()
      | (TypeName (_name,_typ), iis) -> ()
      | FieldType (_, _, _), _ -> ()
      | TypeOfType _, _ -> ()
      | TypeOfExpr _, _ -> ()

      | (FunctionType _ | Array _ | Pointer _), _ -> raise (Impossible 110)


  and pp_param n param =
    if n = 0 then print_string "__Param__" else
    let n = n - 1 in
    let {p_namei = nameopt;
         p_register = (b,iib);
         p_type=t;} = param in

    iib +> List.iter pr_elem;

    match nameopt with
    | None ->
        pp_type n t
    | Some name ->
	pp_type_with_ident n (Some (function _ -> pp_name name))
	  None t Ast_c.noattr Ast_c.noattr




  and pp_type_right n (((qu, iiqu), (ty, iity)) : fullType) =
    match ty, iity with
      (NoType,_) -> failwith "pp_type_right: unexpected NoType"
    | (Pointer t, [i]) ->  pp_type_right n t

    | (Array (eopt, t), [i1;i2]) ->
        pr_elem i1;
        eopt +> do_option (pp_do_expression false n);
        pr_elem i2;
        pp_type_right n t

    | (ParenType t, _) ->  failwith "parenType"
    | (FunctionType (returnt, paramst), [i1;i2]) ->
        pr_elem i1;
        (match paramst with
        | (ts, (b, iib)) ->
            ts +> List.iter (fun (param,iicomma) ->
              assert ((List.length iicomma) <= 1);
              iicomma +> List.iter (function x -> pr_elem x; pr_space());

              pp_param n param;
	    );
            iib +> List.iter pr_elem;
        );
        pr_elem i2

    | (BaseType _, iis)        -> ()
    | (Enum  (sopt, enumt), iis) -> ()
    | (StructUnion (_, sopt, fields),iis)-> ()
    | (StructUnionName (s, structunion), iis) -> ()
    | (EnumName  s, iis) -> ()
    | (Decimal(l,p), iis) -> ()
    | (TypeName (name,_typ), iis) -> ()
    | (FieldType (_, _, _), _) -> ()

    | TypeOfType _, _ -> ()
    | TypeOfExpr _, _ -> ()

    | (FunctionType _ | Array _ | Pointer _), _ -> raise (Impossible 111)

  and pp_type n t =
    if n = 0 then print_string "__Type__" else
    let n = n - 1 in
    pp_type_with_ident n None None t Ast_c.noattr Ast_c.noattr
  (*and pp_type_ident n t i =
    pp_type_with_ident n (Some i) None t Ast_c.noattr Ast_c.noattr*)
  (*and pp_type_ident_rest n t i =
    pp_type_with_ident_rest n (Some i) t Ast_c.noattr Ast_c.noattr*)
  (*and pp_base_type2 n t =
    pp_base_type n t None*)

(* ---------------------- *)
  and pp_decl n = function
    | DeclList ((({v_namei = var;
                   v_type = returnType;
                   v_storage = storage;
                   v_attr = attrs;
                   v_endattr = endattrs;
                  },[])::xs),
	       iivirg::ifakestart::iisto) ->

	pr_elem ifakestart;

        (* old: iisto +> List.iter pr_elem; *)


        (* handling the first var. Special case, we print the whole type *)
	(match var with
	| Some (name, iniopt) ->
	    pp_type_with_ident n
	      (Some (function _ -> pp_name name)) (Some (storage, iisto))
	      returnType attrs endattrs;
	    (match iniopt with
	      Ast_c.NoInit -> ()
	    | Ast_c.ValInit(iini,init) ->
		pr_space(); pr_elem iini; pr_space(); pp_init n init
	    | Ast_c.ConstrInit((init,[lp;rp])) ->
		pr_elem lp; pp_arg_list n init; pr_elem rp
	    | Ast_c.ConstrInit _ -> raise (Impossible 112))
	| None -> pp_type n returnType
	);

      (* for other vars, we just call pp_type_with_ident_rest. *)
	xs +> List.iter (function
	| ({v_namei = Some (name, iniopt);
	    v_type = returnType;
	    v_storage = storage2;
	    v_attr = attrs;
	    v_endattr = endattrs;
	  }, iivirg) ->

	    assert (storage2 = storage);
	    iivirg +> List.iter pr_elem;
	    pp_type_with_ident_rest n (Some (function _ -> pp_name name))
	      returnType attrs endattrs;
	    (match iniopt with
	      Ast_c.NoInit -> ()
	    | Ast_c.ValInit(iini,init) -> pr_elem iini; pp_init n init
	    | Ast_c.ConstrInit((init,[lp;rp])) ->
		pr_elem lp; pp_arg_list n init; pr_elem rp
	    | Ast_c.ConstrInit _ -> raise (Impossible 113));


	| x -> raise (Impossible 114)
	);

	pr_elem iivirg;

    | MacroDecl ((sto, s, es, true), iis::lp::rp::iiend::ifakestart::iisto) ->
	pr_elem ifakestart;
	iisto +> List.iter pr_elem; (* static and const *)
	pr_elem iis;
	pr_elem lp;
	es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
	);

	pr_elem rp;
	pr_elem iiend;

    | MacroDecl ((sto, s, es, false), iis::lp::rp::ifakestart::iisto) ->
	pr_elem ifakestart;
	iisto +> List.iter pr_elem; (* static and const *)
	pr_elem iis;
	pr_elem lp;
	es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
	);

	pr_elem rp;

    | MacroDeclInit
	((sto, s, es, ini), iis::lp::rp::eq::iiend::ifakestart::iisto) ->
	pr_elem ifakestart;
	iisto +> List.iter pr_elem; (* static and const *)
	pr_elem iis;
	pr_elem lp;
	es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
	);

	pr_elem rp;
	pr_elem eq;
	pp_init n ini;
	pr_elem iiend;

    | (DeclList (_, _) | (MacroDecl _) | (MacroDeclInit _)) ->
	raise (Impossible 115)

(* ---------------------- *)
and pp_init n (init, iinit) =
  match init, iinit with
      | InitExpr e, [] -> pp_do_expression false n e;
      | InitList xs, i1::i2::iicommaopt ->
          pr_elem i1; start_block();
          xs +> List.iter (fun (x, ii) ->
            assert (List.length ii <= 1);
            ii +> List.iter (function e -> pr_elem e; pr_nl());
            pp_init n x
          );
          iicommaopt +> List.iter pr_elem;
	  end_block();
          pr_elem i2;

      | InitDesignators (xs, initialiser), [i1] -> (* : *)
          xs +> List.iter (pp_designator n);
          pr_elem i1;
          pp_init n initialiser

    (* no use of '=' in the "Old" style *)
      | InitFieldOld (string, initialiser), [i1;i2] -> (* label:   in oldgcc *)
          pr_elem i1; pr_elem i2; pp_init n initialiser
      | InitIndexOld (expression, initialiser), [i1;i2] -> (* [1] in oldgcc *)
          pr_elem i1; pp_do_expression false n expression; pr_elem i2;
          pp_init n initialiser

      | (InitIndexOld _ | InitFieldOld _ | InitDesignators _
      | InitList _ | InitExpr _
	  ), _ -> raise (Impossible 116)

  (*and pp_init_list n ini = pp_list (pp_init n) ini*)

  and pp_designator n = function
    | DesignatorField (s), [i1; i2] ->
	pr_elem i1; pr_elem i2;
    | DesignatorIndex (expression), [i1;i2] ->
	pr_elem i1; pp_do_expression false n expression; pr_elem i2;

    | DesignatorRange (e1, e2), [iocro;iellipsis;iccro] ->
	pr_elem iocro; pp_do_expression false n e1; pr_elem iellipsis;
	pp_do_expression false n e2; pr_elem iccro;

    | (DesignatorField _ | DesignatorIndex _ | DesignatorRange _
	), _ -> raise (Impossible 117)


(* ---------------------- *)
  and pp_attributes pr_elem pr_space attrs =
    attrs +> List.iter (fun (attr, ii) ->
      ii +> List.iter pr_elem;
    );

(* ---------------------- *)
  and pp_def_start n defbis iifunc1 iifunc2 ifakestart isto =
    let {f_name = name;
          f_type = (returnt, (paramst, (b, iib)));
          f_storage = sto;
          f_body = statxs;
          f_attr = attrs;
	} = defbis in
    pr_elem ifakestart;

    pp_type_with_ident n None (Some (sto, isto))
      returnt Ast_c.noattr Ast_c.noattr;

    pp_attributes pr_elem pr_space attrs;
    pr_space();
    pp_name name;

    pr_elem iifunc1;

        (* not anymore, cf tests/optional_name_parameter and
           macro_parameter_shortcut.c
           (match paramst with
           | [(((bool, None, t), ii_b_s), iicomma)] ->
               assert
		 (match t with
		 | qu, (BaseType Void, ii) -> true
		 | _ -> true
	       );
               assert (iicomma = []);
               assert (ii_b_s = []);
               pp_type_with_ident None None t

           | paramst ->
               paramst +> List.iter (fun (((bool, s, t), ii_b_s), iicomma) ->
	       iicomma +> List.iter pr_elem;

	       (match b, s, ii_b_s with
               | false, Some s, [i1] ->
		   pp_type_with_ident (Some (function _ -> pr_elem i1)) None t;
               | true, Some s, [i1;i2] ->
		   pr_elem i1;
		   pp_type_with_ident (Some (function _ -> pr_elem i2)) None t;

            (* in definition we have name for params, except when f(void) *)
               | _, None, _ -> raise Impossible
               | false, None, [] ->

               | _ -> raise Impossible
           )));

         (* normally ii represent the ",..." but it is also abused
            with the f(void) case *)
         (* assert (List.length iib <= 2);*)
           iib +> List.iter pr_elem;

        *)
    pp_param_list n paramst;
    iib +> List.iter pr_elem;


    pr_elem iifunc2

  and pp_def n def =
    let defbis, ii = def in
    match ii with
    | iifunc1::iifunc2::i1::i2::ifakestart::isto ->
	pp_def_start n defbis iifunc1 iifunc2 ifakestart isto;
	pr_space();
        pr_elem i1;
	pp_statement_seq_list n defbis.f_body;
        pr_elem i2
    | _ -> raise (Impossible 118)

  and pp_fun_header n def =
    let defbis, ii = def in
    match ii with
    | iifunc1::iifunc2::ifakestart::isto ->
	pp_def_start n defbis iifunc1 iifunc2 ifakestart isto
    | _ -> raise (Impossible 1180)

  and pp_param_list n paramst = pp_list (pp_param n) paramst

(* ---------------------- *)

  and pp_ifdef ifdef =
    match ifdef with
    | IfdefDirective (ifdef, ii) ->
	List.iter pr_elem ii


  and pp_directive n = function
    | Include {i_include = (s, ii);} ->
	let (i1,i2) = Common.tuple_of_list2 ii in
	pr_elem i1; pr_space(); pr_elem i2
    | Define ((s,ii), (defkind, defval)) ->
	let (idefine,iident,ieol) = Common.tuple_of_list3 ii in
	pr_elem idefine; pr_space();
	pr_elem iident; pr_space();

	let define_val = function
          | DefineExpr e -> pp_do_expression false n e
          | DefineStmt st -> pp_statement n st
          | DefineDoWhileZero ((st,e), ii) ->
              (match ii with
              | [ido;iwhile;iopar;icpar] ->
                  pr_elem ido;
                  pp_statement n st;
                  pr_elem iwhile; pr_elem iopar;
                  pp_do_expression false n e;
                  pr_elem icpar
              | _ -> raise (Impossible 119)
	      )
          | DefineFunction def -> pp_def n def

          | DefineType ty -> pp_type n ty
          | DefineText (s, ii) -> List.iter pr_elem ii
          | DefineEmpty -> ()
          | DefineInit ini -> pp_init n ini
	  | DefineMulti ss ->
	      ss +> List.iter (pp_statement n)
          | DefineTodo -> pr2 "DefineTodo"
	in
	(match defkind with
	| DefineVar | Undef -> ()
	| DefineFunc (params, ii) ->
            let (i1,i2) = tuple_of_list2 ii in
            pr_elem i1;
	    pp_define_param_list params;
            pr_elem i2;
	);
	define_val defval;
	pr_elem ieol

    | Pragma ((s,ii), pragmainfo) ->
    pp_name s

    | OtherDirective (ii) ->
	List.iter pr_elem ii

  and pp_define_param_list dparams =
    pp_list (fun (s,iis) -> iis +> List.iter pr_elem) dparams in

  let rec pp_toplevel n = function
    | Declaration decl -> pp_decl n decl
    | Definition def -> pp_def n def

    | CppTop directive -> pp_directive n directive


    | MacroTop (s, es,   [i1;i2;i3;i4]) ->
	pr_elem i1;
	pr_elem i2;
	es +> List.iter (fun (e, opt) ->
          assert (List.length opt <= 1);
          opt +> List.iter pr_elem;
          pp_argument n e;
	);
	pr_elem i3;
	pr_elem i4;


    | EmptyDef ii -> ii +> List.iter pr_elem
    | NotParsedCorrectly ii ->
	assert (List.length ii >= 1);
	ii +> List.iter pr_elem
    | FinalDef info -> pr_elem (Ast_c.rewrap_str "" info)

    | IfdefTop ifdefdir -> pp_ifdef ifdefdir

    | Namespace (tls, [i1; i2; i3; i4]) ->
	pr_elem i1; pr_elem i2; pr_elem i3;
	List.iter (pp_toplevel n) tls;
	pr_elem i4;
    | (MacroTop _) | (Namespace _) -> raise (Impossible 120) in

  let pp_flow n nd =
    let pp_expression n e = pp_do_expression false n e in
    match F.unwrap nd  with
    | F.FunHeader (({f_name =idb;
                      f_type = (rett, (paramst,(isvaargs,iidotsb)));
                      f_storage = stob;
                      f_body = body;
                      f_attr = attrs},ii) as def) ->
			assert (body = []);
			pp_fun_header (n+1) def


    | F.Decl decl -> pp_decl (n+1) decl

    | F.ExprStatement (st, (eopt, ii)) ->
        decr_stack ();
	      pp_statement (n+1) (Ast_c.mk_st (ExprStatement eopt) ii)

    | F.IfHeader (_, (e,ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space(); pr_elem i2; pp_expression n e; pr_elem i3
    | F.SwitchHeader (_, (e,ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space(); pr_elem i2; pp_expression n e; pr_elem i3
    | F.WhileHeader (_, (e,ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space(); pr_elem i2; pp_expression n e; pr_elem i3
    | F.DoWhileTail (e,ii) ->
	let (i1,i2,i3,i4) = tuple_of_list4 ii in
	pr_elem i1; pr_elem i2; pp_expression n e;
	pr_elem i3; pr_elem i4


    | F.ForHeader (_st, ((first, (e2opt,il2), (e3opt,il3)), ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space();
	pr_elem i2;
	(match first with
	  ForExp (e1opt,il1) ->
	    pp_statement n (Ast_c.mk_st (ExprStatement e1opt) il1)
	| ForDecl decl -> pp_decl n decl);
	pr_space();
	pp_statement n (Ast_c.mk_st (ExprStatement e2opt) il2);
	assert (il3 = []);
	pr_space();
	pp_statement n (Ast_c.mk_st (ExprStatement e3opt) il3);
	pr_elem i3

    | F.MacroIterHeader (_s, ((s,es), ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space();
	pr_elem i2;

	es +> List.iter (fun (e, opt) ->
	  assert (List.length opt <= 1);
	  opt +> List.iter pr_elem;
	  pp_argument n e;
	  );

	pr_elem i3

    | F.ReturnExpr (_st, (e,ii)) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_space(); pp_expression n e; pr_elem i2

    | F.Case (_st, (e,ii)) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_space(); pp_expression n e; pr_elem i2

    | F.CaseRange (_st, ((e1, e2),ii)) ->
	let (i1,i2,i3) = tuple_of_list3 ii in
	pr_elem i1; pr_space(); pp_expression n e1; pr_elem i2;
	pp_expression n e2; pr_elem i3

    | F.CaseNode i -> ()

    | F.DefineExpr e  -> pp_expression (n+1) e

    | F.DefineType ft  -> pp_type (n+1) ft

    | F.DefineHeader ((s,ii), (defkind))  ->
        let (idefine,iident,ieol) = Common.tuple_of_list3 ii in
	pr_elem idefine; pr_space();
	pr_elem iident; pr_space();
	(match defkind with
	| DefineVar | Undef -> ()
	| DefineFunc (params, ii) ->
	    let (i1,i2) = tuple_of_list2 ii in
	    pr_elem i1;
	    pp_define_param_list params;
	    pr_elem i2)

    | F.DefineDoWhileZeroHeader (((),ii)) ->
	(* not sure what it is, ignore *)
        (* iif ii *)
	pr2 "DefineDoWhileZeroHeader"

    | F.PragmaHeader((s,ii), pragmainfo) ->
        pp_name s

    | F.Include ({i_include = (s, ii);} as a) ->
	pp_directive (n+1) (Include a)

    | F.MacroTop (s, args, ii) ->
	pp_toplevel (n+1) (MacroTop (s, args, ii))

    | F.Break    (st,((),ii),fromswitch) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_elem i2
    | F.Continue (st,((),ii)) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_elem i2
    | F.Default  (st,((),ii)) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_elem i2
    | F.Return   (st,((),ii)) ->
	let (i1,i2) = tuple_of_list2 ii in
	pr_elem i1; pr_elem i2
    | F.Goto  (st, name, ((),ii)) ->
	let (i1, i3) = Common.tuple_of_list2 ii in
	pr_elem i1; pr_space();
	(if n = 1
	then print_string "__label__"
	else pp_name name);
	pr_elem i3
    | F.Label (st, name, ((),ii)) ->
	let (i2) = Common.tuple_of_list1 ii in
	(if n = 1
	then print_string "__label__"
	else pp_name name); pr_elem i2
    | F.EndStatement iopt ->
        (* do_option infof iopt *)
	pr2 "EndStatement"
    | F.DoHeader (st, info) ->
	pr_elem info
    | F.Else info ->
	pr_elem info
    | F.SeqEnd (i, info) ->
	pr_elem info
    | F.SeqStart (st, i, info) ->
	pr_elem info

    | F.MacroStmt (st, ((),ii)) ->
	pp_statement (n+1) (MacroStmt,ii)
    | F.Asm (st, (asmbody,ii)) ->
	pp_statement (n+1) (Asm asmbody, ii)

    | F.Exec(st,(code,ii)) ->
	pp_statement (n+1) (Exec code, ii)

    | F.IfdefHeader (info) ->
	pp_ifdef info
    | F.IfdefElse (info) ->
	pp_ifdef info
    | F.IfdefEndif (info) ->
	pp_ifdef info

    | F.IfdefIteHeader _ii ->
        pr2 "IfdefIteHeader"

    | F.DefineTodo ->
	pr2 "DefineTodo"


    | F.TopNode -> pr2 "TopNode"
    | F.EndNode -> pr2 "EndNode"
    | F.ErrorExit -> pr2 "ErrorExit"
    | F.Exit -> pr2 "Exit"
    | F.Enter -> pr2 "Enter"
    | F.LoopFallThroughNode -> pr2 "LoopFallThroughNode"
    | F.FallThroughNode -> pr2 "FallThroughNode"
    | F.AfterNode _ -> pr2 "AfterNode"
    | F.FalseNode -> pr2 "FalseNode"
    | F.TrueNode _ -> pr2 "TrueNode"
    | F.InLoopNode -> pr2 "InLoopNode"
    | F.Fake -> pr2 "Fake" in

  { expression = (pp_do_expression false);
    statement  = pp_statement;
    ty         = pp_type;
    decl       = pp_decl;
    param      = pp_param;
    flow       = pp_flow;
    funcall    = (pp_funcall true);
  }

let mk_printer run =
  let res = ref [] in
  let pr_nl _ = () in
  let pr_indent _ = () in
  let pr_outdent _ = () in
  let pr_unindent _ = () in
  run (mk_pretty_printers
	 ~pr_nl ~pr_indent ~pr_outdent ~pr_unindent);
  String.concat " " (List.rev !res)

let pp_expression n e =
  mk_printer (function printer -> printer.expression n e)
let pp_statement n e =
  mk_printer (function printer -> printer.statement n e)
let pp_type n e =
  mk_printer (function printer -> printer.ty n e)
let pp_decl n e =
  mk_printer (function printer -> printer.decl n e)
let pp_param n e =
  mk_printer (function printer -> printer.param n e)
let pp_flow n e =
  mk_printer (function printer -> printer.flow n e)
let pp_funcall n e =
  mk_printer (function printer -> printer.funcall n e)

let subtrees_for_node node : (string * int) list =
    let n = [1; 2] in
    let tbl = Hashtbl.create 101 in
    let add_to_table mult f value : unit = List.iter (fun depth ->
        let rep = f depth value in
        hashinc tbl rep (mult * depth)
    ) n
    in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.kexpr = (fun (k, bigf) exp ->
            add_to_table 1 pp_expression exp;
		    k exp
        );
	    Visitor_c.kstatement = (fun (k, bigf) stm ->
            add_to_table 1 pp_statement stm;
            k stm
        );
	    Visitor_c.ktype = (fun (k, bigf) ty ->
            add_to_table 1 pp_type ty;
            k ty
        );
	    Visitor_c.kdecl = (fun (k, bigf) d ->
            add_to_table 1 pp_decl d;
            k d
        );
	    Visitor_c.kparam = (fun (k, bigf) p ->
            add_to_table 1 pp_param p;
            k p
        );
        Visitor_c.knode = (fun (k, bigf) nd ->
            add_to_table 20 pp_flow nd;
            k nd
        );
    }
    in
    Visitor_c.vk_node bigf node;
    Hashtbl.fold (fun k v accu -> (k, !v)::accu) tbl []

let stack_wrapper (f: unit -> unit) : (string * int) list =
    Stack.clear strlist_stack;
    Stack.push [("", 1)] strlist_stack;
    f ();
    Stack.pop strlist_stack

let funcall_subtrees node : (string * int) list =
    let asts = ref [] in
    let bigf = { Visitor_c.default_visitor_c with
        Visitor_c.kexpr = (fun (k, bigf) (((exp, typ), ii) as expression )->
            begin match exp, ii with
            | FunCall (e, es), [i1;i2] ->
                asts := stack_wrapper (fun () ->
                    ignore (pp_funcall init_depth expression)
                ) @ !asts
            | _ -> ()
            end;
		    k expression
        );
    }
    in
    Visitor_c.vk_node bigf node;
    !asts

let ast_encode node : (string * int) list =
    let ast_from_root =
        stack_wrapper (fun () -> ignore (pp_flow init_depth node))
    in
    let funcall_ast = funcall_subtrees node in
    List.rev_append ast_from_root funcall_ast (* tail recursive *)
