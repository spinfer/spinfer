(** Group a forest into nodes clusters, grouped by similarity. *)
val clusterize_python : Code.forest -> Code.forest list

val debug_clusters : bool ref (* only for coccinelle clustering *)
