module Ast = Ast_cocci
module V = Visitor_ast

let bind x y = x @ y
let option_default = []
let mcode r m = option_default
let donothing recursor k e = k e (* just combine in the normal way *)


let rec extract_typedef (typeC: Ast.typeC) : string option =
    (* Extract the typedef name from a complete type.
    * Useful for crafting semantic patches because coccinelle need to know
    * if a type is a typedef in the semantic patch header.
    *)
    match Ast.unwrap typeC with
    | Ast.TypeName(name,_,_,_) -> Some(name)
    | Ast.Pointer(fullType,_)
    | Ast.Array(fullType,_,_,_)
    | Ast.TypeOfType(_,_,fullType,_) ->
        begin match Ast.unwrap fullType with
        | Ast.Type(_,_,typeC) -> extract_typedef typeC
        | _ -> None
        end
    | _ -> None


let collect_typedef rule_elem =
    let astfvfullType recursor k ty =
        bind (k ty) (match Ast.unwrap ty with
        | Ast.Type(_,_,typeC) ->
            begin match (extract_typedef typeC) with
            | Some(typedef) -> [typedef]
            | None -> option_default
            end
        | _ -> option_default
        )
    in

    let astfvtypeC recursor k ty =
        bind (k ty) (match (extract_typedef ty) with
        | Some(typedef) -> [typedef]
        | None -> option_default
        )
    in

    let recursor =
        V.combiner bind option_default
        mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        astfvfullType astfvtypeC donothing donothing donothing
        donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
    in
    recursor.V.combiner_rule_elem rule_elem


let collect_iterator rule_elem =
    let astfv_iterator recursor k re =
        bind (k re) (match Ast.unwrap re with
        | Ast.IteratorHeader(id, _, _, _) ->
            begin match Ast.unwrap id with
            | Ast.Id(name) -> [Ast.unwrap_mcode name]
            | _ -> option_default
            end
        | _ -> option_default
        )
    in
    let recursor =
        V.combiner bind option_default
        mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
        mcode mcode mcode mcode
        donothing donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing donothing
        donothing donothing donothing donothing donothing
        donothing donothing donothing donothing
        astfv_iterator donothing donothing donothing donothing
    in
    recursor.V.combiner_rule_elem rule_elem
