module G = Control_flow_c.G
module CFC = Control_flow_c
module SPC = Semantic_patch_coccinelle
module F = Fragment
module AA = Abstract_ast
open Rule_graph


type results = (result_graph * SPC.analyzed_set option) list
type fragments = F.fragment list
type tri_fragments = fragments * fragments * fragments

type indices = int array

type compatible_nodes =
  | Full of int list
  | Partial of (int * int) list
  | Any
  | Empty

type fork_fun = indices -> result_graph -> bool

exception NeedFork of indices

type relationship = (Code.cfg_node_id * Graph.NodeIdSet.t) list
type dominators = relationship * relationship
type context_sets = relationship * relationship


let results_stack : (result_graph * SPC.analyzed_set option) Stack.t = Stack.create ()
let forked_queue : result_graph Queue.t = Queue.create ()

let string_of_compatible_nodes = function
  | Full _ -> "full"
  | Partial(a) -> Printf.sprintf "partial(%d)" (List.length a)
  | Empty -> "empty"
  | Any -> "any"


let arrays_to_ids_list (gids: indices) (nids: indices) : (int * int) list =
  List.combine
    (Array.to_list gids)
    (Array.to_list nids)

let fork_graph (indices: indices) (result: result_graph) =
    let ini_indices = result.graph_ids in
    let left, right = fork_graph indices result in
    let left_indices = left.graph_ids in
    let right_indices = right.graph_ids in
    Debug.print_patch_debug
        (Printf.sprintf "Forked: %s -> %s / %s"
            (Dumper.dump ini_indices)
            (Dumper.dump left_indices)
            (Dumper.dump right_indices)
        );
    left, right


let encode_compatible (l: (int * int) list) (rg: result_graph) =
  match l with
  | [] -> Empty
  | full when List.length full = Array.length rg.graph_ids ->
    Full(Utils.assoc_right full)
  | partial -> Partial(partial)


let print_safety (safety: SPC.safety_status) : unit =
    let safety_str = function
    | SPC.Safe -> "safe"
    | SPC.Unsafe _ -> "unsafe"
    | SPC.Unknown -> "unknown"
    | SPC.UnboundAdded -> "unbound added metavariable"
    in
    Debug.print_patch_debug ("Patch safety status: " ^ safety_str safety)


let unify_compatible_nodes (n1: compatible_nodes) (n2: compatible_nodes)
  : compatible_nodes =
  match n1, n2 with
  | Empty, _ | _, Empty -> Empty
  | Any, other | other, Any -> other
  | Full(l1), Full(l2) ->
    let new_l = List.flatten (List.mapi (fun i (e1, e2) ->
        if e1 = e2
        then [(i, e1)]
        else []
      ) (List.combine l1 l2))
    in
    if List.length new_l = List.length l1
    then Full(Utils.assoc_right new_l)
    else
    if new_l = [] then Empty else Partial(new_l)
  | Full(f_list), Partial(p_list) | Partial(p_list), Full(f_list) ->
    let new_l = List.filter (fun (index, example_id) ->
        List.nth f_list index = example_id
      ) p_list
    in
    if new_l = [] then Empty else Partial(new_l)
  | Partial(l1), Partial(l2) ->
    let new_l = List.filter (fun e -> List.mem e l2) l1 in
    if new_l = [] then Empty else Partial(new_l)

let take_max_compatible v1 v2 =
  match v1, v2 with
  | (_, Full(_) as f), _ | _, (_, Full(_) as f) -> f
  | (_, Empty), other | other, (_, Empty) -> other
  | (_, Partial(long) as p), (_, Partial(short))
    when (List.length long >= List.length short) -> p
  | (_, Partial(short)), (_, Partial(long) as p)
    when (List.length long >= List.length short) -> p
  | (_ as e), _ -> e



let add_dots (result_graph: result_graph) (diffs_code : Code.t) : result_graph =
  let rgraph = Oo.copy result_graph.graph in
  let rnodes = rgraph#nodes in

  let keys_to_add_dots = ref [] in
  CFC.KeyMap.iter(fun key node ->
      match node with
      | Deletion({cfg_nodes; _})
      | Context({cfg_nodes; _})
      | EndMerge(cfg_nodes) ->
        let parent_nodes = Array.to_list cfg_nodes in
        let succs = rgraph#successors key in
        CFGDiff.KeyEdgeSet.iter(fun (skey,_) ->
            let snode = CFC.KeyMap.find skey rnodes in
            begin match snode with
              | Deletion({cfg_nodes; _})
              | Context({cfg_nodes; _})
              | StartMerge(cfg_nodes) ->
                let child_nodes = Array.to_list cfg_nodes in
                let add = ref false in
                List.iteri (fun i gid ->
                    let (diff_graph, _, _) =
                      List.find (fun (_, _, id) -> id = gid) diffs_code in
                    let key_diff_graph = List.nth parent_nodes i in
                    let succ_key_diff_graph = List.nth child_nodes i in
                    let in_succs =
                      CFGDiff.KeyEdgeSet.exists (fun (nid, _) ->
                          succ_key_diff_graph = nid
                        ) (diff_graph#successors key_diff_graph)
                    in
                    let missing_successor = if in_succs = false then
                        not (
                          CFGDiff.KeyEdgeSet.exists (fun (nid, _) ->
                              let (_,node) = CFC.KeyMap.find nid diff_graph#nodes in
                              match CFC.unwrap node with
                              | CFC.SeqStart(_,_,_)
                              | CFC.SeqEnd(_) ->
                                CFGDiff.KeyEdgeSet.exists (fun (key,_)->
                                    succ_key_diff_graph = key
                                  ) (diff_graph#successors nid);
                              | _ -> succ_key_diff_graph = nid
                            ) (diff_graph#successors key_diff_graph)
                        ) else false
                    in
                    let no_concrete_merge =
                      begin match node, snode with
                        | (EndMerge(prev), Deletion({cfg_nodes=succ;}))
                        | (EndMerge(prev), Context({cfg_nodes=succ;}))
                        | (Deletion({cfg_nodes=prev}), StartMerge(succ))
                        | (Context({cfg_nodes=prev}), StartMerge(succ))
                          when prev <> succ -> true
                        | EndMerge _, StartMerge _ -> true
                        | _ -> false
                      end
                    in
                    if (missing_successor || no_concrete_merge)
                    then add := true;
                  ) (Array.to_list result_graph.graph_ids);
                if !add then
                  begin
                    let context = ref false in
                    List.iteri(fun i gid ->
                        let (graph, _, _) =
                          List.find (fun (_, _, id) -> id = gid) diffs_code
                        in
                        let key_diff_graph = List.nth parent_nodes i in
                        let succ_key_diff_graph = List.nth child_nodes i in
                        let visited_nodes = ref [] in
                        (**Check if the dots is context**)
                        let rec dots_is_context graph src dest: bool =
                          CFGDiff.KeyEdgeSet.exists(fun (key,_) ->
                              let nodes = graph#nodes in
                              let (prefix,_) = Control_flow_c.KeyMap.find key nodes in
                              match prefix with
                              | CFGDiff.Both -> true
                              | _ -> if key = dest then false else
                                if (not (List.mem key !visited_nodes)) then
                                  begin
                                    visited_nodes := key ::!visited_nodes;
                                    (dots_is_context graph key dest)
                                  end
                                else false
                            )  (graph#successors src);
                        in
                        if (dots_is_context graph key_diff_graph succ_key_diff_graph)
                        then context := true
                      )  (Array.to_list result_graph.graph_ids);
                    keys_to_add_dots := (key,skey,!context) :: !keys_to_add_dots;
                  end
              | _ -> ()
            end
          ) succs;

      | _ -> ()
    ) rgraph#nodes;
  List.iter(fun (src, dest, context) ->
      let key = if context then rgraph#add_node Dots
        else rgraph#add_node DeletedDots
      in
      rgraph#del_arc ((src, dest), CFGDiff.Both);
      rgraph#add_arc ((src, key), CFGDiff.Both);
      rgraph#add_arc ((key, dest), CFGDiff.Both);
    ) !keys_to_add_dots;
  {result_graph with
   graph=rgraph;
  }


(** Gives the maximum compatible nodes so that metavariable can be reduced to a
 constant value. Very useful for constant values in addition fragments *)
let metavar_const_compat (metavar: AA.Metavariable.t) (ids: (int * int) list)
        (rg: result_graph) : compatible_nodes =
    let all_examples = AA.Metavariable.elements metavar in
    let relevant_examples = List.map (fun id ->
        List.find (fun (cfg_id, _) -> cfg_id = id) all_examples
    ) ids
    in
    let counter_htbl = Hashtbl.create 30 in
    List.iter (fun (_, tree) ->
        if Hashtbl.mem counter_htbl tree
        then
            let value = Hashtbl.find counter_htbl tree in
            Hashtbl.replace counter_htbl tree (value + 1)
        else
            Hashtbl.add counter_htbl tree 1
    ) relevant_examples;
    let maximum = Hashtbl.fold (fun tree count accu ->
        match accu with
        | None -> Some(tree, count)
        | Some(_, old_count) when old_count < count -> Some(tree, count)
        | _ -> accu
    ) counter_htbl None
    in
    match maximum with
    | None -> Empty
    | Some(tree, _) ->
        let examples_matching = List.filter (fun (_, (_, ex_tree)) ->
            ex_tree = tree
        ) (List.mapi (fun i elt -> (i, i), elt) relevant_examples)
        in
        encode_compatible (Utils.assoc_left examples_matching) rg

let powerset_cond_iter f l =
    let rec autopairs process = function
    | [] -> ()
    | x::xs ->
    List.iter (fun y -> process (x@y)) xs;
    autopairs process xs in
    let rec otherpairs process l1 l2 =
        List.iter (fun x -> List.iter (fun y -> process (x@y)) l2) l1 in
    let placed = ref [] in
    let tested = ref [] in
    let results = ref [] in
    let process changed attempt =
        if List.exists (fun x -> List.mem x !placed) attempt
        then ()
        else begin
            tested := Common.union_set attempt !tested;
            if f attempt
            then begin
                placed := attempt @ !placed;
                results := attempt :: !results;
                changed := true
            end
        end
    in
    let rec loop available leftovers =
        let changed = ref false in
        placed := [];
        tested := [];
        results := [];
        autopairs (process changed) available;
        if !changed
        then begin
            let not_in_placed x = not(List.mem (List.hd x) !placed) in
            let not_in_tested x = not(List.mem (List.hd x) !tested) in
            let new_leftovers = List.filter not_in_placed available in
            let redo, new_leftovers =
                List.partition not_in_tested new_leftovers
            in
            let original_results = !results in
            placed := [];
            results := [];
            otherpairs (process changed) original_results redo;
            let new_redo = List.filter not_in_placed redo in
            let original_results =
                List.filter not_in_placed original_results
            in
            loop (!results@original_results)
                (new_redo @ new_leftovers @ leftovers)
        end
        else available @ leftovers
    in
    loop (List.map (fun x -> [x]) l) []

let tctr = ref 0

let calc_comp_metavars (result_graph: result_graph) (code: Code.t)
        : compatible_nodes list =
    let all_indices =
        Utils.list_init (Array.length result_graph.graph_ids) (fun a -> a)
    in
    let test current_indices = incr tctr;
      let delcon_metavars = ref [] in
      let add_metavars = ref [] in
      CFC.KeyMap.iter (fun key node ->
        match node with
        | Deletion({content=F.Pattern _; cfg_nodes})
        | Context({content=F.Pattern _; cfg_nodes})
        | Addition({content=F.Pattern _; cfg_nodes}) ->
            let examples_ids =
          Utils.filteri (fun i _ -> List.mem i current_indices)
        (arrays_to_ids_list result_graph.graph_ids cfg_nodes) in
            let new_pattern = AA.pattern_from_ids examples_ids code true in
            List.iter (fun (_, m) ->
              let ids_ast = AA.Metavariable.elements m in
              let asts =
        List.map (fun id -> List.assoc id ids_ast) examples_ids in
              match node with
              | Deletion _ | Context _ ->
                  delcon_metavars := asts::!delcon_metavars
              | Addition _ ->
                  add_metavars := asts::!add_metavars
              | _ -> assert false
            ) new_pattern.AA.metavars
        | _ -> ()
      )
    result_graph.graph#nodes;
      List.for_all (fun m -> List.mem m !delcon_metavars) !add_metavars in
    let res =
      if test all_indices
      then [all_indices]
      else powerset_cond_iter test all_indices in
    (if !Debug.show_logs
    then
      begin
    Printf.eprintf "tctr: %d %d\n" !tctr (List.length res);
    List.iter
      (function x ->
        Printf.eprintf "option: %s\n"
          (String.concat " " (List.map string_of_int x)))
      res
      end);
    List.map
      (fun res ->
    encode_compatible (List.map (fun i -> i,i) res) result_graph)
      res

let sort_alist_by_matched_nodes (alist: F.fragment list)
  : F.fragment list =
  let compare_matched_nodes a b =
    Pervasives.compare
      (List.length a.F.examples)
      (List.length b.F.examples)
  in
  List.sort compare_matched_nodes alist


(** Extract cfg graph_id and cfg node_id from fragments ids
*)
let extract_matched_nodes (keys: F.frag_ids)
    (examples: F.example list)
  : (int * int) list =
  List.map (List.assoc keys) examples


let pattern_node_of_fragment_node (node: F.node)
    (cfg_nodes: concrete_node_ids)
  : pattern_node =
  let concrete_node = {
    cfg_nodes=cfg_nodes;
    content=node.F.content
  } in
  match node.F.diff_type with
  | CFGDiff.Both -> Context(concrete_node)
  | CFGDiff.Before -> Deletion(concrete_node)
  | CFGDiff.After -> Addition(concrete_node)


let check_insert_node (node: pattern_node) (result_graph: result_graph) =
  match node with
  | Entry | NormalExit | ErrorExit
  | Dots | DeletedDots -> result_graph.graph#add_node node
  | Context({cfg_nodes=cfg_nodes;_})
  | Addition({cfg_nodes=cfg_nodes;_})
  | Deletion({cfg_nodes=cfg_nodes;_})
  | StartMerge(cfg_nodes)
  | EndMerge(cfg_nodes) ->
    if (Array.length cfg_nodes) = (Array.length result_graph.graph_ids)
    then result_graph.graph#add_node node
    else begin
      Debug.exec_if_graphs_dir
        (print_result_graph result_graph) "last_patch.gv";
      failwith ("Incorrect cfg_nodes array length")
    end


(** Check if cfg_ids dominate frag_ids and frag_ids post-dominate cfg_ids.
    If reverse the roles of cfg_ids and frag_ids are inverted *)
let check_domination (dominators, postdominators)
    (strict: bool)
    (reverse: bool)
    ((cfg_gid, cfg_nid): int * int)
    ((frag_gid, frag_nid): int * int)
  : bool =
  (* Symbol for debug print *)
  let sym =
    if reverse
    then
      if strict then "<" else "<="
    else
    if strict then ">" else ">="
  in
  (* Assigning dominator and post-dominator roles to concrete and
     cfg nodes *)
  let dom_graph, dom_node, post_graph, post_node =
    if not reverse
    then (cfg_gid, cfg_nid, frag_gid, frag_nid)
    else (frag_gid, frag_nid, cfg_gid, cfg_nid)
  in
  if (frag_gid = cfg_gid)
  then try
      if (frag_nid = cfg_nid)
      then not strict
      else begin
        (* Getting the dominators set for the after node,
           a.k.a the post node *)
        let dom =
          List.assoc (post_graph, post_node) dominators
        in
        let post =
          List.assoc (dom_graph, dom_node) postdominators
        in

        (* Checking if assigned roles are correct *)
        let res =
          (Graph.NodeIdSet.mem (dom_graph, dom_node) dom) &&
          (Graph.NodeIdSet.mem (post_graph, post_node) post)
        in
        if res
        then
          Debug.print_patch_debug
            (Printf.sprintf "Id %d: %d %s %d"
               frag_gid dom_node sym post_node
            );
        res
      end
    with Not_found -> false
  else false


(** Sort nodes using the total dominator order. *)
let dominator_sort dominators (reverse: bool) =
  let sort a b =
    if a = b then 0
    else
    if check_domination dominators false reverse a b
    then 1
    else -1
  in
  sort


let existing_sort existing dominators (reverse: bool) =
  let exists elt = List.mem elt existing in
  let sort a b =
    match exists a, exists b with
    | (true, false) -> -1
    | (false, true) -> 1
    | _ -> (dominator_sort dominators reverse) a b
  in
  sort

(** Choice function based on domination relation.
    Sometimes multiple nodes can dominate and be dominated by
    a node in the result graph.
    That usually mean there are duplicate fragments in the result graph.
    In that case we need to choose "the closest" from the result graph node.
    Hopefully the set of compatible nodes form a total ordered set in respect
    to the domination relation. We can then take the first of the last depending
    on the situation.
*)
let choose_dominator_order dominators (reverse: bool) (ids: (int * int) list)
  : int * int =
  List.hd (List.sort (dominator_sort dominators reverse) ids)


let choose_with_existing (existing: (int * int) list) dominators (reverse: bool)
    (ids: (int * int) list) : int * int =
  List.hd (List.sort (existing_sort existing dominators reverse) ids)


let choose_cset (result_graph: result_graph) doms (nodes: (int * indices) list)
  : (int * indices) =
  List.fold_left (fun max_elt node ->
      let get_indices key =
        match CFC.KeyMap.find key result_graph.graph#nodes with
        | Context({cfg_nodes; _})
        | Deletion({cfg_nodes; _}) -> cfg_nodes
        | _ -> assert false
      in
      let ids_list =
        arrays_to_ids_list result_graph.graph_ids (get_indices (fst node))
      in
      let ids_max_list =
        arrays_to_ids_list result_graph.graph_ids (get_indices (fst max_elt))
      in
      let is_greater = List.for_all2 (fun ids_max ids ->
          let max_dom = List.assoc ids_max doms in
          Graph.NodeIdSet.mem ids max_dom
        ) ids_max_list ids_list
      in
      if is_greater then node else max_elt
    ) (List.hd nodes) nodes


(** Check if a node from a pattern graph node `src` is compatible as an
    entry point for all `ids`.
*)
let check_entry (ids: (int * int) list) (src: CFC.KeyMap.key)
    ?(restrict_merge=false)
    ?(existing=None)
    (result_graph: result_graph) doms
  : compatible_nodes =
  let src_node =
    CFC.KeyMap.find src result_graph.graph#nodes
  in
  let is_strict = match src_node with
    | EndMerge(_) -> false
    | _ -> true
  in
  let check cfg_nodes =
    let rg_ids = arrays_to_ids_list result_graph.graph_ids cfg_nodes in
    let sort_fun (_, a) (_, b) = dominator_sort doms true a b in
    let check_fun (i, cfg_ids) (j, frag_ids) =
      match existing with
      | Some(existing) ->
        List.mem (i, j) existing
        &&
        check_domination doms is_strict false cfg_ids frag_ids
      | None ->
        check_domination doms is_strict false cfg_ids frag_ids
    in
    let compatible = Utils.bilist_pairs check_fun sort_fun rg_ids ids in
    encode_compatible compatible result_graph
  in
  match src_node with
  | Entry -> Any
  | Context({cfg_nodes; _})
  | Deletion({cfg_nodes; _})
  | Addition({cfg_nodes; _})
  | StartMerge(cfg_nodes) -> check cfg_nodes
  (* For addition fragments with restrict_merge an EndMerge node is not a
   * valid predecessor.
   * Additions fragments need printable context or deletion nodes to attach to.
   *)
  | EndMerge(cfg_nodes) when not restrict_merge -> check cfg_nodes
  | _ -> Empty


(** Check if a node from a pattern graph node `dest` is compatible as an
    exit point for all `ids`.
*)
let check_exit (ids: (int * int) list) (dest: CFC.KeyMap.key)
    ?(restrict_merge=false)
    ?(existing=None)
    (result_graph: result_graph) doms
  : compatible_nodes =
  let dest_node =
    CFC.KeyMap.find dest result_graph.graph#nodes
  in
  let is_strict = match dest_node with
    | StartMerge(_) -> false
    | _ -> true
  in
  let check cfg_nodes =
    let rg_ids = arrays_to_ids_list result_graph.graph_ids cfg_nodes in
    let sort_fun (_, a) (_, b) = dominator_sort doms false a b in
    let check_fun (i, cfg_ids) (j, frag_ids) =
      match existing with
      | Some(existing) ->
        List.mem (i, j) existing
        &&
        check_domination doms is_strict true cfg_ids frag_ids
      | None ->
        check_domination doms is_strict true cfg_ids frag_ids
    in
    let compatible = Utils.bilist_pairs check_fun sort_fun rg_ids ids in
    encode_compatible compatible result_graph
  in
  match dest_node with
  | ErrorExit
  | NormalExit -> Any
  | Context({cfg_nodes; _})
  | Deletion({cfg_nodes; _})
  | Addition({cfg_nodes; _})
  | EndMerge(cfg_nodes) -> check cfg_nodes
  (* For addition fragments with restrict_merge a StartMerge node is not a
   * valid successor.
   * Additions fragments need printable context or deletion nodes to attach to.
   *)
  | StartMerge(cfg_nodes) when not restrict_merge -> check cfg_nodes
  | _ -> Empty



(** Add nodes of subfragment to result_graph. Returns an association list
    of fragment node id to graph key *)
let add_subfragment_to_graph (subfragment: F.subfragment)
    (subfrag_id: int)
    (examples: F.example list)
    (result_graph: result_graph)
  : (F.node_id * G.key) list =
  (* First step add nodes *)
  let mapping = List.map (fun node ->
      let node_id = node.F.node_id in
      let _, cfg_nodes = List.split (
          extract_matched_nodes (subfrag_id, node_id) examples
        )
      in
      let pattern_node =
        pattern_node_of_fragment_node node (Array.of_list cfg_nodes)
      in
      let key = check_insert_node pattern_node result_graph in
      (node_id, key)
    ) subfragment.F.nodes
  in

  (* Second step add subfragment edges *)
  List.iter ( fun (source, dest) ->
      result_graph.graph#add_arc (
        (
          (List.assoc source mapping),
          (List.assoc dest mapping)
        ),
        CFGDiff.Both
      )
    ) subfragment.F.edges;
  mapping


let add_fragment_to_graph (edge_port_list)
    (fragment: F.fragment)
    (result_graph: result_graph)
  : (F.node_id * G.key) list list =

  (* Add all subfragment *)
  Debug.print_patch_debug
    ("Adding new fragment: " ^ (F.stringify_fragment fragment));
  let mapping = List.mapi (fun i subfragment ->
      add_subfragment_to_graph subfragment i
        fragment.F.examples result_graph
    ) fragment.F.subfragments
  in

  let unused_connections = List.filter (fun ((fid1, nid1), (fid2, nid2)) ->
      (* Order for secondary connection is (exit, entry) *)
      not (List.exists (fun (_, ((efid1, enid1), (efid2, enid2))) ->
          (* Order for port is (entry, exit) so numbers are inverted *)
          efid1 = fid2 && enid1 = nid2 || efid2 = fid1 && enid2 = nid1
        ) edge_port_list)
    ) fragment.F.secondary_connections
  in
  let get_node_id (entry_fid, entry_nid) =
    List.assoc entry_nid (List.nth mapping entry_fid)
  in

  (* Add main edges, removed used ports *)
  List.iter ( fun ((source, dest),
                   ((entry_fid, entry_nid), (exit_fid, exit_nid))) ->
              result_graph.graph#del_arc ((source, dest), CFGDiff.Both);
              result_graph.graph#add_arc (
                (
                  source,
                  get_node_id (entry_fid, entry_nid)
                ),
                CFGDiff.Both
              );
              result_graph.graph#add_arc (
                (
                  get_node_id (exit_fid, exit_nid),
                  dest
                ),
                CFGDiff.Both
              );
              (* Pretty-print edge remplacement *)
              let debug_str = Printf.sprintf
                  "Replaced edges: (%d, %d) -> (%d, %d) (%d, %d)"
                  source dest
                  source (get_node_id (entry_fid, entry_nid))
                  (get_node_id (exit_fid, exit_nid)) dest
              in
              Debug.print_patch_debug debug_str
            ) edge_port_list;


  (* Create edges for unused connections *)

  List.iter (fun (exit_ids, entry_ids) ->
      let entry_node_id = get_node_id entry_ids in
      let exit_node_id = get_node_id exit_ids in
      result_graph.graph#add_arc (
        (
          exit_node_id,
          entry_node_id
        ),
        CFGDiff.Both
      );
    ) unused_connections;

  mapping



let create_initial_graph (initial_fragment: F.fragment) doms
  : result_graph =
  Debug.print_patch_debug
    "\n\n\n========== Creating new graph ==========\n";
  let graph = new CFGDiff.CEG.ograph_mutable in

  (* Bootstrapping the graph_ids *)
  let initial_subfragment =
    List.hd initial_fragment.F.subfragments
  in

  let first_node_key =
    (List.hd initial_subfragment.F.nodes).F.node_id
  in
  let first_matches =
    extract_matched_nodes
      (0, first_node_key) initial_fragment.F.examples
  in
  let graph_ids = Array.of_list (fst (List.split first_matches)) in

  let result_graph = {
    graph=graph;
    graph_ids=graph_ids;
    trivial_ids=(0, 0, 0)
  } in

  (* Preparing trivial nodes *)
  let entry = check_insert_node Entry result_graph in
  let exit = check_insert_node NormalExit result_graph in
  let error_exit = check_insert_node ErrorExit result_graph in

  result_graph.trivial_ids <- (entry, exit, error_exit);

  result_graph.graph#add_arc ((entry, exit), CFGDiff.Both);

  (* Adding subfragment to graph *)
  let f_entry = initial_fragment.F.entry in
  let f_exit = initial_fragment.F.exit in
  let edge_port = ((entry, exit), (f_entry, f_exit)) in
  ignore (
    add_fragment_to_graph [edge_port] initial_fragment result_graph
  );

  result_graph


let get_edges (result_graph: result_graph) =
  CFC.KeyMap.fold (fun key node accu ->
      let succs = result_graph.graph#successors key in
      CFGDiff.KeyEdgeSet.fold (fun (succ, edge_type) accu ->
          (key, succ)::accu
        ) succs accu
    ) result_graph.graph#nodes []


let empty_filter = function
  | Empty -> false
  | _ -> true


(** Try to add context or deleted fragment to result graph using dominance
    information. *)
let add_fragment (fragment: F.fragment) (result_graph: result_graph)
    doms (allow_fork: fork_fun) (is_added: bool)
  : (bool * result_graph * (F.fragment option)) =
  Debug.print_patch_debug ("\nTrying: " ^ (F.stringify_fragment fragment));
  let initial_graph = Oo.copy result_graph.graph in
  let edges = get_edges result_graph in

  let splitted_graph = dissociate_result_graph result_graph is_added in
  let rg_doms =
    let rg_entry, rg_exit, _ = splitted_graph.trivial_ids in
    Graph.get_dominators
      [(splitted_graph.graph, 0)]
      (fun _ -> rg_entry)
      (fun _ -> rg_exit)

  in
  let rg_entry, rg_exit, rg_error_exit = result_graph.trivial_ids in
  let non_trivial_keys = List.filter (fun key ->
      if key = rg_entry || key = rg_exit || key = rg_error_exit
      then false
      else true
    ) (Utils.assoc_left (CFC.KeyMap.bindings result_graph.graph#nodes))
  in
  let map_port_to_ids (ports: (int * int) list) (is_source: bool)
      (assoc_existing: ((int * int) * ((int * int) list)) list) =
    List.flatten (List.map (fun port ->
        let frag_ids = F.extract_ids_for_port port fragment in
        let existing =
          try
            Some(List.assoc port assoc_existing)
          with Not_found -> None
        in
        let compatible_nodes = List.map (fun key ->
            if is_source
            then
              (key, check_entry frag_ids key
                 result_graph doms ~existing:existing ~restrict_merge:is_added)
            else
              (key, check_exit frag_ids key
                 result_graph doms ~existing:existing ~restrict_merge:is_added)
          ) non_trivial_keys
        in
        let non_empty = List.filter (fun (_, nodes) ->
            empty_filter nodes
          ) compatible_nodes
        in
        if non_empty <> []
        then
          let key, nodes = List.hd (List.sort (fun (key1, _) (key2, _) ->
              dominator_sort rg_doms (not is_source) (0, key1) (0, key2)
            ) non_empty)
          in
          [(port, key, nodes)]
        else []
      ) ports)
  in

  let all_connections =
    ((fragment.F.exit, fragment.F.entry)::fragment.F.secondary_connections)
  in
  let exit_ports, entry_ports = List.split all_connections in
  (* Adding trivial edges if missing *)
  let augment_entry entry_ids =
    let contains_main_entry = List.exists (fun (p, _, _) ->
        p = fragment.F.entry
      ) entry_ids
    in
    if contains_main_entry
    then entry_ids
    else ((fragment.F.entry, rg_entry, Any)::entry_ids)
  in
  let augment_exit exit_ids =
    let contains_main_exit = List.exists (fun (p, _, _) ->
        p = fragment.F.exit
      ) exit_ids
    in
    if contains_main_exit
    then exit_ids
    else ((fragment.F.exit, rg_exit, Any)::exit_ids)
  in

  let get_chain_and_compatible (direct: bool) =
    let fst_ports, snd_ports =
      if direct
      then entry_ports, exit_ports
      else exit_ports, entry_ports
    in
    let fst_ids = map_port_to_ids fst_ports direct [] in
    let existing = List.flatten (List.map2 (fun fst_port snd_port ->
        try
          let _, _, nodes =
            List.find (fun (p, _, _) -> p = fst_port) fst_ids
          in
          match nodes with
          | Full(indices) -> [(
              snd_port,
              List.combine
                (Utils.list_init (List.length indices) (fun i -> i))
                indices
            )]
          | Partial(mapping) -> [(snd_port, mapping)]
          | _ -> assert false
        with Not_found -> []
      ) fst_ports snd_ports)
    in
    let snd_ids = map_port_to_ids snd_ports (not direct) existing in

    let entry_ids, exit_ids =
      if direct
      then fst_ids, snd_ids
      else snd_ids, fst_ids
    in
    let entry_ids = augment_entry entry_ids in
    let exit_ids = augment_exit exit_ids in

    let rec chain_ports (accu: ('a * 'a) list) (entry: 'a)
        (fragment: F.fragment) =
      let _, pred, _ = entry in
      let exit = List.find (fun (_, succ, _) ->
          List.mem (pred, succ) edges
        ) exit_ids
      in
      let new_accu = (entry, exit)::accu in
      let exit_port, _, _ = exit in
      if exit_port = fragment.F.exit
      then
        List.rev new_accu
      else
        let _, associated_entry = List.find (fun (exit, _) ->
            exit = exit_port
          ) fragment.F.secondary_connections
        in
        let next_entry = List.find (fun (p, _, _) ->
            p = associated_entry
          ) entry_ids
        in
        chain_ports new_accu next_entry fragment
    in
    try
      let ports_chain =
        chain_ports
          []
          (List.find (fun (p, _, _) -> p = fragment.F.entry) entry_ids)
          fragment
      in

      let all_compatible_nodes =
        List.fold_left (fun accu ((_, _, c1), (_, _, c2)) ->
            c1::c2::accu
          ) [] ports_chain
      in

      let unified_compatible_nodes =
        List.fold_left unify_compatible_nodes Any all_compatible_nodes
      in
      (ports_chain, unified_compatible_nodes)
    with Not_found -> ([], Empty)
  in

  let ports_chain, compatible = take_max_compatible
      (get_chain_and_compatible true) (get_chain_and_compatible false)
  in
  Debug.print_patch_debug (Printf.sprintf
                             "Compatibility is %s."
                             (string_of_compatible_nodes compatible)
                          );
  match compatible with
  | Full(example_ids) ->
    let f_opt, remaining =
      F.partition_with_indices fragment example_ids
    in
    let to_insert = match f_opt with
      | Some(f) -> f
      | None -> assert false
    in
    let edge_port_list =
      List.map (fun ((entry, src, _), (exit, dest, _)) ->
          ((src, dest), (entry, exit))
        ) ports_chain
    in
    ignore (add_fragment_to_graph edge_port_list to_insert result_graph);
    (true, result_graph, remaining)
  | Partial(indices) ->
    let gids = (Array.of_list (Utils.assoc_left indices)) in
    if allow_fork gids result_graph
    then
      raise (NeedFork(gids))
    else
      (false, {result_graph with graph=initial_graph}, Some(fragment))
  | _ ->
    (false, {result_graph with graph=initial_graph}, Some(fragment))



let add_addition_fragment (fragment: F.fragment)
    (result_graph: result_graph)
    (before_doms, after_doms) (pre_csets, post_csets)
    (allow_fork: fork_fun)
  : (bool * result_graph * F.fragment option) =
  let initial_graph = Oo.copy result_graph.graph in
  let nodes = result_graph.graph#nodes in
  let changed, result_graph, new_fragment =
    add_fragment fragment result_graph after_doms allow_fork true
  in
  if changed
  then (true, result_graph, new_fragment)
  else begin
    let map_port_list (port_list: (int * int) list) (is_source: bool) =
      (* In the following code we make the hypothesis the context set
         is a singleton *)
      let is_compatible (src_ids, dest_ids: (int * int) * (int * int))
          (frag_ids: int * int) : bool =
        let cset, main_ids, sec_ids = if is_source
          then pre_csets, dest_ids, src_ids
          else post_csets, src_ids, dest_ids
        in
        let rg_cset = List.assoc main_ids cset in
        let frag_cset = List.assoc frag_ids cset in
        Graph.NodeIdSet.equal rg_cset frag_cset
        &&
        (* Check if singleton *)
        Graph.NodeIdSet.cardinal frag_cset = 1
        &&
        List.exists (fun context_ids ->
            (* Check if context to add dominates both deleted
               and added nodes *)
            check_domination before_doms false (not is_source)
              context_ids main_ids
            &&
            check_domination after_doms false (not is_source)
              context_ids frag_ids
            &&
            (* Check if context to add is dominated by predecessor *)
            check_domination before_doms false (not is_source)
              sec_ids context_ids
          ) (Graph.NodeIdSet.elements rg_cset)
      in
      let is_compatible_any (rg_ids: (int * int))
          (frag_ids: int * int) : bool =
        let cset = if is_source then pre_csets else post_csets
        in
        let rg_cset = List.assoc rg_ids cset in
        let frag_cset = List.assoc frag_ids cset in
        Graph.NodeIdSet.equal rg_cset frag_cset
        &&
        (* Check if singleton *)
        Graph.NodeIdSet.cardinal frag_cset = 1
        &&
        List.exists (fun context_ids ->
            (* Check if context to add dominates both deleted
               and added nodes *)
            check_domination before_doms false (not is_source)
              context_ids rg_ids
            &&
            check_domination after_doms false (not is_source)
              context_ids frag_ids
          ) (Graph.NodeIdSet.elements rg_cset)
      in

      let edge_valid (src, dest) : bool =
        let is_unique edge_set =
          CFGDiff.KeyEdgeSet.cardinal edge_set = 1
        in
        if is_source
        then
          let preds = result_graph.graph#predecessors dest in
          is_unique preds
        else
          let succs = result_graph.graph#successors src in
          is_unique succs
      in
      List.map (fun port ->
          let examples_ids =
            F.extract_ids_for_port port fragment
          in
          let nodes_true = List.fold_left (fun accu ((src, dest) as edge) ->
              let key = if is_source then dest else src in
              if edge_valid edge
              then
                match CFC.KeyMap.find src nodes with
                | Entry when is_source ->
                  begin match CFC.KeyMap.find dest nodes with
                    | Context({cfg_nodes; _}) | Deletion({cfg_nodes; _}) ->
                      let dest_ids =
                        arrays_to_ids_list result_graph.graph_ids cfg_nodes
                      in
                      let compatible =
                        Utils.bilist_pairs
                          (fun (_, a) (_, b) -> is_compatible_any a b)
                          compare dest_ids examples_ids
                      in
                      (key, encode_compatible compatible result_graph)::accu
                    | _  -> accu
                  end
                | Context({cfg_nodes; _}) | Deletion({cfg_nodes; _})
                | EndMerge(cfg_nodes) ->
                  let src_ids =
                    arrays_to_ids_list result_graph.graph_ids cfg_nodes
                  in
                  begin match CFC.KeyMap.find dest nodes with
                    | NormalExit when not is_source ->
                      let compatible =
                        Utils.bilist_pairs
                          (fun (_, a) (_, b) -> is_compatible_any a b)
                          compare src_ids examples_ids
                      in
                      (key, encode_compatible compatible result_graph)::accu
                    | Context({cfg_nodes; _}) | Deletion({cfg_nodes; _})
                    | StartMerge(cfg_nodes) ->
                      let dest_ids =
                        arrays_to_ids_list result_graph.graph_ids cfg_nodes
                      in
                      let compatible =
                        Utils.bilist_pairs
                          (fun (_, a) (_, b) -> is_compatible a b)
                          compare
                          (List.combine src_ids dest_ids)
                          examples_ids
                      in
                      (key, encode_compatible compatible result_graph)::accu
                    | _ -> accu
                  end
                | _ -> accu
              else accu
            ) [] (get_edges result_graph)
          in
          let non_empty = List.filter (fun (_, nodes) ->
              empty_filter nodes
            ) nodes_true
          in
          let key, nodes =
            List.fold_left take_max_compatible (0, Empty) non_empty
          in
          (port, key, nodes)
        ) port_list
    in

    let secondary_ports, secondary_edges =
      List.partition (fun (exit_ids, entry_ids) ->
          List.exists (fun example ->
              let main_exit_cfg_ids =
                List.assoc fragment.F.exit example
              in
              let sec_exit_cfg_ids =
                List.assoc exit_ids example
              in
              let exit_ok = not (
                  Graph.NodeIdSet.equal
                    (List.assoc main_exit_cfg_ids post_csets)
                    (List.assoc sec_exit_cfg_ids post_csets)
                )
              in
              let main_entry_cfg_ids =
                List.assoc fragment.F.entry example
              in
              let sec_entry_cfg_ids =
                List.assoc entry_ids example
              in
              let entry_ok = not (
                  Graph.NodeIdSet.equal
                    (List.assoc main_entry_cfg_ids pre_csets)
                    (List.assoc sec_entry_cfg_ids pre_csets)
                )
              in
              exit_ok && entry_ok
            ) fragment.F.examples
        ) fragment.F.secondary_connections
    in
    let exit_ports, entry_ports = (
      fragment.F.exit::(Utils.assoc_left secondary_ports),
      fragment.F.entry::(Utils.assoc_right secondary_ports)
    )
    in
    let entry, exit, unified_compatible_nodes =
      try
        let entry = map_port_list entry_ports true in

        (* Resetting nodes *)
        let exit = map_port_list exit_ports false in
        Debug.print_patch_debug (Printf.sprintf "Entry: %s" (Dumper.dump entry));
        Debug.print_patch_debug (Printf.sprintf "Exit: %s" (Dumper.dump exit));

        let all_compatible_nodes =
          let extract_compatible = (fun (_, _, c) -> c) in
          (List.map extract_compatible entry)
          @
          (List.map extract_compatible exit)
        in

        let unified_compatible_nodes =
          List.fold_left unify_compatible_nodes Any all_compatible_nodes
        in
        (entry, exit, unified_compatible_nodes)
      with Failure _ | Not_found -> ([], [], Empty)
    in


    Debug.print_patch_debug (Printf.sprintf
                               "Compatibility is %s."
                               (string_of_compatible_nodes unified_compatible_nodes)
                            );
    match unified_compatible_nodes with
    | Full(example_ids) ->
      let f_opt, remaining =
        F.partition_with_indices fragment example_ids
      in
      let to_insert = match f_opt with
        | Some(f) -> f
        | None -> assert false
      in

      let mapping = List.mapi (fun i subfragment ->
          add_subfragment_to_graph subfragment i
            to_insert.F.examples result_graph
        ) fragment.F.subfragments
      in

      let get_node_id (entry_fid, entry_nid) =
        List.assoc entry_nid (List.nth mapping entry_fid)
      in

      let get_merge_indices removed_key cset =
        let result_node =
          CFC.KeyMap.find removed_key result_graph.graph#nodes
        in
        let removed_indices = match result_node with
          | Context({cfg_nodes; _})
          | Deletion({cfg_nodes; _}) -> cfg_nodes
          | _ ->
          Printf.eprintf "unexpected node: %s\n" (pattern2c result_node);
          assert false
        in
        let rg_ids =
          arrays_to_ids_list result_graph.graph_ids removed_indices
        in
        Array.of_list (List.map (fun ids ->
            let context_set = List.assoc ids cset in
            (* Context set is supposed to be a singleton *)
            snd (Graph.NodeIdSet.choose context_set)
          ) rg_ids)
      in

      List.iter (fun (map_ids, removed_key, _) ->
          let merge_indices =
            get_merge_indices removed_key pre_csets
          in
          let key_to_replace, _ =
            (* Was checked for uniqueness before *)
            CFGDiff.KeyEdgeSet.choose
              (result_graph.graph#predecessors removed_key)
          in
          let merge_key =
            check_insert_node (StartMerge(merge_indices)) result_graph
          in


          (* Inserting merge node just before the removed key *)
          let added_key = get_node_id map_ids in
          result_graph.graph#del_arc (
            (
              key_to_replace,
              removed_key
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              key_to_replace,
              merge_key
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              merge_key,
              removed_key
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              merge_key,
              added_key
            ),
            CFGDiff.Both);
        ) entry;
      List.iter (fun (map_ids, removed_key, _) ->
          let merge_indices =
            get_merge_indices removed_key post_csets
          in
          let key_to_replace, _ =
            (* Was checked for uniqueness before *)
            CFGDiff.KeyEdgeSet.choose
              (result_graph.graph#successors removed_key)
          in
          let merge_key =
            check_insert_node (EndMerge(merge_indices)) result_graph
          in
          let added_key = get_node_id map_ids in

          (* Inserting merge node just after the removed key *)
          result_graph.graph#del_arc (
            (
              removed_key,
              key_to_replace
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              removed_key,
              merge_key
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              added_key,
              merge_key
            ),
            CFGDiff.Both);
          result_graph.graph#add_arc (
            (
              merge_key,
              key_to_replace
            ),
            CFGDiff.Both);
        ) exit;
      List.iter (fun (ids_source, ids_dest) ->
          let source = get_node_id ids_source in
          let dest = get_node_id ids_dest in
          result_graph.graph#add_arc (
            (
              source,
              dest
            ),
            CFGDiff.Both);
        ) secondary_edges;

      Debug.print_patch_debug
        ("Adding new fragment: " ^ (F.stringify_fragment fragment));
      (true, result_graph, remaining)
    | Partial(indices) ->
      let gids = (Array.of_list (Utils.assoc_left indices)) in
      if allow_fork gids result_graph
      then
        raise (NeedFork(gids))
      else
        (false, {result_graph with graph=initial_graph}, Some(fragment))
    | _ ->
      (false, {result_graph with graph=initial_graph}, Some(fragment))
  end


let commit_if_changed (old_changed: bool) (new_changed: bool)
    (new_graph) (old_analyzed) (flist)
    (old_fragment) (new_fragment) =
  let new_analyzed = if new_changed then None else old_analyzed in
  Stack.push (new_graph, new_analyzed) results_stack;
  (
    (old_changed || new_changed),
    if new_changed
    then Utils.add_opt_to_list new_fragment flist
    else old_fragment::flist
  )


(** Process deletion_list until a fix point is reached *)
let process_deletion_list (deletion_list: F.fragment list)
    doms (allow_fork: fork_fun)
  : (bool * F.fragment list) =
  Debug.print_patch_debug "Processing deletion list";
  let no_fork = (fun _ _ -> false) in
  let current_fork = ref allow_fork in
  (* Do passes until fix point is reached *)
  let rec iter_until_stable (is_globally_changed, new_list) =
    (* Do a single pass on the list *)
    let is_locally_changed, new_list = List.fold_left (
        fun (is_changed, new_list) fragment ->
          let old_graph, analyzed = Stack.pop results_stack in
          try
            let changed, new_graph, new_fragment =
              add_fragment fragment old_graph doms !current_fork false
            in
            commit_if_changed is_changed changed new_graph analyzed
              new_list fragment new_fragment
          with NeedFork(indices) ->
            (* Disable fork until forked fragment have been inserted *)
            current_fork := no_fork;
            let result_graph, to_queue = fork_graph indices old_graph in
            Queue.add to_queue forked_queue;
            let changed, new_graph, new_fragment =
              add_fragment fragment result_graph doms !current_fork false
            in
            if changed then current_fork := allow_fork;
            commit_if_changed is_changed changed new_graph analyzed
              new_list fragment new_fragment
      ) (false, []) new_list
    in
    let new_list = List.rev new_list in
    if is_locally_changed
    then iter_until_stable (true, new_list)
    else (is_globally_changed, new_list)
  in
  iter_until_stable (false, deletion_list)


(** Process context_list until a single compatible fragment is found *)
let process_context_list (context_list: F.fragment list)
    doms (allow_fork: fork_fun)
    (validation: result_graph -> bool * SPC.analyzed_set)
  : (bool * F.fragment list) =
  (* TODO: use allow_fork *)
  Debug.print_patch_debug "Processing context list";
  let result_graph, analyzed = Stack.pop results_stack in
  let rec iter_until_found is_found result_graph new_list old_analysis = function
    | [] -> (is_found, result_graph, List.rev(new_list), old_analysis)
    | fragment::tail ->
      if is_found
      then iter_until_found true result_graph (fragment::new_list) old_analysis tail
      else begin
        let old_graph =
          {result_graph with graph=(Oo.copy result_graph.graph)}
        in
        let is_compatible, new_graph, new_fragment =
          add_fragment fragment result_graph doms allow_fork false
        in
        if is_compatible
        then
          let is_valid, analysis = validation new_graph in
          if is_valid
          then iter_until_found true new_graph
              (Utils.add_opt_to_list new_fragment new_list) (Some analysis) tail
          else iter_until_found false old_graph
              (fragment::new_list) old_analysis tail
        else iter_until_found false old_graph
            (fragment::new_list) old_analysis tail
      end
  in
  let is_found, new_graph, new_list, new_analyzed =
    iter_until_found false result_graph [] analyzed context_list
  in
  Stack.push (new_graph, new_analyzed) results_stack;
  (is_found, new_list)


let process_addition_list (addition_list: F.fragment list)
    doms csets (allow_fork: fork_fun)
  : (bool * F.fragment list) =
  Debug.print_patch_debug "Processing addition list";
  (* Do passes until fix point is reached *)
  let no_fork = (fun _ _ -> false) in
  let current_fork = ref allow_fork in
  let rec iter_until_stable (is_globally_changed, new_list) =
    (* Do a single pass on the list *)
    let is_locally_changed, new_list = List.fold_left (
        fun (is_changed, new_list) fragment ->
          let old_graph, analyzed = Stack.pop results_stack in
          try
            let changed, new_graph, new_fragment =
              add_addition_fragment fragment old_graph
                doms csets !current_fork
            in
            commit_if_changed is_changed changed new_graph analyzed
              new_list fragment new_fragment
          with NeedFork(indices) ->
            (* Disable fork until forked fragment have been inserted *)
            current_fork := no_fork;
            let result_graph, to_queue = fork_graph indices old_graph in
            Queue.add to_queue forked_queue;
            let changed, new_graph, new_fragment =
              add_addition_fragment fragment result_graph
                doms csets !current_fork
            in
            if changed then current_fork := allow_fork;
            commit_if_changed is_changed changed new_graph analyzed
              new_list fragment new_fragment
      ) (false, []) new_list
    in
    let new_list = List.rev new_list in
    if is_locally_changed
    then iter_until_stable (true, new_list)
    else (is_globally_changed, new_list)
  in
  iter_until_stable (false, addition_list)

let do_metavar_split (allow_fork: fork_fun) (code: Code.t) : bool =
  Debug.print_patch_debug "Looking for metavar to split";
  let result, analyzed = Stack.pop results_stack in
  let nodes = result.graph#nodes in
  let compatible = calc_comp_metavars result code in
  let respecialize_all () =
    CFC.KeyMap.iter (fun key node ->
        match node with
        | Deletion({content; cfg_nodes})
        | Context({content; cfg_nodes})
        | Addition({content; cfg_nodes}) ->
          let examples_ids =
            arrays_to_ids_list result.graph_ids cfg_nodes
          in
          begin
            match content with
            | F.Pattern(_) ->
              let new_pattern =
                AA.pattern_from_ids examples_ids code true
              in
              let new_node = match node with
                | Deletion(a) ->
                  Deletion({a with content=F.Pattern(new_pattern)})
                | Context(a) ->
                  Context({a with content=F.Pattern(new_pattern)})
                | Addition(a) ->
                  Addition({a with content=F.Pattern(new_pattern)})
                | _ -> assert false
              in
              result.graph#replace_node (key, new_node)
            | _ -> ()
          end
        | _ -> ()
      ) nodes;
    Stack.push (result, None) results_stack;
    false (* Don't retry after this step, if true can cause infinite loops *)
  in

  List.fold_left
    (fun prev compatible ->
      Debug.print_patch_debug
        ("Compatibility is " ^ string_of_compatible_nodes compatible);
      prev ||
      match compatible with
      | Partial(indices) ->
          let gids = (Array.of_list (Utils.assoc_left indices)) in
          if allow_fork gids result
          then begin
            let new_result, to_queue = fork_graph gids result in
            Queue.add to_queue forked_queue;
            Stack.push (new_result, None) results_stack;
            true
          end
          else
            respecialize_all ()
      | Empty | Full _ -> respecialize_all ()
      (* Full can come after a split but with different kind of metavar *)
      | _ ->
          Stack.push (result, analyzed) results_stack;
          false)
    false compatible



let check_metavars (graph: result_graph) : bool =
  let metavars_del_con = ref [] in
  let metavars_add = ref [] in
  CFC.KeyMap.iter (fun key node ->
      match node with
      | Deletion({content; cfg_nodes})
      | Context({content; cfg_nodes}) ->
        begin
          let examples = arrays_to_ids_list graph.graph_ids cfg_nodes in
          match content with
          | F.Pattern(pattern) ->
            List.iter(fun (_, m) ->
                let sm = Craft_metavar.shrink_metavar m examples in
                let (_,metavar) = List.split(AA.Metavariable.elements sm) in
                if not (List.mem metavar !metavars_del_con) then
                  metavars_del_con := metavar :: !metavars_del_con
              ) pattern.AA.metavars;
          | _ -> ()
        end
      | Addition({content; cfg_nodes}) ->
        begin
          let examples = arrays_to_ids_list graph.graph_ids cfg_nodes in
          match content with
          | F.Pattern(pattern) ->
            List.iter(fun (_, m) ->
                let sm = Craft_metavar.shrink_metavar m examples in
                let (_,metavar) = List.split(AA.Metavariable.elements sm) in
                if not (List.mem metavar !metavars_add) then
                  metavars_add := metavar :: !metavars_add
              ) pattern.AA.metavars;
          | _ -> ()
        end
      | _ -> ()
    )graph.graph#nodes;
  List.for_all(fun m -> List.mem m !metavars_del_con) !metavars_add


let validate_modification (old_fp: int option) (code: Code.t)
        (result_graph: result_graph)
        : (bool * SPC.analyzed_set) =

    let result_w_dots = add_dots result_graph code in
    let safety, analysis = if check_metavars result_w_dots
    then
        SPC.test_semantic_patch_safety (get_semantic_patch result_w_dots code)
            (!Global.specfile_test)
    else
        SPC.UnboundAdded, SPC.empty_analyzed_set
    in
    print_safety safety;
    Debug.print_patch_debug
        ("Current patch:\n" ^ (get_semantic_patch result_w_dots code));
    match (safety, old_fp) with
    | (SPC.Safe, _) -> true, analysis
    | (SPC.Unsafe(new_fp), Some(old_fp)) when old_fp > new_fp -> true, analysis
    | (SPC.Unsafe(new_fp), None) -> true, analysis
    | (SPC.UnboundAdded, None) -> true, analysis
    | _ ->
        Debug.print_patch_debug "Fragment rejected: does not improve safety";
        (false, analysis)

type 'a check =
  | Ok of 'a
  | Retried of 'a

let construct_graphs (deleted, context, added) doms csets (code: Code.t)
  : tri_fragments * results =
  Stack.clear results_stack;

  let return (value: tri_fragments)
    : tri_fragments check =
    Ok(value)
  in
  let apply_retry (value: tri_fragments) : tri_fragments check =
    Retried(value)
  in
  let ( >>= ) (value: tri_fragments check)
      (f: tri_fragments -> tri_fragments check)
    : tri_fragments check =
    match value with
    | Ok(a) -> f a
    | Retried(value) -> Retried(value)
  in

  let always_fork = (fun _ _ -> true) in
  let never_fork = (fun _ _ -> false) in

  (* Putting fragments matching the most nodes in head *)
  let deleted, context, added = (
    sort_alist_by_matched_nodes deleted,
    sort_alist_by_matched_nodes context,
    sort_alist_by_matched_nodes added
  )
  in
  let before_doms, after_doms = doms in
  let deleted, context =
    if (List.length deleted) > 0
    then begin
      Queue.add
        (create_initial_graph (List.hd deleted) before_doms)
        forked_queue;
      ((List.tl deleted), context)
    end
    else begin
      Queue.add
        (create_initial_graph (List.hd context) before_doms)
        forked_queue;
      (deleted, (List.tl context))
    end
  in

  let rec iter (fragments: tri_fragments) =
    let retry_on_change changed fragments =
      if changed
      then iter fragments >>= apply_retry
      else return fragments
    in

    let process_del split (del, con, add) =
      let _, del =
        process_deletion_list del before_doms split
      in
      (* Don't need to retry after this step *)
      retry_on_change false (del, con, add)
    in

    let process_con split (del, con, add) =
      Debug.print_patch_debug "Skipping context";
(*       retry_on_change false (del, con, add) *)
      let old_safety =
        let result_wo_dots, analyzed = Stack.pop results_stack in
        match analyzed with
        | Some(analysis) ->
          Stack.push (result_wo_dots, analyzed) results_stack;
          SPC.check_safety (SPC.set_to_int_result SPC.NodeSet.cardinal analysis)
        | None ->
          let result_w_dots = add_dots result_wo_dots code in
          if check_metavars result_w_dots
          then begin
              let safety, analysis =
                  SPC.test_semantic_patch_safety
                      (get_semantic_patch result_w_dots code)
                      (!Global.specfile_test)
              in
              Stack.push (result_wo_dots, Some(analysis)) results_stack;
              if safety = SPC.Unknown
              then begin
                let graph_name =
                  "patch_error_" ^ string_of_int !Global.error_patch_counter ^ ".gv"
                in
                incr Global.error_patch_counter;
                Debug.exec_if_graphs_dir (fun s ->
                  Debug.print_patch_debug
                    (Printf.sprintf "Invalid patch, graph available at %s" graph_name);
                  (print_result_graph result_w_dots s)
                ) graph_name;
              end;
              safety
          end
          else begin
              Stack.push (result_wo_dots, None) results_stack;
              SPC.UnboundAdded
          end
      in
      match old_safety with
      | SPC.UnboundAdded ->
        let changed, con =
          process_context_list con before_doms split
            (validate_modification None code)
        in
        retry_on_change changed (del, con, add)
      | SPC.Unsafe(old_false_positive) ->
        Debug.print_patch_debug
          (Printf.sprintf "Unsafe patch: %d" old_false_positive);
        let changed, con =
          process_context_list con before_doms split
            (validate_modification (Some(old_false_positive)) code)
        in
        retry_on_change changed (del, con, add)
      | SPC.Safe ->
        Debug.print_patch_debug "Safe patch";
        retry_on_change false (del, con, add)
      | SPC.Unknown ->
        Debug.print_patch_debug "Error in patch";
        retry_on_change false (del, con, add)
    in

    let process_meta split fragments =
      let old_safety =
        let result_wo_dots, analyzed = Stack.pop results_stack in
        match analyzed with
        | Some(analysis) ->
          Stack.push (result_wo_dots, analyzed) results_stack;
          SPC.check_safety (SPC.set_to_int_result SPC.NodeSet.cardinal analysis)
        | None ->
          let result_w_dots = add_dots result_wo_dots code in
          if check_metavars result_w_dots
          then begin
              let safety, analysis =
                  SPC.test_semantic_patch_safety
                      (get_semantic_patch result_w_dots code)
                      (!Global.specfile_test)
              in
              print_safety safety;
              Debug.print_patch_debug
                  ("Current patch:\n" ^ (get_semantic_patch result_w_dots code));
              Stack.push (result_wo_dots, Some(analysis)) results_stack;
              safety
          end
          else begin
              print_safety SPC.UnboundAdded;
              Debug.print_patch_debug
                  ("Current patch:\n" ^ (get_semantic_patch result_w_dots code));
              Stack.push (result_wo_dots, None) results_stack;
              SPC.UnboundAdded
          end
      in
      match old_safety with
      | SPC.UnboundAdded ->
        let changed = do_metavar_split split code in
        retry_on_change changed fragments
      | _ -> retry_on_change false fragments
    in

    let process_add split (del, con, add) =
      let changed, add =
        process_addition_list add (before_doms, after_doms) csets split
      in
      retry_on_change changed (del, con, add)
    in

    match fragments with
    (*
    | ([], _, []) -> process_con never_fork fragments
    *)
    | (deleted, context, added) ->
      (* The passes of processing can be defined in an imperative way.
       * This monad ensure that further functions are not executed after a
       * recursive call.
      *)
      return fragments >>=
      (process_del never_fork) >>=
      (process_del always_fork) >>=
      (process_con never_fork) >>=
      (process_meta always_fork) >>=
      (process_add never_fork) >>=
      (process_add always_fork)
  in

  let rec empty_queue tri_fragments : (tri_fragments * results) =
    if Queue.is_empty forked_queue
    then
      (tri_fragments, Utils.stack_to_list results_stack)
    else begin
      Debug.print_patch_debug
        "\n\n---------- Processing graph ----------\n";
      Stack.push ((Queue.take forked_queue), None) results_stack;
      match iter tri_fragments with
      | Ok(value)
      | Retried(value) -> empty_queue value
    end
  in
  empty_queue (deleted, context, added)
