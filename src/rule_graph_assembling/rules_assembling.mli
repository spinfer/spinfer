module G = Control_flow_c.G
module CFC = Control_flow_c
module SPC = Semantic_patch_coccinelle
module F = Fragment
module AA = Abstract_ast
open Rule_graph


type results = (result_graph * SPC.analyzed_set option) list
type fragments = F.fragment list
type tri_fragments = fragments * fragments * fragments

type indices = int array

type compatible_nodes =
  | Full of int list
  | Partial of (int * int) list
  | Any
  | Empty

type fork_fun = indices -> result_graph -> bool

exception NeedFork of indices

type relationship = (Code.cfg_node_id * Graph.NodeIdSet.t) list
type dominators = relationship * relationship
type context_sets = relationship * relationship

val add_dots: result_graph -> Code.t -> result_graph


val sort_alist_by_matched_nodes : fragments -> fragments
val construct_graphs: tri_fragments -> dominators * dominators -> context_sets
  -> Code.t -> tri_fragments * results
