module F = Fragment
module CFC = Control_flow_c
module AA = Abstract_ast

(** Array of cfg graph ids *)
type concrete_graph_ids = int array

(** Array of cfg node ids *)
type concrete_node_ids = int array

(** Nodes linked to cfg nodes, that can be printed *)
type pattern_concrete_node = {
    cfg_nodes: concrete_node_ids;
    content: F.content;
}

(** Node of a pattern graph *)
type pattern_node =
    | Entry
    | NormalExit
    | ErrorExit
    | Context of pattern_concrete_node
    | Deletion of pattern_concrete_node
    | Addition of pattern_concrete_node
    | StartMerge of concrete_node_ids
    | EndMerge of concrete_node_ids
    | Dots
    | DeletedDots

let pattern2c = function
    | Entry -> "Entry"
    | NormalExit -> "NormalExit"
    | ErrorExit -> "ErrorExit"
    | Context _ -> "Context"
    | Deletion _ -> "Deletion"
    | Addition _ -> "Addition"
    | StartMerge _ -> "StartMerge"
    | EndMerge _ -> "EndMerge"
    | Dots -> "Dots"
    | DeletedDots -> "DeletedDots"

(** Pattern graph *)
type result_graph = {
    graph: pattern_node CFGDiff.CEG.ograph_mutable;
    graph_ids: concrete_graph_ids;
    mutable trivial_ids: int * int * int;
}

(* Take the `indices` on which to split `result` and return two fresh
 * graphs created from partition of the `result`.
 *)
let fork_graph (indices: int array) (result: result_graph)
        : (result_graph * result_graph) =
    let left_graph = Oo.copy result.graph in
    let right_graph = Oo.copy result.graph in

    (* FIXME: We need to sort indices, don't know why*)
    Array.sort compare indices;
    let restrict_graph_to indices graph : result_graph =
        let restrict (a: int array): int array =
            Array.map (Array.get a) indices
        in
        CFC.KeyMap.iter (fun key node ->
            let new_node = match node with
                | Entry | NormalExit | ErrorExit | Dots | DeletedDots -> node
                | Context(old) ->
                    Context({old with cfg_nodes=restrict old.cfg_nodes})
                | Addition(old) ->
                    Addition({old with cfg_nodes=restrict old.cfg_nodes})
                | Deletion(old) ->
                    Deletion({old with cfg_nodes=restrict old.cfg_nodes})
                | StartMerge(old) -> StartMerge(restrict old)
                | EndMerge(old) -> EndMerge(restrict old)
            in
            graph#replace_node (key, new_node)
        ) result.graph#nodes;
        {result with
            graph=graph;
            graph_ids=restrict result.graph_ids
        }
    in

    let left_result = restrict_graph_to indices left_graph in

    let right_indices = ref [] in
    let index_list = Array.to_list indices in
    Array.iteri (fun i _ ->
        if not(List.mem i index_list) then right_indices := i::!right_indices
    ) result.graph_ids;

    let right_result =
      restrict_graph_to (Array.of_list (List.rev !right_indices)) right_graph
    in

    (left_result, right_result)


let print_result_graph (result_graph: result_graph) (filename: string) : unit =
    let get_border_color = function
    | Entry | NormalExit | ErrorExit -> Some("dodgerblue4")
    | Addition(_) -> Some("green4")
    | Deletion(_) | DeletedDots -> Some("red4")
    | Context({content=F.Structure(F.Fake(_)); _}) ->
        Some("ivory4")
    | _ -> None
    in

    let get_color = function
    | Entry | NormalExit | ErrorExit -> Some("dodgerblue1")
    | Addition(_) -> Some("darkolivegreen2")
    | Deletion(_) | DeletedDots -> Some("brown1")
    | Context({content=F.Structure(F.Fake(_));_}) ->
        Some("ivory1")
    | _ -> None
    in

    let display_fun (_, node) =
        let str = match node with
        | Context({content; _})
        | Deletion({content; _})
        | Addition({content; _}) ->
            begin match content with
            | F.String(str) -> str
            | F.Pattern(pattern) -> Print4debug.print_ast_debug pattern
            | F.Structure(F.BlockStart) -> "{"
            | F.Structure(F.BlockEnd) -> "}"
            | F.Structure(F.Fake(str)) -> str
            end
        | Entry -> "Entry"
        | NormalExit -> "Exit"
        | ErrorExit -> "ErrorExit"
        | Dots | DeletedDots -> "..."
        | StartMerge(_) -> "Start merge node"
        | EndMerge(_) -> "End merge node"
        in
        let labels = match node with
        | Context({cfg_nodes; _})
        | Addition({cfg_nodes; _})
        | Deletion({cfg_nodes; _})
        | StartMerge(cfg_nodes)
        | EndMerge(cfg_nodes) ->
            Printf.sprintf " (%s)" (Dumper.dump (Array.to_list cfg_nodes))
        | _ -> ""
        in
        (str ^ labels, get_border_color node, get_color node)
    in

    let graph_label = Printf.sprintf "Graph ids: %s"
        (Dumper.dump (Array.to_list result_graph.graph_ids))
    in
    CFGDiff.CEG.print_ograph_mutable_generic result_graph.graph
        (Some(graph_label)) display_fun filename false


let dissociate_result_graph (result: result_graph) (added: bool)
        : result_graph =
    let old_graph = result.graph in
    Debug.exec_if_graphs_dir
        (print_result_graph result) "last_patch.gv";
    let entry, normal_exit, error_exit = result.trivial_ids in
    let new_graph = new CFGDiff.CEG.ograph_mutable in
    new_graph#add_nodei entry Entry;
    new_graph#add_nodei normal_exit NormalExit;
    new_graph#add_nodei error_exit ErrorExit;

    let rec iter_from key : unit =
        let node = CFC.KeyMap.find key old_graph#nodes in
        let succs = CFGDiff.KeyEdgeSet.filter (fun (succ, _) ->
            match node with
            | StartMerge _ ->
            let succ_node = CFC.KeyMap.find succ old_graph#nodes in
            begin match succ_node with
                | Deletion _ when not added -> true
                | Addition _ when added -> true
                | Context _ | StartMerge _ | EndMerge _
                | Entry | NormalExit | ErrorExit -> assert false
                | _ -> false
            end
            | _ -> true
        ) (old_graph#successors key)
        in
        CFGDiff.KeyEdgeSet.iter (fun (succ, t) ->
            if CFC.KeyMap.mem succ new_graph#nodes
            then
            new_graph#add_arc ((key, succ), t)
            else begin
            let succ_node = CFC.KeyMap.find succ old_graph#nodes in
            new_graph#add_nodei succ succ_node;
            new_graph#add_arc ((key, succ), t);
            iter_from succ
            end
        ) succs
    in

    iter_from entry;
    Debug.exec_if_graphs_dir
        (print_result_graph {result with graph=new_graph}) "dissociate.gv";
    {result with graph=new_graph}



let compare_result_nodes (a_key, a_node) (b_key, b_node) : int =
    match (a_node, b_node) with
    | StartMerge(_), StartMerge(_) | StartMerge(_), EndMerge(_)
    | EndMerge(_), StartMerge(_) | EndMerge(_), EndMerge(_)
        -> Pervasives.compare a_key b_key
    | StartMerge(_), _ | EndMerge(_), _ -> 1
    | _, StartMerge(_) | _, EndMerge(_) -> -1
    | Addition(_), Addition(_) -> Pervasives.compare a_key b_key
    | Addition(_), _ -> 1
    | _, Addition(_) -> -1
    | _, _ -> Pervasives.compare a_key b_key


let is_toplevel (content: F.content) : bool option =
    match content with
    | F.Pattern(pattern) -> Some(AA.rule_is_toplevel pattern.AA.ast)
    | _ -> None


let mix_nodes (before: pattern_node list) (after: pattern_node list)
        : pattern_node list =
    let is_subexpr = function
    | Deletion({content; _}) | Addition({content; _}) | Context({content; _}) ->
        is_toplevel content = Some(false)
    | _ -> false
    in
    let indexed_before = List.mapi (fun i n -> (i, n)) before in
    let indexed_after = List.mapi (fun i n -> (i, n)) after in
    let subexpr_before =
        List.filter (fun (_, n) -> is_subexpr n) indexed_before
    in
    let subexpr_after =
        List.filter (fun (_, n) -> is_subexpr n) indexed_after
    in
    let association =
        Utils.bilist_pairs (fun _ _ -> true) compare subexpr_before subexpr_after
    in
    let converted = List.map (fun (i, j) ->
        fst (List.nth subexpr_before i), fst (List.nth subexpr_after j)
    ) association
    in
    let rec mix i j before after assoc accu =
    match before, after with
    | [], [] -> List.rev accu
    | head::tail, [] -> mix (i+1) j tail [] assoc (head::accu)
    | [], head::tail -> mix i (j+1) [] tail assoc (head::accu)
    | b_head::b_tail, a_head::a_tail ->
        begin match assoc with
        | [] -> mix (i+1) j b_tail after [] (b_head::accu)
        | (i_max, j_max)::assoc_tail ->
            if i > i_max && j > j_max
            then mix i j before after assoc_tail accu
            else if i > i_max
            then mix i (j+1) before a_tail assoc (a_head::accu)
            else mix (i+1) j b_tail after assoc (b_head::accu)
        end
    in
    if List.length association = List.length subexpr_after
    then mix 0 0 before after converted []
    else before @ after

type brace_info = {
    del_brace: pattern_node option;
    del_ab: (pattern_node list);
    add_bb: (pattern_node list);
}

let empty_info = {del_brace = None; del_ab = []; add_bb = []}

type brace_mode =
    | NoBrace
    | AccuDel
    | AccuAdd

let is_opening_brace (node: pattern_concrete_node) : bool =
    match node.content with
    | F.Structure(F.BlockStart) -> true
    | _ -> false

let is_closing_brace (node: pattern_concrete_node) : bool =
    match node.content with
    | F.Structure(F.BlockEnd) -> true
    | _ -> false


(* We want to transform:
-A  -{  -B  +C  +{  +D
into:
-A  +C   {  -B  +D
*)
let rec merge_braces (nodes: pattern_node list) (f: pattern_concrete_node -> bool)
        : pattern_node list =
    let rec iter accu (info: brace_info) mode l =
    match mode, l with
    | NoBrace, [] -> List.rev accu
    | NoBrace, Deletion(n)::tail when f n ->
        let new_info = {info with del_brace = Some(Deletion(n))} in
        iter accu new_info AccuDel tail
    | NoBrace, head::tail -> iter (head::accu) info NoBrace tail
    | AccuDel, (Deletion(_) as n)::tail
    | AccuDel, (DeletedDots as n)::tail ->
        let new_info = {info with del_ab = n::info.del_ab} in
        iter accu new_info AccuDel tail
    | AccuDel, Addition(_)::tail ->
        iter accu info AccuAdd l
    (* Rollback *)
    | AccuDel, _ ->
        iter (info.del_ab@Utils.option_get info.del_brace::accu) empty_info NoBrace l
    (* Success *)
    | AccuAdd, Addition(n)::tail when f n ->
        let new_brace = match Utils.option_get info.del_brace with
        | Deletion(n) when f n -> Context(n)
        | _ -> assert false
        in
        let new_l = (List.rev (info.del_ab@new_brace::info.add_bb@accu))@tail in
        merge_braces new_l f
    | AccuAdd, (Addition(_) as n)::tail ->
        let new_info = {info with add_bb = n::info.add_bb} in
        iter accu new_info AccuAdd tail
    (* Rollback *)
    | AccuAdd, _ ->
        iter (info.add_bb@info.del_ab@Utils.option_get info.del_brace::accu)
            empty_info NoBrace l
    in
    iter [] empty_info NoBrace nodes


let get_string_list metavars_tbl result_graph (code: Code.t)
        (nodes: pattern_node list) : string list =
    let node_prefix = function
    | Deletion(_)
    | DeletedDots -> "- "
    | Addition(_) -> "+ "
    | Dots
    | Context(_) -> "  "
    | _ -> ""
    in
    let gids = Array.to_list result_graph.graph_ids in
    let get_string content cfg_nodes =
        let examples_ids = List.combine gids (Array.to_list cfg_nodes) in
        begin match content with
        | F.String(str) -> str ^ "\n"
        | F.Pattern(pattern) ->
            Craft_metavar.print_pattern_ast metavars_tbl
            (Some(examples_ids)) pattern
            ^ "\n"
        | F.Structure(structure) ->
            begin match structure with
            | F.BlockStart -> "{\n"
            | F.BlockEnd -> "}\n"
            | F.Fake(_) -> ""
            end
        end
    in

    let rec iter accu = function
    | [] -> List.rev accu
    | Dots::[] -> iter ((node_prefix Dots ^ "... when any\n")::accu) []
    | Dots::Dots::tail -> iter accu (Dots::tail)
    | Dots::(Deletion({content=d_con; cfg_nodes=d_cfg}) as d)::
      (Addition({content=a_con; cfg_nodes=a_cfg}) as a)::tail
    | (Deletion({content=d_con; cfg_nodes=d_cfg}) as d)::
      (Addition({content=a_con; cfg_nodes=a_cfg}) as a)::tail
      when is_toplevel d_con = Some(false) && is_toplevel a_con = Some(false) ->
        let str =
            (if accu = [] then "" else (node_prefix Dots) ^ "...\n") ^
            (node_prefix d) ^ (get_string d_con d_cfg) ^
            (node_prefix a) ^ (get_string a_con a_cfg)
        in
        iter (str::accu) (Dots::tail)
    | node::tail ->
        let str_node = match node with
        | Context({content; cfg_nodes})
        | Deletion({content; cfg_nodes})
        | Addition({content; cfg_nodes}) ->
            let wrap content =
                if is_toplevel content = Some(false)
                then
                    (* FIXME: This is a bit hackish *)
                    match content with
                    | F.Pattern(p) ->
                        let examples_ids =
                            List.combine gids (Array.to_list cfg_nodes)
                        in
                        F.Pattern(AA.pattern_from_ids examples_ids code false)
                    | _ -> assert false
                else content
            in
            begin try
                get_string (wrap content) cfg_nodes
            with Failure _ ->
                Printf.sprintf
                    "(<+... %s ...+>);\n"
                    (String.trim (get_string content cfg_nodes))
            end
        | Dots
        | DeletedDots -> "...\n"
        | _ -> ""
        in
        iter ((node_prefix node ^ str_node)::accu) tail
    in
    let new_nodes =
        merge_braces (merge_braces nodes is_opening_brace) is_closing_brace
    in
    iter [] new_nodes


let get_semantic_patch (result_graph: result_graph) (code: Code.t) : string =
    let entry, _, _ = result_graph.trivial_ids in
    Debug.exec_if_graphs_dir
        (print_result_graph result_graph) "last_patch.gv";
    let nodes = result_graph.graph#nodes in

    let metavars_tbl = Hashtbl.create 20 in

    let rec inline_tree (tree: Graph.dom_tree) (accu: CFGDiff.CEG.key list)
            : (CFGDiff.CEG.key list) =
        tree.Graph.key::(List.fold_right inline_tree tree.Graph.children accu)
    in
    let rg_before = dissociate_result_graph result_graph false in
    let rg_after = dissociate_result_graph result_graph true in
    Debug.exec_if_graphs_dir
        (print_result_graph rg_before) "last_patch_before.gv";
    Debug.exec_if_graphs_dir
        (print_result_graph rg_after) "last_patch_after.gv";
    let dominator_tree_before =
        Graph.compute_dominator_tree rg_before.graph entry compare_result_nodes
    in
    let inlined_before = inline_tree dominator_tree_before [] in
    let dominator_tree_after =
        Graph.compute_dominator_tree rg_after.graph entry compare_result_nodes
    in
    let inlined_after = inline_tree dominator_tree_after [] in

    let rec group_by_merge inlined global local merge_indices =
        match inlined with
        | [] -> (List.rev ((List.rev local)::global), List.rev merge_indices)
        | head::tail ->
        let pattern = CFC.KeyMap.find head nodes in
        begin match pattern with
            | StartMerge _ ->
                group_by_merge tail ((List.rev local)::global) []
                    (head::merge_indices)
            | EndMerge _ ->
                group_by_merge tail ((List.rev local)::global) [] merge_indices
            | _ ->
                group_by_merge tail global (head::local) merge_indices
        end
    in

    let grouped_before, indices_before =
        group_by_merge inlined_before [] [] []
    in
    let grouped_after, indices_after =
        group_by_merge inlined_after [] [] []
    in
    assert (indices_before = indices_after);
    assert (List.length grouped_before = List.length grouped_after);

    let pattern_of ids = List.map (fun id ->
        CFC.KeyMap.find id nodes
    ) ids
    in
    let get_str_list = get_string_list metavars_tbl result_graph code in
    let filter_nodes (after: bool) = List.filter (function
    | Addition({content=F.Structure(F.Fake _); _})
    | Context({content=F.Structure(F.Fake _); _})
    | Deletion({content=F.Structure(F.Fake _); _}) -> false
    | Addition({content; cfg_nodes}) when after -> true
    | Context(_) | Deletion(_)
    | Dots | DeletedDots when not after -> true
    | _ -> false
    )
    in

    let split_at_context inlined =
        let rec do_split accu remaining =
        match remaining with
        | [] -> (List.rev accu, [])
        | pattern::tail ->
            match pattern with
            | Context _ | Dots -> (List.rev accu, remaining)
            | _ -> do_split (pattern::accu) tail
        in
        do_split [] inlined
    in

    let str_list = get_str_list (List.flatten (List.map2 (fun before after ->
        let before, after = pattern_of before, pattern_of after in
        let before_con, after_con = split_at_context before in
        mix_nodes (filter_nodes false before_con) (filter_nodes true after)
        @
        filter_nodes false after_con
        ) grouped_before grouped_after))
    in
    let str_body = String.concat "" str_list in

    (* Group metavariables by type for nice printing *)
    let grouped_metavariables = Hashtbl.create 10 in
    Hashtbl.iter (fun m (metaname, content) ->
        let content = if content = "" then "" else " = {" ^ content ^ "}" in
        let metatype = Craft_metavar.decl_metavar m in
        try
            (* We need a list because there is no function (except find_all) that
                group bindings for the same key together.
            *)
            let old_list = Hashtbl.find grouped_metavariables metatype in
            Hashtbl.replace grouped_metavariables metatype ((metaname ^ content)::old_list)
        with Not_found ->
            Hashtbl.add grouped_metavariables metatype [metaname ^ content]
    ) metavars_tbl;

    let str_metavariables = Hashtbl.fold (fun metatype l accu ->
        accu ^ (Printf.sprintf "%s %s;\n" metatype (String.concat ", " l))
        ) grouped_metavariables ""
    in

    (**Typedef**)

    let explore_ptree ((typedef_set, iterator_set): Utils.StringSet.t * Utils.StringSet.t)
            ast
            : (Utils.StringSet.t * Utils.StringSet.t) =
        let typedefs = Collect_special.collect_typedef ast in
        let iterators = Collect_special.collect_iterator ast in
        let open Utils.StringSet in
        (
            union (of_list typedefs) typedef_set,
            union (of_list iterators) iterator_set
        )
    in

    let typedef_set, iterator_set = List.fold_left (fun sets node ->
        match CFC.KeyMap.find node nodes with
        | Context({content; _})
        | Deletion({content; _})
        | Addition({content; _}) ->
            begin match content with
            | F.Pattern(pattern) -> explore_ptree sets pattern.AA.ast
            | _ -> sets
            end
        | _ -> sets
        ) (Utils.StringSet.empty, Utils.StringSet.empty) (inlined_before@inlined_after)
    in

    let str_typedefs = Utils.StringSet.fold (fun elt accu ->
        accu ^ "typedef " ^ elt ^ ";\n"
    ) typedef_set ""
    in
    let str_iterators = Utils.StringSet.fold (fun elt accu ->
        accu ^ "iterator name " ^ elt ^ ";\n"
    ) iterator_set ""
    in
    Printf.sprintf "@@\n%s%s%s@@\n%s"
        str_metavariables str_typedefs str_iterators str_body
