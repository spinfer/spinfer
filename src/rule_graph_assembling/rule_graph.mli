(** Array of cfg graph ids *)
type concrete_graph_ids = int array

(** Array of cfg node ids *)
type concrete_node_ids = int array

(** Nodes linked to cfg nodes, that can be printed *)
type pattern_concrete_node = {
    cfg_nodes: concrete_node_ids;
    content: Fragment.content;
}

(** Node of a pattern graph *)
type pattern_node =
    | Entry
    | NormalExit
    | ErrorExit
    | Context of pattern_concrete_node
    | Deletion of pattern_concrete_node
    | Addition of pattern_concrete_node
    | StartMerge of concrete_node_ids
    | EndMerge of concrete_node_ids
    | Dots
    | DeletedDots

val pattern2c : pattern_node -> string

type result_graph = {
    graph: pattern_node CFGDiff.CEG.ograph_mutable;
    graph_ids: concrete_graph_ids;
    mutable trivial_ids: int * int * int;
}

val dissociate_result_graph : result_graph -> bool -> result_graph

val fork_graph : int array -> result_graph -> result_graph * result_graph

val print_result_graph: result_graph -> string -> unit

val get_semantic_patch: result_graph -> Code.t -> string
