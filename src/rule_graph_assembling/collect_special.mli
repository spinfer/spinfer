module Ast = Ast_cocci
module V = Visitor_ast

val collect_typedef : Ast.rule_elem -> string list
val collect_iterator : Ast.rule_elem -> string list
