from __future__ import division
import re
import numpy as np
import matplotlib.pyplot as plt
file = open("invalid_rules.data")

pattern_rule= re.compile("^\=\=\= \.\/\w+\d+ of \.\/\w+")
pattern_file = re.compile("^#### File")
pattern_diff = re.compile("^ (\d+) files? changed, (\d+) insertions?\(\+\), (\d+) deletions?\(\-\)")
pattern_error = re.compile("^diff:")
line = file.readline()

results = []

#Line contains a rule header
while (pattern_rule.match(line)):
	line = file.readline()
	#Line contains a file header
	while (pattern_file.match(line)):
		line = file.readline()
		#Line contains a user diff header
		line = file.readline()
		#Line contains diff histogram information
		line = file.readline()
		#Line contains diff information
		m = pattern_diff.match(line)
		if (m):
			usr_add_del = { "add" : int(m.group(2)), "del" : int(m.group(3))}
		line = file.readline()
		#Line contains cocci diff header
		line = file.readline()
		if (pattern_error.match(line)):
			#Line contains error log, no file found
			cocci_add_del = None
			line = file.readline()
			#Line contains "0 files changed"
		else:
			#Line contains diff histogram information
			line = file.readline()
			#Line contains diff information
			m = pattern_diff.match(line)
			cocci_add_del = { "add" : int(m.group(2)), "del" : int(m.group(3))}
		results.append({ "usr" : usr_add_del, "cocci" : cocci_add_del})
		line = file.readline()
file.close()

files = len(results)
score_table = []
overshoot_files = 0
for result in results:
	score = (result['cocci']['add']/result['usr']['add'] + result['cocci']['del']/result['usr']['del'])/2*100
	if score > 150:
		overshoot_files +=1 
	else:
		score_table.append(score)

print "=== Patchparse statistics ==="
print "The score of a rule on a file is the percentage of difference in terms of additions and deletion between cocci and user diffs compared to the user diff. A file with a score of 100 means the inferred patch made the same number of deletions and additions as the user."
print "Over %d files, the mean score is %d and the median is %d. %d files not shown in the histogram have a score superior to 150, showing that the patch was maybe too general." % (files, np.mean(score_table),np.median(score_table),overshoot_files)
plt.hist(score_table,bins=40)
plt.title("Correctness of patchparse patches")
plt.xlabel("Percentage of user additions and deletion covered by patch")
plt.ylabel("Number of files")
plt.xlim(0,150)
plt.show()
