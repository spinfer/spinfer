let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let check = function
    0 -> ()
  | x -> failwith (Printf.sprintf "returned %d" x)

let months =
  [("Jan",(1,31));
    ("Feb",(2,28));
    ("Mar",(3,31));
    ("Apr",(4,30));
    ("May",(5,31));
    ("Jun",(6,30));
    ("Jul",(7,31));
    ("Aug",(8,31));
    ("Sep",(9,30));
    ("Oct",(10,31));
    ("Nov",(11,30));
    ("Dec",(12,31))]

let get_commit dir =
  let cmd =
    Printf.sprintf "cd %s; git log | grep ^commit | head -1" dir in
  match cmd_to_list cmd with
    [commit] ->
      (match Str.split (Str.regexp " ") commit with
	[_;commit] -> commit
      |	_ -> failwith "bad commit")
  | _ -> failwith "bad commit"

let commit_from_file file =
  let cmd =
    Printf.sprintf "grep ^commit %s | head -1" file in
  match cmd_to_list cmd with
    [commit] ->
      (match Str.split (Str.regexp " ") commit with
	[_;commit] -> Some commit
      |	_ -> failwith "bad commit")
  | [] -> None
  | _ -> failwith "bad commit"

let get_date dir =
  let cmd =
    Printf.sprintf "cd %s; git log | grep ^Date: | head -1" dir in
  match cmd_to_list cmd with
    [date] ->
      (match Str.split (Str.regexp "[ \t]+") date with
	_::_::month::day::time::year::_ ->
	  let day =
	    try int_of_string day
	    with Failure _ -> failwith ("bad day: "^day) in
	  let year =
	    try int_of_string year
	    with Failure _ -> failwith ("bad day: "^year) in
	  (month,day,year)
      |	_ -> failwith "bad date")
  | _ -> failwith "bad date"

let next_day (month,day,year) inc =
  (if inc > 28 then failwith "increment cannot be greater than 28");
  let rec matchfrom = function
      [] -> failwith ("bad month: "^month)
    | ((x,_) as h)::xs ->
	if x = month
	then (h,xs)
	else matchfrom xs in
  let ((mo,(_,lastday)),rest) = matchfrom months in
  if day + inc > lastday
  then
    match rest with
      [] ->
      (match months with
	(mo,_)::_ -> (mo,day + inc - lastday,year+1)
      |	_ -> failwith "impossible")
    | (mo,_)::_ -> (mo,day + inc - lastday,year)
  else (month,day+inc,year)

(* ------------------------------------------------------------------ *)

type date = Date of (string * int * int) | Commit of string

let geq (mo,day,yr) (nmo,nday,nyr) =
  (yr > nyr) ||
  (yr = nyr &&
   let m = fst (List.assoc mo months) in
   let nm = fst (List.assoc nmo months) in
   ((m > nm) ||
    (m = nm && day > nday)))

let get_files date start finish repo out increment restrict =
  let rec loop start_date start =
    let continue (mo,day,yr) ((nmo,nday,nyr) as next) =
      let pre_dest =
	Printf.sprintf "%s/%s_%d_%d_%s_%d_%d" out mo day yr nmo nday nyr in
      let dest = Printf.sprintf "%s/%s" (Sys.getcwd()) pre_dest in
      let cmd = Printf.sprintf "/bin/rm -rf %s; mkdir -p %s\n" dest dest in
      check (Sys.command cmd);
      let until =
	if next = finish
        then ""
	else Printf.sprintf "--until=\"%s %d %d\" " nmo nday nyr in
      let cmd =
	Printf.sprintf
	  "cd %s; git log -p --since=\"%s %d %d\" %s %s> %s/gitlog"
	  repo mo day yr until restrict dest in
      Printf.printf "%s\n" cmd; flush stdout;
      check (Sys.command cmd);
      if geq next finish
      then [pre_dest]
      else
	(match commit_from_file (dest^"/gitlog") with
	  None ->
	    pre_dest::loop next (Commit "no idea what the commit is used for")
	| Some last_commit ->
	    pre_dest::loop next (Commit last_commit)) in
    match start with
      Date date ->
	let next = next_day date increment in
	continue date next
    | Commit commit ->
	let next = next_day start_date increment in
	continue start_date next in
  loop date start

let run_patchparse patchparse repo linuxnext extraopts file =
  let url =
    if linuxnext
    then "https://git.kernel.org/cgit/linux/kernel/git/next/linux-next.git/commit/?id="
    else "https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=" in
  let options = "--min 10 --noev --noall --print-sp "^extraopts in
  let cmd =
    Printf.sprintf "%s --git %s/gitlog %s --out-dir %s --gitdir %s --giturl %s"
      patchparse file options file repo url in
  Printf.printf "%s\n" cmd; flush stdout;
  check (Sys.command cmd)

(* ------------------------------------------------------------------ *)

let start_date = ref ""
let end_date = ref ""
let increment = ref 1
let repository = ref "/home/julia/linux-next"
let out_dir = ref "output"
let update_command = ref "./update_linux-next"
let patchparse = ref "/home/denis/patchparse3/patchparse.opt"
let restrict = ref ""
let args = ref ""

let speclist = Arg.align
 ["--start", Arg.Set_string start_date, "  starting date";
  "--end", Arg.Set_string end_date, "  ending date";
  "--out", Arg.Set_string out_dir, "  output directory";
  "--inc", Arg.Set_int increment, "  date increment";
  "--update", Arg.Set_string update_command, "  update command";
  "--restrict", Arg.Set_string restrict, "  subdir to consider";
  "--args", Arg.Set_string args, "  other args for patchparse, must be quoted"]
  
let usage = "Usage: cocci_infer repository [--start date] [--end date], etc"

let anonymous str = repository := str

let parse_date s =
  (if not (String.length s = 6) then failwith "bad date argument");
  let month = int_of_string (String.sub s 0 2) in
  let day = int_of_string (String.sub s 2 2) in
  let year = int_of_string ("20" ^ (String.sub s 4 2)) in
  let mo =
    try fst (List.nth months (month - 1))
    with Failure _ -> failwith "bad month" in
  (mo,day,year)

let _ =
  Arg.parse speclist anonymous usage;
  check
    (Sys.command
      (Printf.sprintf "cd %s; git reset --hard\n" !repository));
  let commit = get_commit !repository in
  let date = get_date !repository in
  let cmd = Printf.sprintf "cd %s/..; %s" !repository !update_command in
  Printf.printf "%s\n" cmd;
  check (Sys.command cmd);
  let new_date = get_date !repository in
  let start =
    if !start_date = ""
    then Commit commit
    else Date (parse_date !start_date) in
  let finish =
    if !end_date = ""
    then new_date
    else parse_date !end_date in
  let files = get_files date start finish !repository !out_dir !increment
      !restrict in
  let linuxnext =
    try
      let _ = Str.search_forward (Str.regexp "linux-next") !repository 0 in
      true
    with Not_found -> false in
  List.iter (run_patchparse !patchparse !repository linuxnext !args) files
