#! /bin/bash

#The script takes as an argument a folder produced by the calendar program. It executes the redorunall rule of the makefile in each date interval folder, and stores for each rule and each file with diffstat the following diffs :
#- the diff between the original file and the file patched by the developer
#- the diff between the original file and the file patched by spatch given the rule found by patchparse
#The results are stored in a text file at location $logfile.
#A folder linux.git containing the source code of the linux kernel must be placed in the same folder as the script, and the script must be in the same folder as the analysed folder.

folder=$1
logfile="/home/denis/calendar/invalid_rules.csv"
> $logfile
difffile="diff"
cd $folder
for makefile in $(find . -name "allgitlog.make" | sort)
do
	echo "Directory: "$(dirname $makefile)
	cd $(dirname $makefile)
	mv ../../linux.git/ ./
 	make -f allgitlog.make -i iredorunall
	mv linux.git/ ../../
	for diff in $(find -name "index" | sort )
	do
		cd $(dirname $diff)
		echo "=== $(dirname $diff) of $(dirname $makefile)" | tee -a $logfile
		while read pair
		do
			original_file="$(a=($pair);echo ${a[0]})"
			after_user="$(a=($pair);echo ${a[1]})"
			after_cocci=${after_user%.res.c}.c.cocci_res
			diff -u -w $original_file $after_cocci > /dev/null
			if [ $? -eq 1 ]
			then
				echo "#### File $original_file" >> $logfile
				echo ">>> Diff between before and after by user" >> $logfile
				diff -u -w $pair | diffstat >> $logfile
				echo ">>> Diff between before and after by Coccinelle" >> $logfile
				diff -u -w $original_file $after_cocci 2>> $logfile | diffstat >> $logfile
			fi
		done < ../$diff
		cd ../
		folder_name=$(dirname $diff)
	done
	cd ../
done
