find_rules () {
	folder=$1
	logfile=$2
	echo "Date range;Number of rules found" > $logfile 
	sum=0
	occurences=0

	for file in $(find $folder -name "allgitlog.make") 
	do
   		count=$(grep -c '^rule[0-9]*\.out' $file)
    		echo "$(basename $(dirname $file));$count" >> $logfile
    		sum=$(($sum+$count))
    		occurences=$(($occurences+1))
	done
	echo "$sum;$(echo "$sum/$occurences" | bc -l)"
}
TIMEFORMAT=%U\;%S
logfile="analysis.dat"
	echo "Analysis of calendar" > $logfile
	for i in 28
	do
		echo "====Executing calendar for increment of $i days====" >> $logfile
		echo "Time of execution (user,system)" >> $logfile
		/usr/bin/time -a -f "%U;%S" -o $logfile ./calendar --start 040115 --end 063115 --out analysisC$i --inc $i --update 'cd linux.git; git pull' ./linux.git
		echo "Info on rules found (number, average per date increment)" >> $logfile
		info=$(find_rules analysisC$i analysisC$i/found_rules.csv)
		echo "$info" >> $logfile
	done
