\documentclass[dvipsnames]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{lmodern}
\usepackage{csquotes}
\usepackage{pifont}
\usepackage{graphicx}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\usepackage{tikz}
\usetikzlibrary{shadows,graphs}
\tikzstyle{source_code_before}=[draw, fill=red!10, text centered,drop shadow,text width=3cm]
\tikzstyle{source_code_after}=[draw, fill=green!10, text centered,drop shadow,text width=3cm]
\tikzstyle{cfg_before}=[draw, fill=red!30, text centered,drop shadow,text width=1.5cm]
\tikzstyle{cfg_after}=[draw, fill=green!30, text centered,drop shadow,text width=1.5cm]
\tikzstyle{merged_cfg}=[draw, fill=yellow!50, text centered,drop shadow]
\tikzstyle{cfg_node}=[draw, fill=yellow!30, text centered,drop shadow]
\tikzstyle{pattern}=[draw, fill=purple!30, text centered,drop shadow]
\tikzstyle{sequence}=[draw, fill=purple!40, text centered,drop shadow]
\tikzstyle{semantic_patch}=[draw, fill=purple!50, text centered,drop shadow]
\tikzstyle{legend_data}=[draw, fill=gray!10, text centered,drop shadow,align=center]
\tikzstyle{algorithm}=[draw, fill=blue!10, align = center, rounded corners]
\tikzstyle{algorithm_emph}=[draw, fill=cyan!50, align = center, rounded corners]
\tikzstyle{tool}=[draw, fill=blue!30, align = center, rounded corners]
\tikzstyle{tree_node}=[shape=rectangle, rounded corners,draw, align=center,fill=blue!20]
\usepackage{listings}
\lstdefinelanguage{SmPL}{
morekeywords = {...,X0,X1,X2,X3,X4,X5,X6,X7,X8,X9X10,X11,X12,X13,X14,X15,X16,
X17,X18,X19,X20,X21,X22,X23,X24,X25,X26,u$0,v$0},
otherkeywords = {...,<+,+>},
sensitive = false,
morecomment = [l][\color{ForestGreen}]{+\ },
morecomment = [l][\color{red}]{-\ },
morecomment = [l][\color{blue}]{@}
}
\lstset{
	keywordstyle=\bfseries\color{blue},
	commentstyle=\color{red},
	basicstyle=\small\ttfamily,
	numbers=left,
	numbersep=5pt,
    language=SmPL,
	numberstyle=\scriptsize,
    showstringspaces=false,
    frame=tb
}

\mode<presentation>
\usetheme{Pittsburgh}
\usecolortheme{seahorse}
\addtobeamertemplate{navigation symbols}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
	\hspace{7.25cm}
}{}


\title{Semantic patch inference}
\author[Denis \textsc{Merigoux}]{Denis \textsc{Merigoux}}
\institute[Polytechnique - LIP6]{École polytechnique - LIP6}
\date{Tuesday, July 12\textsuperscript{th} 2016}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\section*{Introduction}

\begin{frame}
	\frametitle{The Linux kernel code}
	\centering
	\includegraphics[width=\textwidth]{size}
	\begin{flushright}
		\tiny Chart from \cite{DBLP:journals/corr/Palix0SCML14}
	\end{flushright}
\end{frame}

\begin{frame}
	\frametitle{Kernel commit example}
	Excerpt from commit \href{https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=323de9efdf3e75d1dfb48003a52e59d6d9d4c7a5}{323de9ef}'s diff:
	\includegraphics[width=\textwidth]{git_diff}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What is Coccinelle?}
\begin{onlyenv}<1>
	\texttt{drivers/pinctrl/bcm/pinctrl-bcm2835.c}: excerpt of \texttt{diff} output of commit \href{https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=323de9efdf3e75d1dfb48003a52e59d6d9d4c7a5}{323de9ef} of the Linux kernel.
	\begin{lstlisting}
@@ -1036,9 +1036,9 @@
pc->pctl_dev = pinctrl_register
	(&bcm2835_pinctrl_desc, dev, pc);
if (!pc->pctl_dev) {
	gpiochip_remove(&pc->gpio_chip);
	return -EINVAL;
}
	\end{lstlisting}
\end{onlyenv}
\begin{onlyenv}<2>
	\texttt{drivers/pinctrl/bcm/pinctrl-bcm2835.c}: excerpt of \texttt{diff} output of commit \href{https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=323de9efdf3e75d1dfb48003a52e59d6d9d4c7a5}{323de9ef} of the Linux kernel.
	\begin{lstlisting}
@@ -1036,9 +1036,9 @@
pc->pctl_dev = pinctrl_register
	(&bcm2835_pinctrl_desc, dev, pc);
- if (!pc->pctl_dev) {
+ if (IS_ERR(pc->pctl_dev)) {
	gpiochip_remove(&pc->gpio_chip);
- 	return -EINVAL;
+ 	return PTR_ERR(pc->pctl_dev);
}
	\end{lstlisting}
\end{onlyenv}
\begin{onlyenv}<3>
	SmPL \cite{Padioleau:2007:SDL:1223344.1223430} syntax used by Coccinelle:
	\begin{lstlisting}
@@ expression X0,X1; @@
X1 = pinctrl_register(...)
... when != X1
- if(!X1)
+ if(IS_ERR(X1))
{
    ...
-   return X0;
+   return PTR_ERR(X1);
}
	\end{lstlisting}
\end{onlyenv}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What is a pattern?}
	Piece of code, possibly containing metavariables:
	\begin{itemize}
		\item \lstinline|X0=pinctrl_register(...)|
		\item \lstinline|if(!X1)|
		\item \lstinline|return X0;|
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What is a semantic patch?}
	\begin{lstlisting}
@@ expression X0,X1; @@
X1 = pinctrl_register(...)
... when != X1
- if(!X1)
+ if(IS_ERR(X1))
{
    ...
-   return X0;
+   return PTR_ERR(X1);
}
	\end{lstlisting}
		Sequence of patterns separated by \lstinline|...|
		\begin{itemize}
			\item \lstinline|...| means \enquote{for all execution paths}.
			\item Disjunctions in semantic patches: \lstinline?( x | y )?.
			\item Context code.
		\end{itemize}

\end{frame}

\section{Semantic patch inference}

\begin{frame}
	\frametitle{What is semantic patch inference?}
	\begin{tikzpicture}
		\node (sf1b) [source_code_before] {\texttt{file1\_before.c}};
		\path (sf1b)+(0,-0.85) node (sf1a) [source_code_after] {\texttt{file1\_after.c}};
		\path (sf1a)+(0,-1.25) node (sf2b) [source_code_before] {\texttt{file2\_before.c}};
		\path (sf2b)+(0,-0.85) node (sf2a) [source_code_after] {\texttt{file2\_after.c}};
		\path (sf1b)+(0,1.25) node [legend_data]{\textbf{Input}\\Source files};
		\uncover<2>{
		\path (sf1b)+(7,-1.5) node (sp) [semantic_patch] {$\{\mathsf{SP}_1,\mathsf{SP}_2,\ldots\}$};
		\path (sp)+(0,1.25) node [legend_data]{\textbf{Output}\\Semantic patches};

		\draw[-stealth,color=gray,line width=5pt] (sf1b)+(1.75,-1.5) -- (sp);
		\path (sf1b)+(3.5,-1) node {\Huge ?};
		}
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Applications of semantic patch inference}
	\begin{block}{Code transformation}
		\begin{itemize}
			\item Automatic crafting of semantic patches from examples.
			\item Be less demanding on developers.
			\item Analyse the contents of a commit.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Semantic patch inference: related work}
	\begin{block}{Spdiff \cite{Andersen:2012:SPI:2351676.2351753}}
		Uses SmPL, targets C code, underlying theory of atomic patches.
	\end{block}
	\begin{block}{LASE \cite{Meng:2013:LLA:2486788.2486855}}
		Targets Java code, empirical evaluation.
	\end{block}
	\pause
	\vspace{\fill}
	\begin{center}
		New tool inspired by both approaches:\\\pause
		\Huge Spinfer
	\end{center}
\end{frame}

\begin{frame}<1-6>[label=workflow]
	\frametitle{Spinfer's workflow: three phases}
	\centering
	\begin{tikzpicture}
		\node (input) [legend_data]{\textbf{Input}\\Source files};

		\onslide<2-5,7->{
		\path (input)+(0,-1.75) node (preprocessing) [algorithm] {\textbf{1. Code preprocessing}\\Parse the code and build a merged Control Flow Graph (CFG).\\\emph{Uses code clone detection.}};
		\path [draw,->] (input) -- (preprocessing);
		}

		\onslide<6>{
		\path (input)+(0,-1.75) node (preprocessing) [algorithm_emph] {\textbf{1. Code preprocessing}\\Parse the code and build a merged Control Flow Graph (CFG).\\\emph{Uses code clone detection.}};
		\path [draw,->] (input) -- (preprocessing);
		}

		\onslide<3-6,8->{
		\path (preprocessing)+(0,-2) node (mining) [algorithm] {\textbf{2. Pattern mining}\\Extract patterns from the input code.\\\emph{Uses common subtree extraction and code clone detection.}};
		\path [draw,->] (preprocessing) -- (mining);
		}

		\onslide<7>{
		\path (preprocessing)+(0,-2) node (mining) [algorithm_emph] {\textbf{2. Pattern mining}\\Extract patterns from the input code.\\\emph{Uses common subtree extraction and code clone detection.}};
		\path [draw,->] (preprocessing) -- (mining);
		}

		\onslide<4-7,9->{
		\path (mining)+(0,-1.75) node (crafting) [algorithm] {\textbf{3. Semantic patch crafting}\\Determine the order of the patterns to create a semantic patch.};
		\path [draw,->] (mining) -- (crafting);
		}

		\onslide<8>{
		\path (mining)+(0,-1.75) node (crafting) [algorithm_emph] {\textbf{3. Semantic patch crafting}\\Determine the order of the patterns to create a semantic patch.};
		\path [draw,->] (mining) -- (crafting);
		}

		\onslide<5->{
		\path (crafting)+(0,-1.5) node (output) [legend_data] {\textbf{Output}\\Semantic patches};
		\path [draw,->] (crafting) -- (output);
		}
	\end{tikzpicture}
\end{frame}

\section{Code pre-processing}

\begin{frame}
	\frametitle{Code preprocessing: building the merged CFG}
	\centering
	\begin{tikzpicture}
		\node (sf1b) [source_code_before] {\texttt{file1\_before.c}};
		\path (sf1b)+(0,-0.85) node (sf1a) [source_code_after] {\texttt{file1\_after.c}};
		\path (sf1a)+(0,-1.25) node (sf2b) [source_code_before] {\texttt{file2\_before.c}};
		\path (sf2b)+(0,-0.85) node (sf2a) [source_code_after] {\texttt{file2\_after.c}};
		\path (sf1b)+(0,1) node [legend_data]{Source files};

		\path (sf2a)+(2.4,-1.75) node (legendparsing) [algorithm] {\textbf{Code parsing}};
		\path (legendparsing)+(0,6) node (legendparsingendline) {};
		\path [draw, dotted,line width=0.2em] (legendparsing) -- (legendparsingendline);
		\path (sf1b)+(4,0) node (cfg1b) [cfg_before] {$\mathsf{CFG}_{1,\mathsf{b}}$};
		\path [draw,->] (sf1b) -- (cfg1b);
		\path (cfg1b)+(0,-0.85) node (cfg1a) [cfg_after] {$\mathsf{CFG}_{1,\mathsf{a}}$};
		\path [draw,->] (sf1a) -- (cfg1a);
		\path (cfg1a)+(0,-1.25) node (cfg2b) [cfg_before] {$\mathsf{CFG}_{2,\mathsf{b}}$};
		\path [draw,->] (sf2b) -- (cfg2b);
		\path (cfg2b)+(0,-0.85) node (cfg2a) [cfg_after] {$\mathsf{CFG}_{2,\mathsf{a}}$};
		\path [draw,->] (sf2a) -- (cfg2a);
		\path (cfg1b)+(0,1) node [legend_data]{CFGs};

		\onslide<2>{
		\path (cfg2a)+(2,-1.75) node (legendcfgmerging) [algorithm] {\textbf{CFG merging}};
		\path (legendcfgmerging)+(0,6) node (legendcfgmergingendline) {};
		\path [draw, dotted,line width=0.2em] (legendcfgmerging) -- (legendcfgmergingendline);
		\path (cfg1b)+(4,-0.4) node (mergedcfg1) [merged_cfg] {$\mathsf{CFG}_{1,m}$};
		\path [draw,->] (cfg1b) -- (mergedcfg1);
		\path [draw,->] (cfg1a) -- (mergedcfg1);
		\path (cfg2b)+(4,-0.4) node (mergedcfg2) [merged_cfg] {$\mathsf{CFG}_{2,m}$};
		\path [draw,->] (cfg2b) -- (mergedcfg2);
		\path [draw,->] (cfg2a) -- (mergedcfg2);
		\path (mergedcfg1)+(0,1.4) node (merged_skimmed_cfg) [legend_data] {Merged CFGs};
		}
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Code preprocessing: merged CFG}
	\hspace{\fill}
	\includegraphics[width=0.48\textwidth,height=\textwidth,keepaspectratio,trim=1cm 1cm 1cm 1cm]{sample_cfg.pdf}
	\hspace{\fill}
	\onslide<2>{
	\includegraphics[height=\textheight,width=0.48\textwidth,keepaspectratio,trim=2cm 2cm 2cm 2cm]{../graph_skimmed.pdf}
	}
\end{frame}

\againframe<7>{workflow}

\section{Pattern mining}

\begin{frame}
	\frametitle{Pattern mining}
	\begin{tikzpicture}
		\node (mergedcfg1) [merged_cfg] {$\mathsf{CFG}_{1,m}$};
		\path (mergedcfg1)+(0,-1.5) node (mergedcfg2) [merged_cfg] {$\mathsf{CFG}_{2,m}$};
		\path (mergedcfg1)+(0,1.5) node (merged_skimmed_cfg) [legend_data] {Merged CFGs};

		\path (mergedcfg1)+(3,-0.75) node (cfg_nodes) [cfg_node] {$\{n_1,n_2,\ldots\}$};
		\path (cfg_nodes)+(0,2.25) node [legend_data] {CFG nodes};
		\path [draw,->] (mergedcfg1) -- (cfg_nodes);
		\path [draw,->] (mergedcfg2) -- (cfg_nodes);

		\onslide<2>{
		\path (cfg_nodes)+(4,0) node (patterns) [pattern] {$\{p_1,p_2,\ldots\}$};
		\path (patterns)+(0,2.25) node [legend_data] {Patterns};
		\path [draw,->] (cfg_nodes) -- (patterns);

		\path (patterns)+(-2,-2.5) node (pattern_mining) [algorithm] {\textbf{Pattern mining}\\Uses MCES \cite{lozano2004maximum} and clone detection \cite{Jiang:2007:DSA:1248820.1248843}};
		\path (pattern_mining)+(0,5) node (pattern_mining_endline) {};
		\path [draw, dotted,line width=0.2em] (pattern_mining) -- (pattern_mining_endline);
		}
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Pattern mining: Maximum Common Embedded Subtree}
	\begin{onlyenv}<1>
		\begin{tikzpicture}[sibling distance=1.25cm,
			every node/.style =  {tree_node, font=\tiny}]
			\node (tree1) {\texttt{Binary}}
			child { node {\texttt{Constant}}
				child { node {\texttt{1}} } }
		child { node { \texttt{/}} }
			child { node {\texttt{Unary}}
		  	child { node {\texttt{DeRef}} }
		  	child { node {\texttt{Identifier}}
			child { node {\texttt{x}} } } };
			\path (tree1)+(0,1) node [draw=none,fill=none] {\lstinline[language=C]|1 / *x|};
		\end{tikzpicture}
		\hspace*{\fill}
		\begin{tikzpicture}[sibling distance=1.25cm,
			every node/.style = {tree_node, font=\tiny}]
			\node (tree2) {\texttt{Binary}}
		child { node {\texttt{Identifier}}
				child { node {\texttt{y}} } }
				child { node { \texttt{/}} }
				child { node {\texttt{Unary}}
		  	child { node {\texttt{DeRef}} }
		  	child { node {\texttt{Identifier}}
		child { node {\texttt{z}} } } };
		\path (tree2)+(0,1) node [draw=none,fill=none] {\lstinline[language=C]|y / *z|};
		\end{tikzpicture}
	\end{onlyenv}\begin{onlyenv}<2>
		\begin{tikzpicture}[sibling distance=1.25cm,
			every node/.style =  {tree_node, font=\tiny}]
			\node (tree1) [fill=red!20]  {\texttt{Binary}}
			child { node {\texttt{Constant}}
				child { node {\texttt{1}} } }
		child { node  [fill=red!20]  { \texttt{/}} }
			child { node  [fill=red!20] {\texttt{Unary}}
		  	child { node  [fill=red!20] {\texttt{DeRef}} }
		  	child { node  [fill=red!20] {\texttt{Identifier}}
			child { node {\texttt{x}} } } };
			\path (tree1)+(0,1) node [draw=none,fill=none] {\lstinline[language=C]|1 / *x|};
	\end{tikzpicture}
		\hspace*{\fill}
		\begin{tikzpicture}[sibling distance=1.25cm,
			every node/.style = {tree_node, font=\tiny}]
			\node (tree2) [fill=red!20]  {\texttt{Binary}}
				child { node {\texttt{Identifier}}
				child { node {\texttt{y}} } }
				child { node  [fill=red!20]  { \texttt{/}} }
				child { node  [fill=red!20] {\texttt{Unary}}
		  	child { node  [fill=red!20] {\texttt{DeRef}} }
		  	child { node  [fill=red!20] {\texttt{Identifier}}
		child { node {\texttt{z}} } } };
		\path (tree2)+(0,1) node [draw=none,fill=none] {\lstinline[language=C]|y / *z|};
		\end{tikzpicture}
	\end{onlyenv}\begin{onlyenv}<3>
	\hspace{\fill}
	\begin{tikzpicture}[sibling distance=2.25cm,
		every node/.style = tree_node]
		\node (tree1) [fill=red!20]  {\texttt{Binary}}
			child { node {\lstinline|X0|}}
			child { node  [fill=red!20]  { \texttt{/}} }
			child { node  [fill=red!20] {\texttt{Unary}}
		child { node  [fill=red!20] {\texttt{DeRef}} }
		child { node  [fill=red!20] {\texttt{Identifier}}
	child { node {\lstinline|X1|} } } };
	\path (tree2)+(0,1) node [draw=none,fill=none] {\lstinline|X0 / *X1|};
	\end{tikzpicture}
	\hspace{\fill}
	\end{onlyenv}
\end{frame}

\begin{frame}
	\frametitle{Pattern mining: pre-grouping the CFG nodes}
	Before searching for patterns in a CFG node set $\mathcal{N}$, clusterize $\mathcal{N}$.
	\begin{description}
		\item[Pros.] Reduces overall cost, extracted patterns less abstract.
		\item[Cons.] Extracted patterns less abstract.
	\end{description}
	\vspace{\fill}
	\begin{block}<2->{Nodes}
		\centering
		\begin{onlyenv}<2>
		~\\
		\begin{tikzpicture}
			\node (lt) [tree_node,fill=red!20] {\lstinline|if(!pct->pctldev)|};
			\path (lt)+(0,-1) node (lb) [tree_node,fill=red!20] {\lstinline|if(!npct->pctl)|};

			\path (lt)+(5,0) node (rt) [tree_node,fill=green!20] {\lstinline|return PTR_ERR(pct->ptcldev);|};
			\path (rt)+(0,-1) node (rb) [tree_node,fill=green!20] {\lstinline|return -EINVAL;|};
		\end{tikzpicture}
		\end{onlyenv}\begin{onlyenv}<3>
			~\\
			\begin{tikzpicture}
				\node (lt) [tree_node] {\lstinline|if(!pct->pctldev)|};
				\path (lt)+(0,-1) node (lb) [tree_node] {\lstinline|if(!npct->pctl)|};

				\path (lt)+(5,0) node (rt) [tree_node] {\lstinline|return PTR_ERR(pct->ptcldev);|};
				\path (rt)+(0,-1) node (rb) [tree_node] {\lstinline|return -EINVAL;|};
			\end{tikzpicture}
		\end{onlyenv}\begin{onlyenv}<4>
			~\\
			\begin{tikzpicture}
				\node (lt) [tree_node] {\lstinline|var1 = x->field1;|};
				\path (lt)+(0,-1) node (lb) [tree_node] {\lstinline|var2 = x->field2;|};

				\path (lt)+(5,0) node (rt) [tree_node] {\lstinline|while(x > bar(var1))|};
				\path (rt)+(0,-1) node (rb) [tree_node] {\lstinline|while(x > bar(var2))|};
			\end{tikzpicture}
		\end{onlyenv}\begin{onlyenv}<5>
			~\\
			\begin{tikzpicture}
				\node (lt) [tree_node,fill=red!20] {\lstinline|var1 = x->field1;|};
				\path (lt)+(0,-1) node (lb) [tree_node,fill=red!20] {\lstinline|var2 = x->field2;|};

				\path (lt)+(5,0) node (rt) [tree_node,fill=green!20] {\lstinline|while(x > bar(var1))|};
				\path (rt)+(0,-1) node (rb) [tree_node,fill=green!20] {\lstinline|while(x > bar(var2))|};
			\end{tikzpicture}
		\end{onlyenv}
	\end{block}

	\begin{block}<2->{Patterns extracted}
		\begin{onlyenv}<2>
			With grouping:
			\begin{center}
			\begin{tikzpicture}
				\node (lt) [tree_node,fill=red!20] {\lstinline|if(!X0->X1)|};
				\path (lt)+(5,0) node (lb) [tree_node,fill=green!20] {\lstinline|return X2;|};
			\end{tikzpicture}
		\end{center}
		\end{onlyenv}\begin{onlyenv}<3>
			Without grouping:
			\begin{center}
			\begin{tikzpicture}
				\node (lt) [tree_node] {\lstinline|X0|};
			\end{tikzpicture}
		\end{center}
		\end{onlyenv}\begin{onlyenv}<4>
			Without grouping:
			\begin{center}
			\begin{tikzpicture}
				\node (lt) [tree_node] {\lstinline|x|};
			\end{tikzpicture}
		\end{center}
	\end{onlyenv}\begin{onlyenv}<5>
			With grouping:
			\begin{center}
			\begin{tikzpicture}
				\node (lt) [tree_node,fill=red!20] {\lstinline|X0 = x->X1;|};
				\path (lt)+(5.5,0) node (lb) [tree_node,fill=green!20] {\lstinline|while(x > bar(X2))|};
			\end{tikzpicture}
		\end{center}
		\end{onlyenv}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Pattern mining algorithm}
	\begin{itemize}
		\item<1-> Set of CFG nodes $\mathcal{N}=\{n_1,n_2,\ldots\}$.
		\item<2-> Set of raw patterns: $$\mathcal{P}=\left\{\mathsf{MCES}(n_i,n_j)\middle|n_i,n_j\in\mathcal{N}, n_i\neq n_j\right\}$$
		\item<3-> Raw pattern that matches the maximal number of nodes: $$p_{\mathsf{max}} = \argmax_{p\in\mathcal{P}} \left|\left\{n\in\mathcal{N}\middle| p\text{ matches }n\right\}\right| $$
		\item<4-> Refine $p_\mathsf{max}$ with the matched nodes $\Rightarrow$ abstraction rules.
		\item<5-> Start again with $\mathcal{N}$ minus the nodes matched by the pattern.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Pattern mining: results with the current implementation}
	\only<1>{
	\includegraphics[width=\textwidth]{git_diff}
	}\begin{onlyenv}<2>
	Pattern extracted from the code removed by commit \href{https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=323de9efdf3e75d1dfb48003a52e59d6d9d4c7a5}{323de9ef} of the Linux kernel (execution time: 1 min 30 s for an unfavorable scenario).
	\begin{lstlisting}
Pattern matching 41 nodes: if (!X0->X1)
Disjunctions:
X1 =
    | pmx (1 match)
    | pinctrl (1 match)
    | pctrl_dev (1 match)
    | pctrl (4 matches)
    | pctldev (5 matches)
    | pctl_dev (6 matches)
    | pctl (20 matches)
    | pcdev (1 match)
    | ctrl (2 matches)
	\end{lstlisting}
\end{onlyenv}
\end{frame}

\againframe<8>{workflow}

\section{Semantic patch crafting}

\begin{frame}
	\frametitle{Patterns to semantic patches}
	\centering
	\begin{tikzpicture}
		\node (patterns) [pattern] {$\{p_1,p_2,\ldots\}$};
		\path (patterns)+(0,1) node [legend_data] {Patterns};
		\path (patterns)+(0,2) node (cfgs) [merged_cfg] {$\{\mathsf{CFG}_1,\mathsf{CFG}_2,\ldots\}$};
		\path (patterns)+(0,3) node [legend_data] {Merged CFGs};

		\onslide<2->{
		\path (patterns)+(4.5,1.5) node (sequences) [sequence] {$\{s_1,s_2,\ldots\}$};
		\path (sequences)+(0,1.5) node [legend_data] {Sequences\\of patterns};
		\path [draw,->] (patterns) -- (sequences);
		\path [draw,->] (cfgs) -- (sequences);
		\path (patterns)+(2.5,-1.5) node (growing) [algorithm] {\textbf{Growing sequences}\\\textbf{algorithm}};
		\path (growing)+(0,5) node (growing_endline) {};
		\path [draw, dotted,line width=0.2em] (growing) -- (growing_endline);
		}

		\onslide<3->{
		\path (sequences)+(3.5,0) node (semantic_patches) [semantic_patch] {$\{\mathsf{SP}_1,\mathsf{SP}_2,\ldots\}$};
		\path (semantic_patches)+(0,1.5) node [legend_data] {Semantic\\patches};
		\path [draw,->] (sequences) -- (semantic_patches);
		\path (sequences)+(1.5,-3) node (serialization) [algorithm] {\textbf{Serialization}};
		\path (serialization)+(0,5) node (serialization_endline) {};
		\path [draw, dotted,line width=0.2em] (serialization) -- (serialization_endline);
		}
	\end{tikzpicture}
\end{frame}

\begin{frame}
	\frametitle{Ideas for growing pattern sequences}
	Same working set of nodes of the CFG ($\mathcal{N}$).
	\begin{enumerate}
		\item<1-> Initialize sequence with a pattern of modified nodes.\\
		E.g.: \lstinline|if(X1)|
		\item<2-> Search for a new pattern in the non-matched nodes.\\
		E.g.: \lstinline|return PTR_ERR(X1);|
		\item<3-> Try to insert the new pattern anywhere in the sequence.\\
		E.g.: \lstinline|if(X1)...return PTR_ERR(X2);|
		\item<4-> If not possible, make the pattern more concrete.\\
		E.g.: \lstinline|if(X1)...return PTR_ERR(!X2->X3);|
	\end{enumerate}
\end{frame}

\againframe<5>{workflow}

\section*{Conclusion}

\begin{frame}
	\frametitle{Future possible improvements}
	\begin{itemize}
		\setlength\itemsep{0.5cm}
		\item<1-> Finish the implementation (a few weeks).
		\item<2-> Improve the pattern mining algorithm.
		\item<3-> Custom abstraction rules defined by the user in a configuration file.
		\item<4-> Handling nested patterns.
	\end{itemize}
\end{frame}

\begin{frame}
	\centering
	\Huge Questions
\end{frame}

\begin{frame}[allowframebreaks]
        \frametitle{References}
        \bibliographystyle{alpha}
        \bibliography{../spinfer.bib}
\end{frame}

\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
