# Installation (Manual)
## Pre-requisites

Spinfer itself only has the standard OCaml pre-requisites.
Nevertheless, it requires [Coccinelle](https://github.com/coccinelle/coccinelle) as a submodule to run.

To install this submodule, run:
```
git submodule update --init
```

You will need to install the following dependencies :
### General dependencies
```
sudo apt-get install build-essential bash
```
### Coccinelle dependencies
```
sudo apt-get install ocaml libpcre-ocaml autoconf automake
```

For recent Ocaml version (>= 4.06) you will also need the Ocaml Num library:
```
sudo apt-get install libnum-ocaml libnum-ocaml-dev
```

### Spinfer dependencies
Spinfer also has standard python3 package pre-requisites, namely scikit-learn
and scipy, and uses ocamlbuild for building the executable.
To install those dependencies on Ubuntu/Debian use:
```
sudo apt-get install ocamlbuild python3-numpy python3-sklearn python3-scipy
```

Please note that the most recent versions supported are python 3.6 with scikit-learn version 0.24.2.
Likewise, spinfer has been shown to compile with ocaml version 4.08.1.

## Installation

After completing the pre-requisites, simply enter:
```
bash build_dependencies.sh
make build
```


# Installation (Docker)

You can use Spinfer in docker containers by building Spinfer image with the
Dockerfile included in this repository.

To build an image named `spinfer:latest` simply execute:
```bash
cd /path/to/spinfer/sources
docker build -t spinfer:latest .
```

# Usage
## Index File
Spinfer needs an index listing the pairs of files needed for semantic patch inference.
Each line list a pair of files, the left member is the file before modification,
the right member is the same file after modification.
Members of the pairs are separated by a space character.

An example of such index file is given below:
```
# This is a comment
file1_before.c file1_after.c
file2_before.c file2_after.c
foo/file3_before.c foo/file3_after.c
# Absolute paths and paths containing ".." are not allowed in index file.
```

**It is recommended to place your index file in the same directory as the C file examples**

## Running Spinfer
After creating your index file use `cd` to move to the directory containing
your file examples and index file.


Running Spinfer will depend on the installation method. 
In the following sections we suppose that your index file is named
`index` and you want to create a semantic patch called
`sp_output.cocci`.
### If installed manually
Launch the following command:
```
./spinfer.native -f index -o sp_output.cocci
```

### If installed using Docker
Launch this command:
```
docker run --mount type=bind,src="$(pwd)",dst=/data -it spinfer:latest -f index -o sp_output.cocci
```

# Troubleshooting
## Compilation

If you find a similar error as this one:
> SANITIZE: a total of 1 file that should probably not be in your source tree has been found.

Please run the following script:
```bash
/path/to/spinfer/_build/sanitize.sh
```

Then resume the compilation with the following command:
```
make build
```
