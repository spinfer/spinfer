#!/bin/bash
set -ex


#Coccinelle
cd coccinelle/
./autogen
./configure --disable-pcre --disable-pcre-syntax
cd ../

make -C coccinelle opt
cp coccinelle/spatch.opt statics/spatch
cp coccinelle/standard.iso coccinelle/standard.h statics/

mkdir -p bundles/parmap
cp coccinelle/bundles/parmap/parmap.{cmxa,cmi,a,cma} bundles/parmap/
cp coccinelle/bundles/parmap/libparmap_stubs.a bundles/parmap/

mkdir -p bundles/stdcompat
cp coccinelle/bundles/stdcompat/stdcompat*.{cmxa,cmi,a,cma} bundles/stdcompat/
cp coccinelle/bundles/stdcompat/libstdcompat__stubs.a bundles/stdcompat/

make -C coccinelle distclean

