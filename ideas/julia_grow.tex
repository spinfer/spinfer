\documentclass{article}
\usepackage{fullpage}

\usepackage[all,color,line]{xy}

\usepackage{amssymb}
\usepackage{graphicx}

% Very convenient to add comments on the paper. Just set the boolean
% to false before sending the paper:
\usepackage{ifthen}
\newboolean{showcomments}
\setboolean{showcomments}{true}
%\setboolean{showcomments}{false}
\ifthenelse{\boolean{showcomments}}
{ \newcommand{\mynote}[2]{\textcolor{red}{
    \fbox{\bfseries\sffamily\scriptsize#1}
    {\small$\blacktriangleright$\textsf{\emph{#2}}$\blacktriangleleft$}}}}
{ \newcommand{\mynote}[2]{}}

\newcommand{\jl}[1]{\mynote{Julia}{#1}}
\newcommand{\va}[1]{\mynote{Van-Anh}{#1}}
\newcommand{\ls}[1]{\mynote{Lucas}{#1}}

\begin{document}

\section{Goal}

Given a set of directed {\em example graphs} indicating a set of changes, the goal
is to produce an ordered {\em result graph list} generalizing these changes.
Pretty-printing the \textit{result graph list} should produce a semantic patch
rule.

\section{Definitions}

A {\em graph} is a collection of nodes and directed edges representing the
control-flow graph of a C function.  A graph has a unique {\em entry node},
a unique {\em exit node}, for normal returns, and a unique {\em
  error exit node}, for error returns.  Every node in the graph is
reachable from the entry node.  From every node in the graph, either the
exit node or the error exit node or both is reachable.  The entry node has
no incoming edges.  The exit and error exit nodes have no outgoing edges.
In a result graph, an edge that connects to an entry node, an exit node, or
an error exist node is known as a {\em trivial edge}.

In the list of example graphs, each node is associated with an index that is
unique across the complete set of example graphs.  This index is a pair of
which the first element indicates the graph name and the second element
indicates the name of the node within the graph.

\newcommand{\gvec}[1]{\mbox{\it{vec}}({#1})}
\newcommand{\gref}[1]{\mbox{\it{ref}}({#1})}

Each \textit{result graph} is associated with a vector $v$ of some size $s$
that represents the names of \textit{examples graphs} the 
\textit{result graph} is abstracting.
As a consequence each element of $v$ is the name of an example graph.
Since similar changes can happen multiple time in a given \textit{result graph}
it is possible for $v$ to contain identical elements.
The vector associated with \textit{result graph} $Rg$ is named $\gvec{Rg}$.

Each node in the \textit{result graph} is likewise associated with a vector
$v$ of the same size $s$.
Each element of $v$ is a set of node names.
For a given \textit{result graph} node $n$, for any $0 \leq i < s$,
$\gvec{n}[i]$ is a set of nodes in $\gvec{Rg}[i]$.

\subsection{Domination}
A node $A$ {\em dominates} a node $B$ in a graph directed $G$ if every path 
from the entry node to $B$ goes through $A$.
A node $B$ {\em postdominates} a node $A$ in a directed graph $G$ if every path
from $A$ to an exit node goes through $B$.
%We need to generalize these concepts to sets of nodes.
%A set of nodes $A$ {\em dominates} a set of nodes $B$ in a graph $G$ if
%every path from the entry node to a node in $B$ goes through some node in
%$A$.
%A set of nodes $B$ {\em postdominates} a set of nodes $A$ in a graph $G$ if
%every path from a node in $A$ to an exit node goes through a node in $B$.

%% A node $B$ {\em exit
%%   postdominates} a node $A$ in a graph $G$ if every path from $A$ to the
%% exit node goes through $B$.  A node $B$ {\em error exit postdominates} a
%% node $A$ in a graph $G$ if every path from $A$ to the error exit node goes
%% through $B$.

\subsection{Graph pair}
As \textit{example graphs} indicate a set of changes it is useful to construct
them from source code changes.

From a source code change one can define, $G_-$ the Control Flow Graph (CFG) of
the source before the change and $G_+$ the CFC of the source after the change.
These graphs are regrouped into a graph pair named $G$.

\newcommand{\PRE}[1]{\mbox{\it{PRE}}({#1})}
\newcommand{\POST}[1]{\mbox{\it{POST}}({#1})}
\newcommand{\POSTexit}[1]{\mbox{\it{POST}}_{\mbox{\scriptsize{exit}}}({#1})}
\newcommand{\POSTerror}[1]{\mbox{\it{POST}}_{\mbox{\scriptsize{error}}}({#1})}

\newcommand{\CPRE}[1]{\mbox{\it{PRECSET}}({#1})}
\newcommand{\CPOST}[1]{\mbox{\it{POSTCSET}}({#1})}

$\PRE{A,B,G}$ holds if node $A$ dominates node $B$ in graph $G$.
$\POST{A,B,G}$ holds if node $B$ postdominates node $A$ in graph $G$.
%% $\POSTexit{A,B,G}$ holds if node $B$ exit postdominates node $A$ in graph
%% $G$.  $\POSTerror{A,B,G}$ holds if node $B$ error exit postdominates node
%% $A$ in graph $G$.


%There are two alternative definitions for $\PRE{A,B,G}$:
%\begin{itemize}
%\item $\PRE{A,B}$ holds if node $A$ dominates node $B$.
%\item $\PRE{A,B}$ holds if node $A$ dominates node $B$ when
%  considering only the set of nodes consisting of the union of $\{A,B\}$
%  with the set of nodes in the current result graph.
%\end{itemize}
%The first possibility has the advantage of being able to be computed once
%and for all.  The second possibility has the advantage of reflecting the
%behavior of Coccinelle.  The second possibility, however, has the
%disadvantage of having to be computed over and over, since the result graph
%is continually changing.  The first possibility is more restrictive than
%the second, and thus safe to use, if it is preferred.


\subsection{Semantic patches}
\subsubsection{Worklists}
There are two worklists composed of fragments (defined in the next section),
the {\em A-list} (forall) and the {\em E-list} (exists). 
The A-list contains fragments that some previous phase has identified as 
interesting.  It does not contain {\tt \ldots} nodes. 
The E-list contains the {\tt \ldots} nodes from all of the
graphs. Note that for an element of the E-list, the list of associated
indices always contains exactly one element.

The \emph{A-list} is then splitted into 3 parts depending on the nature
of the fragment: context fragment, code addition fragment,
code deletion fragment.

\newcommand{\fs}{\mathcal{L}}
Let $\fs$ be a set of before and after files, $A$ the \textit{A-list} generated 
from $\fs$, $E$ the corresponding \textit{E-list}, $Rg$ a result graph formed
by fragments of $A$ and $E$ and $Sp(Rg)$ the semantic patch produced
by the pretty printing of $Rg$.

\subsubsection{Safety}
$Sp(Rg)$ is said to be safe for $\fs$ if it does not produce any false positive
in regard to $\fs$, i.e. $Sp(Rg)$ applied to the before parts of $\fs$ produces
a subset of the changes between the before and after parts of $\fs$.


\subsection{Control Flow Simplification}
Let $G_A$ and $G_B$ be two graphs.
$G_A$ is said to be a contraction of $G_B$ if $G_A$ can be obtained from $G_B$
by a serie of edge contractions in which loops and multiples edges are removed.

Let $Rg_-$ be the subgraph of $Rg$ consisting only in deleted and context nodes.
Similarily $Rg_+$ is the subgraph consisting in added and context nodes.

$Rg$ is said to be a Control Flow Simplification (or CFS) of the graph pair
$(G_-, G_+$), if both $Rg_-$ and $Rg_+$ are contractions of $G_-$ and $G_+$
respectively.


\newcommand{\node}[1]{\mbox{\it{node}}({#1})}
\subsection{Fragment}
A {\em fragment} is a collection of one or more nodes that is considered as
a single unit for addition to a result graph. A fragment carries both 
information about the code it abstracts (such as variables and metavariables names)
and information about the topological modification it introduces (such as 
creating branches or loops).

A fragment consists in at least one subfragment alongside a list of suggested
edges.
\begin{itemize}
\item A subfragment is a weakly connected directed graph.
This graph represents an abstraction of some \textit{example graph} subgraphs.

Each subfragment $F$ is associated with a list $\gref{F}$ in which each element
is a pair with the first component being the name of an example graph and
the second component being a mapping of the nodes in the fragment to the nodes
in the example graph.
In this list, there can be more than one pair for a given example graph.  A
subfragment is associated with a collection of one or more {\em ports}, that
are furthermore classified as either {\em entry} or {\em exit} ports.  An
entry port is the potential destination of an incoming edge in the result
graph, and an exit port is the potential source of an outgoing edge in the
result graph.  
For a port $p$, $\node{p}$ is the node that contains $p$.
\item Suggested edges are a suggestion of connection between ports.
These edges can connect ports in the same or in different fragments.
They are topological hints about what the fragment is doing.
\end{itemize}

\subsubsection{Example}
Typically, a fragment contains only one sub-fragment, and that
is a single CFG node with no suggested edges.
The exception is for compound terms, such as an {\tt if}, {\tt for} loop, etc.
The fragment for an {\tt if} is shown below, and consists of two sub-fragments.

\begin{figure}[h]
\center
\includegraphics[width=0.5\textwidth]{if.eps}
\caption{An \texttt{if} fragment}
\end{figure}

In this figure squares represent entry and exit ports and dotted edges represent
suggested edges. 

\subsubsection{Fragment compatibility}
Let $F$ be a fragment and $Rg$ a result graph.
$F$ is said to be fully compatible with $Rg$, if and only if for all graph 
pairs $G$ in $\gvec{Rg}$, $Rg$ is a CFS of $G$ and $F$ can be added to $Rg$
so that the addition is also a CFS of $G$.
If the previous property holds for at least one graph pair, $F$ is said to be
partially compatible with $Rg$.

\subsubsection{Fragment properties}
In a context or a deletion fragment there is exactly one entry port and
one exit port that are not connected by suggested edges.
This property ensure that if we contract all edges in the graph formed by the 
subgraphs in the subfragments and connected by suggested edges, we get a simple
fragment composed of one node, one subfragment, one entry port and one exit port.
This property is not necessarily true for addition fragments.


\section{Algorithm}

The first step is to create the initial \textit{result graph}.
This initial graph consists of one deletion fragment (or one context fragment
if no deletion fragment is available), with its suggested edges.
The only remaining entry and exit ports are connected two special
nodes of the \textit{result graph}: the entry node and the exit node
respectively.

The algorithm then proceeds in four steps:
\begin{enumerate}
\item The first step processes deletion fragments of A-list
\item The second step processes context fragments of A-list
\item The third step processes addition fragments of A-list
\item The last step processes the E-list
\end{enumerate}

\subsection{First step: placement of deletion fragments}
\newcommand{\edges}{\mathcal{E}}
Let $\edges$ be the set of edges contained in $Rg$ before fragment insertion.
Let $F_{entry}$ be the set of entry ports of fragment $F$, and $F_{exit}$ the
set of exit ports.
For each fragment $F$ we are looking for $E \subset \edges, E \neq \emptyset$,
so that $\forall (src, dst) \in E$, $\exists (entry, exit) 
\in F_{entry} \times F_{exit}$ so that :
\begin{itemize}
    \item[] $\PRE{src, entry, Rg} \wedge \POST{src, entry, Rg}$
    \item[$\bigwedge$] $\PRE{exit, dst, Rg} \wedge \POST{exit, dst, Rg}$
    \item[$\bigwedge$] $\PRE{entry, exit, Rg} \wedge \POST{entry, exit, Rg}$
\end{itemize}
and $F$ is fully compatible with $Rg$.

If such a fragment exists then it is added to $Rg$ by inserting its nodes and
edges then replacing $Rg$ edges that are in $E$ by edges: $(src, entry)$ and
$(exit, dst)$.

We continue inserting fragments until a fixed point is reached.

\subsection{Second step: placement of context fragments}

We begin by checking whether $Sp(Rg)$ is safe.
If this is the case we skip this step and continue to the next one.

We then try to insert a fragment similarily to step one, except that we
want the fragment to reduce the number of false positive of $Sp(Rg)$ and
we stop after the first fragment found.

After such a fragment is found we go back to step one.


\subsection{Third step: placement of addition fragments}
We first try to add a addition fragment $F$ similarily to the first step.

If that does not work we look for existing nodes in $Rg$ so that
we can connect all entry ports of $F$ to existing nodes with the same 
$\CPRE{}$ and all exit ports to nodes with the same $\CPOST{}$.
If such a node is found we add it to the result graph by adding extra edges.

We continue inserting fragments until a fixed point is reached.

\subsection{Fourth step: placement of E-list fragments}

The process for E-list dots is the same as step one, except that:
\begin{itemize}
\item E-list fragments can never be attached to result graph entry and 
exit nodes
\item There only needs to exist an $i$ for which the connection is valid, it
does not need to be valid for all of them.
\end{itemize}
\end{document}
