From julia.lawall@lip6.fr Thu Nov  9 15:15:47 2017
Date: Thu, 9 Nov 2017 22:15:44 +0800 (CST)
From: Julia Lawall <julia.lawall@lip6.fr>
To: Van-Anh NGUYEN <vananhiph@gmail.com>, Lucas Serrano <lucas.serrano@inria.fr>
Subject: notes

May_9_2017_May_14_2017/rule1

The first rule is:


@@
@@
- seq_puts(s, "0") ;
+ seq_putc(s, '0') ;

This doesn't correspond to the evidence at all.  There are calls with the
scond argument as "0", but then the first argument is not s.  There are
calls with the first argument as s but then the second argument is not "0".

There is a similar situation for the second rule.

-------------------------

May_9_2017_May_14_2017/rule2

First rule:

@@
identifier X0 ;
type X1 ;
type X2 ;
identifier X3 ;
identifier X4 ;
identifier X5 ;
@@
-  void X5(struct drm_plane *X4, struct drm_crtc *crtc)
+  void X5(struct intel_plane *plane, struct intel_crtc *crtc)
{
...
- struct intel_plane *intel_plane = to_intel_plane(X4) ;
+ struct drm_i915_private *dev_priv = to_i915(plane->base.dev) ;
...
- X1 X0 = intel_plane->X3 ;
+ X2 X0 = plane->X3 ;
...
}

There are different values for X3, ie the field name.  It is normal that
they should have different types and that the types should change in
different ways.  There should be separate rules, or a disjunction.

-------------------------

May_9_2017_May_14_2017/rule3

The first rule is:

@@
identifier X0 ;
identifier X1 ;
@@
- X0->i_ctime = ubifs_current_time(X1) ;
+ X0->i_ctime = current_time(X1) ;

This is probably OK, but more general would be:

@@
expression X1;
@@

- ubifs_current_time
+ current_time
   (X1)


-------------------------

May_9_2017_May_14_2017/rule4

The first two rules don't apply because of the { S }, that should be S (EASY).

The first rule is:

@@
identifier X1 ;
identifier X2 ;
statement S ;
@@
- if (!X2)
{ S }
...
- X2 = vmalloc(X1) ;

It looks at minimum like it should be:

@@
identifier X1 ;
identifier X2 ;
@@
- if (!X2)
-    X2 = vmalloc(X1) ;

But the real rule seems to be more like:

@@
expression X1 ;
expression X2 ;
@@

- X2 = kmalloc(X1, GFP_KERNEL | __ GFP_NOWARN) ;
- if (!X2)
-    X2 = vmalloc(X1) ;
+ X2 = kvmalloc(X1) ;

It is possible that | __ GFP_NOWARN is not really necessary.

The situation for the second rule seems similar.

The third and fourth rules should be merged into the first and second
rules, respectively.

The sixth rule is incorrect

@@
identifier X1 ;
identifier X2 ;
@@
- X2 = kmalloc(X1, GFP_KERNEL | GFP_KERNEL) ;

GFP_KERNEL | GFP_KERNEL doesn't appear in the code and is not in the
evidence.  The evidence has GFP_KERNEL | __GFP_NOWARN.

The seventh rule has the same problem, with a double __GFP_NOWARN.

-------------------------

May_14_2017_May_19_2017/rule1

The first two rules could be merged to be:

@@
expression X0 ;
@@
- drm_free_large(X0) ;
+ kvfree(X0) ;

The ; is furthermore not needed, although it doesn't hurt.

The third rule is:

@@
expression X0 ;
identifier X1 ;
identifier X2 ;
@@
- X1 = drm_malloc_ab(X2, sizeof (X0)) ;
+ X1 = kvmalloc_array(X2, sizeof (X0), GFP_KERNEL) ;

X0 is indicated to be an exprssion, but in practice among the various
evidence, in many cases it is a type.  If there was a type case and if
additioally X1 were an expression, then the fourth rule would not be
needed.  If X2 were an expression, then

@@
expression X0 ;
identifier X1 ;
identifier X2 ;
identifier X3 ;
@@
- X2 = drm_malloc_ab(X1->X3, sizeof (X0)) ;
+ X2 = kvmalloc_array(X1->X3, sizeof (X0), GFP_KERNEL) ;

would not be needed.

-------------------------

May_14_2017_May_19_2017/rule2

The looks complex.  It is not clear from the examplea how the new last
argument should be constructed.

-------------------------

May_14_2017_May_19_2017/rule3

The second rule is:

@@
identifier X0 ;
identifier X1 ;
@@
- X0->X1.v64 = 0 ;
+ X0->X1 = 0 ;

This looks too generic.  Even though X0 and X1 map to different things in
the different cases, perhaps they all have the same type.  Or perhaps one
of a small set of types.  On the other hand, the = 0 part should probably
not be part of the rule.  There is (at least) one case with:

-       if (tp->rcv_rtt_est.time.v64 == 0)
+       if (tp->rcv_rtt_est.time == 0)

The rule:

@@
type X0 ;
type X1 ;
@@
- X1  ;
+ X0  ;

Makes no sense.  The problem is that we don't look into type declarations.
Probably this is not a priority for the moment.

The following rule has no content, except for the u32, which doesn't give
enough information:

@@
type X0 ;
type X1 ;
type X2 ;
type X3 ;
type X4 ;
type X5 ;
identifier X6 ;
identifier X7 ;
identifier X8 ;
type X9 ;
identifier X10 ;
identifier X11 ;
typedef u32 ;
@@
-  X9 X8(X0 X11, X3 X7, u32 X6, X2 X10)
+  X9 X8(X1 X11, X5 X7, u32 X6, X4 X10)
{
...
}

The following rule is pretty printed incorrectly:

@@
identifier X0 ;
type X1 ;
typedef u32 ;
@@
+ u32 seq, u32 snd_una ;
...
+ X1 X0 ;

u32 should not be repeated in front of snd_una.

This rule is furthermore odd, because it matches two overlapping pairs of
statements.  That is, the code is basically

+A
+B
+C

and then it is conaidering AB as one instance and AC as another.  There is
no - code, because the - code contains only two statements and thus can't
be doubly matched in the same way.

The rule

@@
identifier X0 ;
identifier X1 ;
identifier X2 ;
@@
+ X0->X1.X2 = 0 ;

is too generic.  It merges two things that have completely different field
names.  This should be covered by the second rule.

The following rule:

@@
identifier X0 ;
identifier X1 ;
identifier X2 ;
@@
+ X0 = tcp_stamp_us_delta(tp->tcp_mstamp, X2->X1) ;

should be:

@@
expression e1, e2;
@@

- skb_mstamp_us_delta(&e1, &e2)
+ tcp_stamp_us_delta(e1, e2)

The rule:

@@
identifier X0 ;
identifier X1 ;
identifier X2 ;
identifier X3 ;
@@
+ X2->X3 = X0(X1) ;

is too generic. It seems to relate to dropping the stamp_jiffies field from
some kind of structure.

-------------------------

May_14_2017_May_19_2017/rule4

The first four rules look like they could be merged into one, with an
arbitrary expression as the argument.

The fifth and sixth rules should be merged, with the name of the second
parameter as a metavariable.  The name of the first parameter could surely
be a metavariable too, even if that is not needed for the examples.  Note
that this will not make the rules work, but we can leave the choice of new
function names as low priority for the moment.

The rule:

@@
identifier X0 ;
identifier X1 ;
statement S ;
@@
+ X0 = tcf_block_get(&X1->block, &X1->filter_list) ;
...
+ if (X0)
{ S }

could at least be:

@@
identifier X0 ;
identifier X1 ;
statement S ;
@@
+ X0 = tcf_block_get(&X1->block, &X1->filter_list) ;
+ if (X0)
+ S

More study of the context would be required to see if there is a rule for
where this code should be added and what S should be.

The next two rules should have identifiers for the parameter names, like
the pair of rules discussed previously.

The rule:

@@ @@
+ return err ;

Should be added under the if (X0) in some cases for the rule described
above.  Similarly for the rule adding int err;.

-------------------------

May_14_2017_May_19_2017/rule5

The first two rules don't match anything.  When the last argument is false,
the second argument is fl, not filter, and when the last argument is true
the second argument is filter, not fl.  The last rule doesn't parse because
a while loop needs a body.

It looks like all the rules could be merged into one:

@@
@@
- tc_classify
+ tcf_classify
   (...)

-------------------------

May_14_2017_May_19_2017/rule6

The pretty printing for case lines seems to be completely incorrect.  It
should be case XXX: not case (XXX).  But it is not clear that much can be
done here because the changes are within names.

The following rule also involves changes within names, and doesn't seem
very promising.

@@
identifier X0 ;
identifier X1 ;
identifier X2 ;
@@
- X2 = X1 ;
+ hwcaps.mask |= X0 ;

The remaining problems have to do with the pretty printing of switch and
case.
