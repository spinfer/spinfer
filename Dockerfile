FROM debian:10-slim

WORKDIR /usr/src/spinfer
# For debian version > 10 these package will be required: libnum-ocaml libnum-ocaml-dev
RUN apt-get update \
&&  DEBIAN_FRONTEND=noninteractive apt-get install -q -y git make automake ocaml ocamlbuild ocaml-findlib python3-numpy python3-sklearn python3-scipy
RUN git clone https://gitlab.inria.fr/spinfer/spinfer.git . && git submodule update --init
RUN /bin/bash build_dependencies.sh
RUN rm coccinelle/tools/spgen/source/spgen_lexer.mll coccinelle/tools/spgen/source/spgen_lexer.ml
RUN make \
&&  make install

RUN mkdir -p /data
WORKDIR /data
ENTRYPOINT ["spinfer"]
CMD ["-f", "index", "-o", "spinfer.cocci"]

