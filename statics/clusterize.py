#!/usr/bin/env python3

import json
import random
import sys
import argparse
from math import log
from collections import Counter

import numpy as np
from scipy.cluster import hierarchy
from sklearn.metrics import silhouette_score, pairwise_distances

random.seed(42)
np.random.seed(42)


def create_clust_dict(cluster, data):
    """
    Parse output of clustering algorithm and encode it into a more user friendly dict.
    """
    clust_dict = {}
    for i, nb in enumerate(cluster):
        new = clust_dict.get(nb, [])
        new.append((i, data[i]))
        clust_dict[nb] = new
    return clust_dict


def clusterize_with_hierarchical(data, init_guess):
    distances = pairwise_distances(data, metric='cosine')
    if 1 not in init_guess or np.any(pairwise_distances(data, metric='cosine') > 0.9):
        # For a strange reason euclidean metric seems to give better results
        # than cosine.
        # TODO: investigate why.
        res = hierarchy.linkage(data, method='complete', metric='cosine')
        clusters = [
            hierarchy.fcluster(res, i, criterion='maxclust')
            for i in range(2, 15) if i < data.shape[0]
        ]
        clusters = list(filter(lambda x: 2 in x, clusters))
        # TODO: look when it happens
        if not clusters:
            print("Cannot make more than one cluster", file=sys.stderr)
            return np.ones(data.shape[0], dtype=int)
        scores = [
            silhouette_score(distances, cluster, 'precomputed')
            for cluster in clusters
        ]
        # The best number of cluster is supposed to be the first
        # maximum of the silhouette function.
        # If the function is monotonic it is the first value.
        score_diff = np.diff(scores)
        if np.any(score_diff < -0.01):
            best_index = np.argmax(np.diff(scores) < 0.01)
        else:
            best_index = 0
        best_nb = max(clusters[best_index])
        best_clust = hierarchy.fcluster(res, best_nb, criterion='maxclust')
        return best_clust
    else:
        return np.ones(data.shape[0], dtype=int)


def weight_trees(trees, example_ids):
    """
    Weights trees using document-frequency -- inverse example frequency.
    This weighting function is an inverse analog of the common TF-IDF weighting.
    A tree weight is proportional to its document frequency and inversely
    proportional to its average example frequency.
    """
    examples = [[[tree for tree in trees[id].keys()] for id in l] for l in example_ids]
    itf = {}
    df = {}
    for ids, example in zip(example_ids, examples):
        tree_counter = Counter([tree for trees in example for tree in trees])
        for tree, count in tree_counter.items():
            nb_example = len(example)
            for i in ids:
                itf[(tree, i)] = log(nb_example/(count+1)) + 1
            document_count = df[tree] if tree in df else 0
            df[tree] = document_count + 1

    weights = {(tree, i): df[tree]/len(examples)*freq for (tree, i), freq in itf.items()}
    return weights


def encode_trees(trees, weights):
    """
    Encode all examples into a numpy array, using weights dictionnary.
    """
    all_repr = {r for r, _ in weights.keys()}
    hash_to_id = {h: i for i, h in enumerate(all_repr)}
    compacted = np.zeros((len(trees), len(weights)), dtype='float')
    for i, vector in enumerate(trees):
        for hash_key in vector.keys():
            id = hash_to_id.get(hash_key)
            if id is not None:
                compacted[i, id] = weights[(hash_key, i)]
    return compacted


def filter_empty(data):
    """
    Remove empty row from data and keep non-zeros indices.
    """
    nz_indices = np.unique(np.nonzero(data)[0])
    return data[nz_indices], nz_indices


def get_encoded_data(filename_in, filename_example):
    with open(filename_example, 'r') as f:
        examples_repartition = json.load(f)
    # Parsing input file
    with open(filename_in, 'r') as f:
        vectors = [json.loads(line) for line in f if line.strip()]
    return encode_trees(vectors, weight_trees(vectors, examples_repartition))


def filter_noise(full_data):
    norm = np.linalg.norm(full_data, ord=np.inf, axis=-1)
    norm_mean = np.mean(norm)
    norm_std = np.std(norm)
    eps = 1e-6  # If stddev is zero we need to have some offset
    full_data[norm < (norm_mean - 3*norm_std) - eps] = 0
    return filter_empty(full_data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file', type=str, help="Path to input file"
    )
    parser.add_argument(
        'output_file', type=str, help="Path to output file"
    )
    parser.add_argument(
        'example_file', type=str, help="Path to example file"
    )

    arguments = parser.parse_args()
    filename_in = arguments.input_file
    filename_out = arguments.output_file
    filename_example = arguments.example_file

    with open(filename_example, 'r') as f:
        examples_repartition = json.load(f)
    init_guess = map(len, examples_repartition)
    del examples_repartition

    full_data = get_encoded_data(filename_in, filename_example)
    filtered_data, nz_indices = filter_noise(full_data)

    best_clust = clusterize_with_hierarchical(filtered_data, init_guess)
    first_level_clusters = create_clust_dict(best_clust, filtered_data)
    second_level_clusters = [[nz_indices[i] for i in list(zip(*data))[0]] for data in first_level_clusters.values()]

    with open(filename_out, 'w') as f:
        for cluster in second_level_clusters:
            if cluster:
                print(*cluster, file=f)
