#!/usr/bin/env python3

import os
import shutil
import subprocess
import argparse
import json
import tempfile

CACHE_DIR = "/tmp/spinfer_cache"
SPATCH_OPTIONS = [
    "--very-quiet",
    "--in-place",
    "--allow-inconsistent-paths",
    "--no-show-diff",
    "--iso-limit",
    "0", # Equivalent to empty iso file
    "--no-gotos", # Solve crash regarding braces
]
NORMALIZE_FILE = os.path.join(os.path.dirname(__file__), "normalize.cocci")


def copy_files(dir_src, dir_dst, files):
    for file_src, file_dst in files:
        full_src = os.path.join(dir_src, file_src)
        full_dst = os.path.join(dir_dst, file_dst)

        # Check if arrival dir exists and create it if needed
        leaf_dir_dst = os.path.dirname(full_dst)
        if not os.path.exists(leaf_dir_dst):
            os.makedirs(leaf_dir_dst)

        shutil.copyfile(full_src, full_dst)


def call_spatch(directory, jobs, file_groups):
    base_cmd = [
        "spatch",
        "--sp-file", NORMALIZE_FILE,
        "--jobs", str(jobs),
        "--file-groups", file_groups
    ]
    cmd = base_cmd + SPATCH_OPTIONS

    old_path = os.getcwd()
    os.chdir(directory)
    subprocess.run(cmd)
    os.chdir(old_path)


def get_checksum(files):
    output = subprocess.check_output(["md5sum"] + files, universal_newlines=True)
    return [line.strip().split() for line in output.splitlines()]


def get_cache_checksums():
    return os.listdir(CACHE_DIR)


def create_file_groups(files):
    f, name = tempfile.mkstemp(text=True)
    f = os.fdopen(f, 'w') # Convert FD to python file object
    for filename, lines in files.items():
        if lines:
            print("%s:%s\n" % (filename, lines), file=f)
    f.close()
    return name


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--copy-only', action="store_true", help="Do not normalize files"
    )
    parser.add_argument(
        '--use-cache', action="store_true", help="Use spinfer cache"
    )
    parser.add_argument(
        '-j', '--jobs', type=int, default=1, help="Number of threads to use"
    )
    parser.add_argument(
        "dir_src", type=str, help="Source directory"
    )
    parser.add_argument(
        "dir_dst", type=str, help="Destination directory"
    )
    parser.add_argument(
        "files", type=argparse.FileType(), help="Json for files to copy and normalize"
    )
    arguments = parser.parse_args()
    files = json.load(arguments.files)

    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)

    if arguments.copy_only or not arguments.use_cache:
        file_names = [(f, f) for f in files.keys()]
        copy_files(arguments.dir_src, arguments.dir_dst, file_names)
        # if not arguments.copy_only:
        #     file_groups = create_file_groups(files)
        #     call_spatch(arguments.dir_dst, arguments.jobs, file_groups)
        #     os.remove(file_groups)

    else: # Not copy_only and use_cache
        files_checksums = get_checksum(list(files.keys()))
        cache_checksums = get_cache_checksums()

        cached_files = [(s, f) for s, f in files_checksums if s in cache_checksums]
        uncached_files = [(f, f) for s, f in files_checksums if not s in cache_checksums]
        # Reverse is used to copy files for later cache usage
        reverse_uncached = [(f, s) for s, f in files_checksums if not s in cache_checksums]

        # First step: copy and normalize uncached files
        if uncached_files:
            copy_files(arguments.dir_src, arguments.dir_dst, uncached_files)
            # file_groups = create_file_groups(files)
            # call_spatch(arguments.dir_dst, arguments.jobs, file_groups)
            # os.remove(file_groups)

        # Second step: copy cached files
        copy_files(CACHE_DIR, arguments.dir_dst, cached_files)

        # Third step: add new files to cache
        copy_files(arguments.dir_dst, CACHE_DIR, reverse_uncached)