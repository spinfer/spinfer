#!/usr/bin/env python3
from pathlib import Path
import os
from subprocess import check_call
from sys import argv


oracle_path, new_repr, file_out = argv[1:]
oracle_path = Path(oracle_path)

def append_suffix(path, suffix):
    return path.with_suffix(''.join(path.suffixes) + suffix)

with open(append_suffix(oracle_path, '.repr'), 'rt') as f:
    before_repr = sorted([(r, i) for i, r in enumerate(f) if r])
with open(new_repr, 'rt') as f:
    after_repr = sorted([(r, i) for i, r in enumerate(f) if r])

mapping = {i: j for (_, i), (_, j) in zip(before_repr, after_repr)}

clusters = []
with open(append_suffix(oracle_path, '.truth'), 'rt') as f_in:
    local_cluster = []
    for l_in in f_in:
        if not l_in.strip():
            clusters.append(local_cluster)
            local_cluster = []
        elif l_in.startswith("# ignore"):
            break
        else:
            res = l_in.split('\t')
            if len(res) > 1:
                local_cluster.append(mapping[int(res[0])])

with open(file_out, 'wt') as f_out:
    for cluster in clusters:
        print(*cluster, sep=' ', file=f_out)