// ----------------------------------------------
// Define filtering
// ----------------------------------------------
// FIXME: Not sure about this rule
@define@
identifier I;
@@
#define I ...;

// ----------------------------------------------
// Double assignment
// ----------------------------------------------
@doubleassign@
expression E0, E1, E2;
@@
- E0 = E1 = E2;
+ E1 = E2;
+ E0 = E1;

// ----------------------------------------------
// Constant commutation normalization
// ----------------------------------------------
@commeq@
expression E;
constant C;
@@
- C == E
+ E == C

@commneq@
expression E;
constant C;
@@
- C != E
+ E != C

// ----------------------------------------------
// Test normalization
// ----------------------------------------------
@andnormptr@
expression *E;
@@
(
- E == 0
+ E == NULL
|
- !E
+ E == NULL
|
//- E != 0
//+ E != NULL
- E != 0
+ E
|
//- E
//+ E != NULL
- E != NULL
+ E
)
&& ...

@andnormexpr@
expression E;
@@
(
- E == 0
+ !E
|
- E != 0
+ E
)
&& ...

@ornormptr@
expression *E;
@@
(
- E == 0
+ E == NULL
|
- !E
+ E == NULL
|
//- E != 0
//+ E != NULL
- E != 0
+ E
|
//- E
//+ E != NULL
- E != NULL
+ E
)
|| ...

@ornormexpr@
expression E;
@@
(
- E == 0
+ !E
|
- E != 0
+ E
)
|| ...

@exists@
expression e;
@@

- !e
+ e == NULL
  ...
(
  kfree(e)
|
  vfree(e)
|
  kvfree(e)
)

//@ifelsenorm@
//expression E;
//statement S1;
//statement S2;
//statement S3;
//statement S4;
//statement S5;
//@@
//(
//  if (...) S3 else if (...) S4 else S5
//|
//- if (!E) S1
//- else S2
//+ if (E) S2
//+ else S1
//)

// ----------------------------------------------
// Statement braces
// ----------------------------------------------
@ifbracesnorm depends on !(define)@
statement S;
@@
if (...)
(
  {...}
|
+ {
  S
+ }
)

@elsebracesnorm depends on !(define)@
statement S1;
statement S2;
@@
if (...)
(
  {...}
|
+ {
  S1
+ }
)
else
(
  {...}
|
+ {
  S2
+ }
)

@forbracesnorm depends on !(define)@
statement S;
@@
for (...; ...; ...)
(
  {...}
|
+ {
  S
+ }
)

@whilebracesnorm depends on !(define)@
statement S;
@@
while (...)
(
  {...}
|
+ {
  S
+ }
)

//@dowhilebracesnorm depends on !(define)@
//statement S;
//@@
//do
//(
//  {...}
//|
//+ {
//  S
//+ }
//)
//while (...);

@@
expression E;
@@

return
- (
  E
- )
  ;